#!/usr/bin/env python

# Standard packages
import os
import csv
import sys
import argparse

from sample_sheet import SampleSheet

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--samples', help="Path to Sample Sheet")

    args = parser.parse_args()
    args.logLevel = "INFO"

    cwd = os.getcwd()
    leader_dir = os.path.join(cwd, "LEADER_output")
    fasta_file = os.path.join(cwd, "passing_merged_seqs.fasta")

    sample_sheet = SampleSheet(args.samples)

    with open(fasta_file, 'w') as fasta:
        library_count = 1
        for sample in sample_sheet.samples:
            library = f"{sample['Sample_ID']}_S{library_count}"
            top10_merged = os.path.join(leader_dir, f"{library}_L001_001_combined.fastq_read_summary_merged_top10_searchtop500.tsv")
            sys.stdout.write(f"Parsing {top10_merged}\n")
            with open(top10_merged, 'r') as csvfile:
                reader = csv.reader(csvfile, delimiter="\t")
                next(reader, None)
                for row in reader:
                    fasta.write(f">{library}_{row[3]}\n")
                    fasta.write(f"{row[4]}\n")
            library_count += 1
