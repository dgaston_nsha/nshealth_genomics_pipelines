#!/usr/bin/env python

# Standard packages
import os
import csv
import sys
import xlrd
import argparse
import xlsxwriter

from sample_sheet import SampleSheet
from collections import defaultdict

def parse_vquest(excel_file):
    results = defaultdict(dict)
    book = xlrd.open_workbook(excel_file)
    sheet = book.sheet_by_index(0)
    for row_index in range(sheet.nrows):
        results[sheet.cell(row_index,1).value()]['productive'] = sheet.cell(row,2).value()
        results[sheet.cell(row_index,1).value()]['v_allele'] = sheet.cell(row,3).value()
        results[sheet.cell(row_index,1).value()]['j_allele'] = sheet.cell(row,7).value()
        results[sheet.cell(row_index,1).value()]['d_allele'] = sheet.cell(row,11).value()
        results[sheet.cell(row_index,1).value()]['percent_id'] = sheet.cell(row,5).value()
    return results

def parse_arrest(arrest_file):
    results = defaultdict(dict)
    with open(args.input, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t')
        header = next(reader)
        for row in reader:
            results[row[0]]['subset'] = row[4]
            results[row[0]]['confidence'] = row[2]
    return results

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--samples', help="Path to Sample Sheet")
    parser.add_argument('-o', '--output', help="Filename root for output file",
                        default = "report")

    args = parser.parse_args()
    args.logLevel = "INFO"

    cwd = os.getcwd()
    leader_dir = os.path.join(cwd, "LEADER_output")

    sys.stdout.write("Parsing vquest.xls...\n")
    vquest = parse_vquest("vquest.xls")

    sys.stdout.write("Parsing arrest.tsv...\n")
    arrest = parse_arrest("arrest.tsv")

    sample_sheet = SampleSheet(args.samples)
    report_name = f"{args.output}.xlsx"

    wb = xlsxwriter.Workbook(report_name)
    summary_sheet = wb.add_worksheet("Results Summary")

    merge_format = wb.add_format({'align': 'center'})

    summary_sheet.merge_range('A1:D1', 'Sample Summary', merge_format)
    summary_sheet.write("A2", "Sample")
    summary_sheet.write("B2", "SeqID")
    summary_sheet.write("C2", "Clonal")
    summary_sheet.write("D2", "Hypermutated")

    # Lymphotrack  Results
    summary_sheet.merge_range('E1:N1', 'Lymphotrack', merge_format)
    summary_sheet.write("E2", "V-gene")
    summary_sheet.write("F2", "J-gene")
    summary_sheet.write("G2", "% Total Reads")
    summary_sheet.write("H2", "Mutation Rate")
    summary_sheet.write("I2", "% ID to WT")
    summary_sheet.write("J2", "Productive")
    summary_sheet.write("K2", "In-Frame")
    summary_sheet.write("L2", "No Stop Codon")
    summary_sheet.write("M2", "V-coverage")
    summary_sheet.write("N2", "CDR3")

    #IMGT/V-Quest Results
    summary_sheet.merge_range('O1:S1', 'V-Quest', merge_format)
    summary_sheet.write("O2", "V-gene")
    summary_sheet.write("P2", "J-gene")
    summary_sheet.write("Q2", "D-gene")
    summary_sheet.write("R2", "% ID to WT")
    summary_sheet.write("S2", "Productive")

    #Arrest Results
    summary_sheet.merge_range('U1:V1', 'Arrest', merge_format)
    summary_sheet.write("U2", "Subset")
    summary_sheet.write("V2", "Confidence")
    summary_row_idx = 3

    library_count = 1
    for sample in sample_sheet.samples:
        library = f"{sample['Sample_ID']}_S{library_count}"
        top10_merged = os.path.join(leader_dir, f"{library}_L001_001_combined.fastq_read_summary_merged_top10_searchtop500.tsv")
        sheet = wb.add_worksheet(f"{library}")

        sys.stdout.write(f"Parsing {top10_merged}\n")
        with open(top10_merged, 'r') as csvfile:
            reader = csv.reader(csvfile, delimiter="\t")
            next(reader, None)
            num_seqs = 0
            for row in reader:
                sheet.write()
                if float(row[9]) >= 2.0 or num_seqs == 0:
                    summary_sheet.write()
                    if float(row[9]) < 2.0:
                        summary_sheet.write()
                    else:
                        summary_sheet.write()
                    summary_row_idx += 1
                num_seqs += 1
