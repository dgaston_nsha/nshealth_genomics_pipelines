#!/usr/bin/env python

# Standard packages
import os
import re
import sys
import getpass
import argparse
import xlsxwriter

import numpy as np

from scipy import stats
from database import utils_updated as utils
from modules import configuration

from multiprocessing import Pool
from collections import defaultdict

from cassandra import InvalidRequest
from cassandra.cqlengine import connection
from cassandra.auth import PlainTextAuthProvider

from database.variantstore import Variant
from database.variantstore import SampleVariant
from database.coveragestore import AmpliconCoverage
from database.coveragestore import SampleCoverage

VERSION = "1.5"

def get_all_amplicons(samples):
    amplicons_list = list()
    for sample in samples:
        for library in samples[sample]:
            report_panel_path = (f"/home/dgaston/ddb-configs/disease_panels/{samples[sample][library]['panel']}/{samples[sample][library]['report']}")
            target_amplicons = utils.get_target_amplicons(report_panel_path)
            for amplicon in target_amplicons:
                if amplicon not in amplicons_list:
                    amplicons_list.append(amplicon)

    return amplicons_list

def get_coverage_data_all_amplicons(amplicons_list, addresses, auth):
    connection.setup(addresses, "coveragestore", auth_provider=auth)

    amplicon_coverage_stats = defaultdict(dict)

    for amplicon in amplicons_list:
        sys.stdout.write(f"Retrieving data for amplicon: {amplicon}\n")
        coverage_values = list()
        coverage_data = AmpliconCoverage.objects.timeout(None).filter(AmpliconCoverage.amplicon == amplicon)
        ordered_samples = coverage_data.order_by('sample', 'run_id').limit(coverage_data.count() + 1000)

        for result in ordered_samples:
            coverage_values.append(result.mean_coverage)

        if len(coverage_values) > 0:
            amplicon_coverage_stats[amplicon]['median'] = np.median(coverage_values)
            amplicon_coverage_stats[amplicon]['std_dev'] = np.std(coverage_values)
            amplicon_coverage_stats[amplicon]['min'] = np.amin(coverage_values)
            amplicon_coverage_stats[amplicon]['max'] = np.amax(coverage_values)
        else:
            amplicon_coverage_stats[amplicon]['median'] = -1
            amplicon_coverage_stats[amplicon]['std_dev'] = -1
            amplicon_coverage_stats[amplicon]['min'] = -1
            amplicon_coverage_stats[amplicon]['max'] = -1

    return amplicon_coverage_stats

def set_up_reporting_sheet(sheet, callers):
    column_index = 0

    sheet.write(0, column_index, "IGV Review")
    column_index += 1
    sheet.write(0, column_index, "Review Status")
    column_index += 1

    set_up_sheet_header(column_index, sheet, callers)

    igv_list = ['Unreviewed', 'IGV/Manual', 'Prev Reviewed']
    status_list = ['Unreviewed', 'R - Pass', 'R - Suspicious', 'R - Artifact',
    'F - Pass', 'F - Suspicious', 'F -Artifact']

    sheet.write('A2', 'Unreviewed')
    sheet.data_validation('A2', {'validate': 'list', 'source': igv_list})

    sheet.write('B2', 'Unreviewed')
    sheet.data_validation('B2', {'validate': 'list', 'source':status_list})

    sheet.write(10, 0, "Suspicious Variants")
    sheet.write('A12', 'Unreviewed')
    sheet.data_validation('A12', {'validate': 'list', 'source':igv_list})

    sheet.write('B12', 'Unreviewed')
    sheet.data_validation('B12', {'validate': 'list', 'source':status_list})

    sheet.write(15, 0, "VUS")
    sheet.write('A17', 'Unreviewed')
    sheet.data_validation('A17', {'validate': 'list', 'source':igv_list})

    sheet.write('B17', 'Unreviewed')
    sheet.data_validation('B17', {'validate': 'list', 'source':status_list})

def set_up_sheet_header(column_index, sheet, callers):
    sheet.write(0, column_index, "Library")
    column_index += 1

    sheet.write(0, column_index, "Gene")
    column_index += 1

    sheet.write(0, column_index, "Amplicon")
    column_index += 1

    sheet.write(0, column_index, "Ensembl Transcript")
    column_index += 1

    sheet.write(0, column_index, "Ref")
    column_index += 1

    sheet.write(0, column_index, "Alt")
    column_index += 1

    sheet.write(0, column_index, "SnpEff c.HGVS (auto)")
    column_index += 1

    sheet.write(0, column_index, "SnpEff p.HGVS (auto)")
    column_index += 1

    sheet.write(0, column_index, "Ensembl HGVS")
    column_index += 1

    sheet.write(0, column_index, "RefSeq HGVS")
    column_index += 1

    sheet.write(0, column_index, "Representative VAF")
    column_index += 1

    sheet.write(0, column_index, "Max Caller Somatic VAF")
    column_index += 1

    sheet.write(0, column_index, "Num Times in Database")
    column_index += 1

    sheet.write(0, column_index, "Num Times in Run")
    column_index += 1

    sheet.write(0, column_index, "Median VAF in DB")
    column_index += 1

    sheet.write(0, column_index, "Median VAF in Run")
    column_index += 1

    sheet.write(0, column_index, "StdDev VAF")
    column_index += 1

    sheet.write(0, column_index, "VAF Percentile Rank")
    column_index += 1

    sheet.write(0, column_index, "Callers")
    column_index += 1

    sheet.write(0, column_index, "Filters")
    column_index += 1

    sheet.write(0, column_index, "Caller Counts")
    column_index += 1

    sheet.write(0, column_index, "COSMIC IDs")
    column_index += 1

    sheet.write(0, column_index, "Num COSMIC Samples")
    column_index += 1

    sheet.write(0, column_index, "COSMIC AA")
    column_index += 1

    sheet.write(0, column_index, "Clinvar Pathogenicity")
    column_index += 1

    sheet.write(0, column_index, "Clinvar HGVS")
    column_index += 1

    sheet.write(0, column_index, "Clinvar Disease")
    column_index += 1

    sheet.write(0, column_index, "Coverage")
    column_index += 1

    sheet.write(0, column_index, "Num Reads")
    column_index += 1

    sheet.write(0, column_index, "Impact")
    column_index += 1

    sheet.write(0, column_index, "Severity")
    column_index += 1

    sheet.write(0, column_index, "Maximum Population AF")
    column_index += 1

    sheet.write(0, column_index, "Min Caller Depth")
    column_index += 1

    sheet.write(0, column_index, "Max Caller Depth")
    column_index += 1

    sheet.write(0, column_index, "Chrom")
    column_index += 1

    sheet.write(0, column_index, "Start")
    column_index += 1

    sheet.write(0, column_index, "End")
    column_index += 1

    sheet.write(0, column_index, "rsIDs")
    column_index += 1

    sheet.write(0, column_index, "Matching Samples in Run")
    column_index += 1


    if 'mutect' in callers:
        sheet.write(0, column_index, "MuTect_AF")
        column_index += 1

        sheet.write(0, column_index, "MuTect_DP")
        column_index += 1

    if 'vardict' in callers:
        sheet.write(0, column_index, "VarDict_AF")
        column_index += 1

        sheet.write(0, column_index, "VarDict_DP")
        column_index += 1

    if 'freebayes' in callers:
        sheet.write(0, column_index, "FreeBayes_AF")
        column_index += 1

        sheet.write(0, column_index, "FreeBayes_DP")
        column_index += 1

    if 'scalpel' in callers:
        sheet.write(0, column_index, "Scalpel_AF")
        column_index += 1

        sheet.write(0, column_index, "Scalpel_DP")
        column_index += 1

    if 'platypus' in callers:
        sheet.write(0, column_index, "Platypus_AF")
        column_index += 1

        sheet.write(0, column_index, "Platypus_DP")
        column_index += 1

    if 'pindel' in callers:
        sheet.write(0, column_index, "Pindel_AF")
        column_index += 1

        sheet.write(0, column_index, "Pindel_DP")
        column_index += 1

def report_sample(params):
    config, sample, samples, addresses, auth, thresholds, callers, amplicon_stats = params
    connection.setup(addresses, "variantstore", auth_provider=auth)

    report_data = dict()
    filtered_variant_data = defaultdict(list)
    off_target_amplicon_counts = defaultdict(int)
    target_amplicon_coverage = dict()
    ordered_amplicon_coverage = list()
    reportable_amplicons = list()

    iterated = 0
    passing_variants = 0
    filtered_low_freq = 0
    filtered_low_depth = 0
    filtered_off_target = 0

    tier1_clinvar_terms = ("Pathogenic", "Likely_pathogenic", "drug_response", "_drug_response", "Pathogenic/Likely_pathogenic")
    benign_clinvar_terms = ("Benign", "Likely_benign", "likely_benign", "Benign/Likely_benign")

    for library in samples[sample]:
        report_panel_path = (f"/home/dgaston/ddb-configs/disease_panels/{samples[sample][library]['panel']}/{samples[sample][library]['report']}")

        target_amplicons = utils.get_target_amplicons(report_panel_path)

        for amplicon in target_amplicons:
            coverage_data = SampleCoverage.objects.timeout(None).filter(
                SampleCoverage.sample == (samples[sample][library]['sample_name']),
                SampleCoverage.amplicon == amplicon,
                SampleCoverage.run_id == samples[sample][library]['run_id'],
                SampleCoverage.library_name == (samples[sample][library]['library_name']),
                SampleCoverage.program_name == "sambamba"
            )
            ordered_amplicons = (coverage_data.order_by('amplicon', 'run_id').limit(coverage_data.count() + 1000))
            for result in ordered_amplicons:
                reportable_amplicons.append(result)
                target_amplicon_coverage[amplicon] = result
                ordered_amplicon_coverage.append(result)

        variants = SampleVariant.objects.timeout(None).filter(
            SampleVariant.reference_genome == config['genome_version'],
            SampleVariant.sample == samples[sample][library]['sample_name'],
            SampleVariant.run_id == samples[sample][library]['run_id'],
            SampleVariant.library_name == (samples[sample][library]['library_name']),
            SampleVariant.max_maf_all <= thresholds['max_maf'],
            SampleVariant.max_som_aaf >= 0.01
        ).allow_filtering()

        num_var = variants.count()
        ordered = variants.order_by('library_name', 'chr', 'pos', 'ref', 'alt', 'date_annotated').limit(variants.count() + 1000)

        print(f"{library}: retrieved {num_var} variants from database\n")
        print(f"{library}: classifying and filtering variants\n")

        for variant in ordered:
            iterated += 1
            if variant.amplicon_data['amplicon'] == 'None':
                filtered_off_target += 1
                off_target_amplicon_counts[
                    variant.amplicon_data['amplicon']] += 1
            else:
                amplicons = variant.amplicon_data['amplicon'].split(',')
                assignable = 0
                for amplicon in amplicons:
                    if amplicon in target_amplicons:
                        assignable += 1
                        break
                if assignable:
                    match_variants = Variant.objects.timeout(None).filter(
                        Variant.reference_genome == config['genome_version'],
                        Variant.chr == variant.chr,
                        Variant.pos == variant.pos,
                        Variant.ref == variant.ref,
                        Variant.alt == variant.alt
                    ).allow_filtering()

                    num_matches = match_variants.count()
                    ordered_var = match_variants.order_by('pos', 'ref', 'alt', 'sample', 'library_name', 'run_id').limit(num_matches + 1000)
                    vafs = list()
                    run_vafs = list()
                    run_match_samples = list()
                    num_times_callers = defaultdict(int)
                    num_times_in_run = 0

                    for var in ordered_var:
                        vaf = var.max_som_aaf
                        vafs.append(vaf)
                        if var.run_id == variant.run_id:
                            num_times_in_run += 1
                            run_vafs.append(vaf)
                            run_match_samples.append(var.library_name)
                        for caller in var.callers:
                            num_times_callers[caller] += 1

                    variant.vaf_median = np.median(vafs)
                    variant.vaf_std_dev = np.std(vafs)
                    variant.run_median = np.median(run_vafs)
                    variant.vaf_perc_rank = stats.percentileofscore(vafs, variant.max_som_aaf, kind="mean")
                    variant.num_times_called = num_matches
                    variant.num_times_run = num_times_in_run
                    variant.matching_samples = run_match_samples

                    caller_counts_elements = list()
                    for caller in num_times_callers:
                        caller_counts_elements.append(f"{caller}: {num_times_callers[caller]}")
                    variant.num_times_callers = ",".join(caller_counts_elements)

                    # Filter FreeBayes and Pindel only calls to their own category
                    if len(variant.callers) == 1:
                        if variant.callers[0] == "freebayes" or variant.callers[0] == "pindel":
                            if variant.max_som_aaf < thresholds['min_saf']:
                                filtered_variant_data['freebayes_pindel_fail'].append(variant)
                                filtered_low_freq += 1
                            elif variant.max_depth < thresholds['depth']:
                                filtered_variant_data['freebayes_pindel_fail'].append(variant)
                                filtered_low_depth += 1
                            else:
                                filtered_variant_data['freebayes_pindel_pass'].append(variant)
                                passing_variants += 1
                            continue

                    # Putting in to Benign based on ClinVar
                    if variant.in_clinvar:
                        pathogenicity_terms_string = variant.clinvar_data['pathogenic']
                        pathogenicity_terms = pathogenicity_terms_string.split(',')
                        if any(i in benign_clinvar_terms for i in pathogenicity_terms):
                            if variant.max_som_aaf < thresholds['min_saf']:
                                filtered_variant_data['benign_fail'].append(variant)
                                filtered_low_freq += 1
                            elif variant.max_depth < thresholds['depth']:
                                filtered_variant_data['benign_fail'].append(variant)
                                filtered_low_depth += 1
                            else:
                                filtered_variant_data['benign_pass'].append(variant)
                                passing_variants += 1
                            continue


                    # Putting in to Tier1 based on COSMIC
                    if variant.cosmic_ids:
                        if variant.max_som_aaf < thresholds['min_saf']:
                            filtered_variant_data['cosvar_fail_variants'].append(variant)
                            filtered_low_freq += 1
                        elif variant.max_depth < thresholds['depth']:
                            filtered_variant_data['cosvar_fail_variants'].append(variant)
                            filtered_low_depth += 1
                        else:
                            filtered_variant_data['cosvar_pass_variants'].append(variant)
                            passing_variants += 1
                        continue

                    # Putting in to Tier1 based on ClinVar
                    if variant.in_clinvar:
                        pathogenicity_terms_string = variant.clinvar_data['pathogenic']
                        pathogenicity_terms = pathogenicity_terms_string.split(',')
                        if any(i in tier1_clinvar_terms for i in pathogenicity_terms):
                            if variant.max_som_aaf < thresholds['min_saf']:
                                filtered_variant_data['cosvar_fail_variants'].append(variant)
                                filtered_low_freq += 1
                            elif variant.max_depth < thresholds['depth']:
                                filtered_variant_data['cosvar_fail_variants'].append(variant)
                                filtered_low_depth += 1
                            else:
                                filtered_variant_data['cosvar_pass_variants'].append(variant)
                                passing_variants += 1
                            continue

                    if variant.severity == 'HIGH':
                        if variant.max_som_aaf < thresholds['min_saf']:
                            filtered_variant_data['high_fail_variants'].append(variant)
                            filtered_low_freq += 1
                        elif variant.max_depth < thresholds['depth']:
                            filtered_variant_data['high_fail_variants'].append(variant)
                            filtered_low_depth += 1
                        else:
                            filtered_variant_data['high_pass_variants'].append(variant)
                            passing_variants += 1
                        continue
                    elif variant.severity == 'MED':
                        if variant.max_som_aaf < thresholds['min_saf']:
                            filtered_variant_data['med_fail_variants'].append(variant)
                            filtered_low_freq += 1
                        elif variant.max_depth < thresholds['depth']:
                            filtered_variant_data['med_fail_variants'].append(variant)
                            filtered_low_depth += 1
                        else:
                            filtered_variant_data['med_pass_variants'].append(variant)
                            passing_variants += 1
                        continue
                    else:
                        if variant.max_som_aaf < thresholds['min_saf']:
                            filtered_variant_data['low_fail_variants'].append(variant)
                            filtered_low_freq += 1
                        elif variant.max_depth < thresholds['depth']:
                            filtered_variant_data['low_fail_variants'].append(variant)
                            filtered_low_depth += 1
                        else:
                            filtered_variant_data['low_pass_variants'].append(variant)
                            passing_variants += 1
                        continue
                else:
                    filtered_off_target += 1
                    off_target_amplicon_counts[variant.amplicon_data[
                        'amplicon']] += 1

        print(f"{library}: iterated through {iterated} variants\n")
        print(f"{library}: passing {len(filtered_variant_data['cosvar_pass_variants'])} ClinVar and COSMIC Variants\n")
        print(f"{library}: passing {len(filtered_variant_data['high_pass_variants'])} additional High Impact Variants\n")
        print(f"{library}: passing {len(filtered_variant_data['med_pass_variants'])} additional Medium Impact Variants\n")
        print(f"{library}: passing {len(filtered_variant_data['low_pass_variants'])} additional Low Impact Variants\n")

    report_data['variants'] = filtered_variant_data
    report_data['coverage'] = target_amplicon_coverage

    report_name = "{}.xlsx".format(sample)

    wb = xlsxwriter.Workbook(report_name)

    error_style = wb.add_format({'bg_color': '#FF0000'})
    warning_style = wb.add_format({'bg_color': '#FF9900'})
    pass_style = wb.add_format({'bg_color': '#00FF00'})
    interest_style = wb.add_format({'bg_color': '#808080'})
    default_style = wb.add_format({'bg_color': '#FFFFFF'})

    coverage_sheet = wb.add_worksheet("Coverage")
    report_sheet = wb.add_worksheet("Reporting Variants")
    cosvar_sheet = wb.add_worksheet("ClinVar COSMIC Pass")
    high_sheet = wb.add_worksheet("High Impact Pass")
    med_sheet = wb.add_worksheet("Med Impact Pass")
    low_sheet = wb.add_worksheet("Low Impact Pass")
    benign_sheet = wb.add_worksheet("Benign Pass")
    freepin_sheet = wb.add_worksheet("FreeBayes Pindel Only")
    cosvar_fail_sheet = wb.add_worksheet("ClinVar COSMIC Fail")
    high_fail_sheet = wb.add_worksheet("High Impact Fail")
    med_fail_sheet = wb.add_worksheet("Med Impact Fail")
    low_fail_sheet = wb.add_worksheet("Low Impact Fail")
    freepin_fail_sheet = wb.add_worksheet("FreeBayes Pindel Only Fail")
    benign_fail_sheet = wb.add_worksheet("Benign Fail")

    tier_sheets = (cosvar_sheet, high_sheet, med_sheet, low_sheet, benign_sheet, freepin_sheet,
                   cosvar_fail_sheet, high_fail_sheet, med_fail_sheet, low_fail_sheet,
                   freepin_fail_sheet, benign_fail_sheet)

    tier_key = ("cosvar_pass_variants", "high_pass_variants", "med_pass_variants", "low_pass_variants",
                "benign_pass", "freebayes_pindel_pass",
                "cosvar_fail_variants", "high_fail_variants", "med_fail_variants", "low_fail_variants",
                "freebayes_pindel_fail", "benign_fail")

    libraries = list()
    report_templates = list()
    run_id = ""
    for library in samples[sample]:
        libraries.append(samples[sample][library]['library_name'])
        report_templates.append(samples[sample][library]['report'])
        run_id = samples[sample][library]['run_id']
    lib_string = " | ".join(libraries)
    reports_string = " | ".join(report_templates)

    coverage_sheet.write(0, 0, "Sample")
    coverage_sheet.write(0, 1, f"{sample}")

    coverage_sheet.write(1, 0, "Libraries")
    coverage_sheet.write(1, 1, f"{lib_string}")

    coverage_sheet.write(2, 0, "Run ID")
    coverage_sheet.write(2, 1, f"{run_id}")

    coverage_sheet.write(3, 0, "Reporting Templates")
    coverage_sheet.write(3, 1, f"{reports_string}")

    coverage_sheet.write(4, 0, "Minimum Somatic Allele Frequency")
    coverage_sheet.write(4, 1, f"{thresholds['min_saf']}")

    coverage_sheet.write(5, 0, "Minimum Amplicon Depth")
    coverage_sheet.write(5, 1, f"{thresholds['depth']}")

    coverage_sheet.write(6, 0, "Maximum Population Allele Frequency")
    coverage_sheet.write(6, 1, f"{thresholds['max_maf']}")

    coverage_sheet.write(7, 0, "Depth Fail Criteria: Max Caller DP")
    coverage_sheet.write(7, 1, f"{thresholds['depth']}")

    coverage_sheet.write(8, 0, "Bioinformatics Pipeline Version")
    coverage_sheet.write(8, 1, f"{config['pipeline_version']}")

    coverage_sheet.write(9, 0, "COSMIC Version")
    coverage_sheet.write(9, 1, f"{config['cosmic_version']}")

    coverage_sheet.write(10, 0, "ClinVar Version")
    coverage_sheet.write(10, 1, f"{config['clinvar_version']}")

    coverage_sheet.write(12, 0, "Sample")
    coverage_sheet.write(12, 1, "Library")
    coverage_sheet.write(12, 2, "Amplicon")
    coverage_sheet.write(12, 3, "Num Reads")
    coverage_sheet.write(12, 4, "Coverage")
    coverage_sheet.write(12, 5, "Database Median")
    coverage_sheet.write(12, 6, "Database Std Dev")

    coverage_sheet.write("F1", "1st Reviewer:")
    coverage_sheet.write("F2", "2nd Reviewer:")
    coverage_sheet.write("F3", "Final Review:")

    reviewers_list = ['DG', 'JW', 'RX', 'MC', 'SF', 'TG']

    coverage_sheet.write('G1', 'Reviewer')
    coverage_sheet.data_validation('G1', {'validate': 'list', 'source': reviewers_list})

    coverage_sheet.write('G2', 'Reviewer')
    coverage_sheet.data_validation('G2', {'validate': 'list', 'source': reviewers_list})

    coverage_sheet.write('G3', 'Reviewer')
    coverage_sheet.data_validation('G3', {'validate': 'list', 'source': reviewers_list})

    row_num = 13

    failed_coverage_amplicons = list()

    for amplicon in reportable_amplicons:
        if amplicon.mean_coverage < 250:
            style = error_style
            failed_coverage_amplicons.append(amplicon)
        elif amplicon.mean_coverage < 500:
            style = warning_style
        else:
            style = pass_style

        coverage_sheet.write(row_num, 0, f"{amplicon.sample}", style)
        coverage_sheet.write(row_num, 1, f"{amplicon.library_name}", style)
        coverage_sheet.write(row_num, 2, f"{amplicon.amplicon}", style)
        coverage_sheet.write(row_num, 3, f"{amplicon.num_reads}", style)
        coverage_sheet.write(row_num, 4, f"{round(amplicon.mean_coverage, 0)}", style)
        coverage_sheet.write(row_num, 5,
                             f"{round(amplicon_stats[amplicon.amplicon]['median'], 0)}", style)
        coverage_sheet.write(row_num, 6,
                             f"{round(amplicon_stats[amplicon.amplicon]['std_dev'], 0)}", style)

        row_num += 1

    coverage_sheet.write(10, 9, "Failed Amplicons (< 250X Median Coverage)")
    coverage_sheet.write(10, 10, "Coverage")
    row_num = 11
    for failed_amplicon in failed_coverage_amplicons:
        coverage_sheet.write(row_num, 9, f"{failed_amplicon.amplicon}")
        coverage_sheet.write(row_num, 10, f"{round(failed_amplicon.mean_coverage, 0)}")
        row_num += 1

    ###########################################################################
    sheet_num = 0
    set_up_reporting_sheet(report_sheet, callers)

    for sheet in tier_sheets:
        column_index = 0
        row = 1
        set_up_sheet_header(column_index, sheet, callers)

        for variant in report_data['variants'][tier_key[sheet_num]]:
            style = default_style
            if variant.max_som_aaf > 0.05:
                style = interest_style
                if int(variant.num_times_run) >= 10:
                    if float(variant.vaf_perc_rank) < 0.8:
                        style = error_style
            if variant.cosmic_ids:
                num_list = variant.cosmic_data['num_samples'].strip("()")
                nums = num_list.split(',')
                for num in nums:
                    if num == 'None':
                        num = 0
                    if int(num) >= 5:
                        style = interest_style
                if int(variant.num_times_run) >= 10:
                    if float(variant.vaf_perc_rank) < 0.8:
                        style = error_style
            if len(variant.callers) == 1:
                style = warning_style

            # cosmic_nums = re.findall(r'\b\d+\b', variant.cosmic_data['num_samples'])
            amplicons = variant.amplicon_data['amplicon'].split(',')

            coverage_values = list()
            reads_values = list()
            for amplicon in amplicons:
                if amplicon in reportable_amplicons:
                    coverage_values.append(
                        str(report_data['coverage'][amplicon]['mean_coverage']))
                    reads_values.append(
                        str(report_data['coverage'][amplicon]['num_reads']))

            coverage_string = ",".join(coverage_values)
            reads_string = ",".join(reads_values)

            if len(variant.ref) < 200:
                ref = variant.ref
            else:
                ref = "Length > 200bp"
                style = warning_style

            if len(variant.alt) < 200:
                alt = variant.alt
            else:
                alt = "Length > 200bp"
                style = warning_style

            if len(variant.codon_change) < 200:
                codon_change = variant.codon_change
            else:
                codon_change = "Length > 200aa"
                style = warning_style

            if len(variant.aa_change) < 200:
                aa_change = variant.aa_change
            else:
                aa_change = "Length > 200aa"
                style = warning_style

            column_index = 0
            sheet.write(row, column_index, f"{variant.library_name}", style)
            column_index += 1

            sheet.write(row, column_index, f"{variant.gene}", style)
            column_index += 1

            sheet.write(row, column_index, f"{variant.amplicon_data['amplicon']}", style)
            column_index += 1

            sheet.write(row, column_index, f"{variant.transcript}", style)
            column_index += 1

            sheet.write(row, column_index, f"{ref}", style)
            column_index += 1

            sheet.write(row, column_index, f"{alt}", style)
            column_index += 1

            sheet.write(row, column_index, f"{codon_change}", style)
            column_index += 1

            sheet.write(row, column_index, f"{aa_change}", style)
            column_index += 1

            sheet.write(row, column_index, f"-", style)
            column_index += 1

            sheet.write(row, column_index, f"-", style)
            column_index += 1

            sheet.write(row, column_index, f"-", style)
            column_index += 1

            sheet.write(row, column_index, round(variant.max_som_aaf, 3), style)
            column_index += 1

            sheet.write(row, column_index, variant.num_times_called, style)
            column_index += 1

            sheet.write(row, column_index, variant.num_times_run, style)
            column_index += 1

            sheet.write(row, column_index, round(variant.vaf_median, 3), style)
            column_index += 1

            sheet.write(row, column_index, round(variant.run_median, 3), style)
            column_index += 1

            sheet.write(row, column_index, round(variant.vaf_std_dev, 3), style)
            column_index += 1

            sheet.write(row, column_index, round(variant.vaf_perc_rank, 0), style)
            column_index += 1

            sheet.write(row, column_index, f"{','.join(variant.callers) or None}", style)
            column_index += 1

            sheet.write(row, column_index, f"{'.'}", style)
            column_index += 1

            sheet.write(row, column_index, f"{variant.num_times_callers}", style)
            column_index += 1

            sheet.write(row, column_index, f"{','.join(variant.cosmic_ids) or None}", style)
            column_index += 1

            sheet.write(row, column_index, f"{variant.cosmic_data['num_samples']}", style)
            column_index += 1

            sheet.write(row, column_index, f"{variant.cosmic_data['aa']}", style)
            column_index += 1

            sheet.write(row, column_index, f"{variant.clinvar_data['pathogenic']}", style)
            column_index += 1

            sheet.write(row, column_index, f"{variant.clinvar_data['hgvs']}", style)
            column_index += 1

            sheet.write(row, column_index, f"{variant.clinvar_data['disease']}", style)
            column_index += 1

            sheet.write(row, column_index, f"{coverage_string}", style)
            column_index += 1

            sheet.write(row, column_index, f"{reads_string}", style)
            column_index += 1

            sheet.write(row, column_index, f"{variant.impact}", style)
            column_index += 1

            sheet.write(row, column_index, f"{variant.severity}", style)
            column_index += 1

            sheet.write(row, column_index, f"{variant.max_maf_all}", style)
            column_index += 1

            sheet.write(row, column_index, f"{variant.min_depth}", style)
            column_index += 1

            sheet.write(row, column_index, f"{variant.max_depth}", style)
            column_index += 1

            sheet.write(row, column_index, f"{variant.chr}", style)
            column_index += 1

            sheet.write(row, column_index, f"{variant.pos}", style)
            column_index += 1

            sheet.write(row, column_index, f"{variant.end}", style)
            column_index += 1

            sheet.write(row, column_index, f"{','.join(variant.rs_ids)}", style)
            column_index += 1

            sheet.write(row, column_index, f"{variant.matching_samples}", style)
            column_index += 1


            if 'mutect' in callers:
                sheet.write(row, column_index, f"{variant.mutect.get('AAF') or None}", style)
                column_index += 1

                sheet.write(row, column_index, f"{variant.mutect.get('DP') or None}", style)
                column_index += 1

            if 'vardict' in callers:
                sheet.write(row, column_index, f"{variant.vardict.get('AAF') or None}", style)
                column_index += 1

                sheet.write(row, column_index, f"{variant.vardict.get('DP') or None}", style)
                column_index += 1

            if 'freebayes' in callers:
                sheet.write(row, column_index, f"{variant.freebayes.get('AAF') or None}", style)
                column_index += 1

                sheet.write(row, column_index, f"{variant.freebayes.get('DP') or None}", style)
                column_index += 1

            if 'scalpel' in callers:
                sheet.write(row, column_index, f"{variant.scalpel.get('AAF') or None}", style)
                column_index += 1

                sheet.write(row, column_index, f"{variant.scalpel.get('DP') or None}", style)
                column_index += 1

            if 'platypus' in callers:
                sheet.write(row, column_index, f"{variant.platypus.get('AAF') or None}", style)
                column_index += 1

                sheet.write(row, column_index, f"{variant.platypus.get('DP') or None}", style)
                column_index += 1

            if 'pindel' in callers:
                sheet.write(row, column_index, f"{variant.pindel.get('AAF') or None}", style)
                column_index += 1

                sheet.write(row, column_index, f"{variant.pindel.get('DP') or None}", style)
                column_index += 1

            row += 1
        sheet_num += 1
    wb.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--samples_file', help="Input file for samples")
    parser.add_argument('-a', '--address', help="IP Address for Cassandra connection", default='127.0.0.1')
    parser.add_argument('-u', '--username', help='Cassandra username for login', default=None)
    parser.add_argument('-c', '--configuration', help="Configuration file for various settings")
    parser.add_argument('-r', '--report',
                        help="Root name for reports (per sample)",
                        default='report')
    parser.add_argument('-d', '--min_depth',
                        help='Minimum depth threshold for variant reporting',
                        default=200.0)
    parser.add_argument('-g', '--good_depth',
                        help='Floor for good depth of coverage',
                        default=500.0)
    parser.add_argument('-t', '--min_somatic_var_freq',
                        help='Minimum reportable somatic variant frequency',
                        default=0.01)
    parser.add_argument('-x', '--low_vaf_cutoff',
                        help='Minimum reportable somatic variant frequency',
                        default=0.05)
    parser.add_argument('-p', '--max_pop_freq',
                        help='Maximum allowed population allele frequency',
                        default=0.005)
    args = parser.parse_args()
    args.logLevel = "INFO"

    num_cores = 24

    if args.username:
        password = getpass.getpass()
        auth_provider = PlainTextAuthProvider(username=args.username, password=password)
    else:
        auth_provider = None

    pool = Pool(num_cores)
    sample_data = list()

    sys.stdout.write("Parsing configuration data\n")
    config = configuration.configure_runtime(args.configuration)

    sys.stdout.write("Parsing sample data\n")
    libraries = configuration.configure_samples(args.samples_file, config)
    samples = configuration.merge_library_configs_samples(libraries)

    thresholds = {'min_saf': float(args.min_somatic_var_freq),
                  'low_vaf': float(args.low_vaf_cutoff),
                  'max_maf': float(args.max_pop_freq),
                  'depth': int(args.min_depth)}

    callers = ("mutect", "platypus", "vardict", "scalpel", "freebayes", "pindel")

    sys.stdout.write("Compiling list of all amplicons requested for set of reports\n")
    amplicons_list = get_all_amplicons(samples)

    sys.stdout.write("Retrieving coverage data for all amplicons\n")
    amplicon_stats = get_coverage_data_all_amplicons(amplicons_list, [args.address], auth_provider)

    for sample in samples:
        config_data = (config, sample, samples, [args.address], auth_provider, thresholds, callers, amplicon_stats)
        sample_data.append(config_data)

    sys.stdout.write("Creating reports\n")
    results = pool.map(report_sample, sample_data)
