#!/usr/bin/env python

# Standard packages
import os
import re
import sys
import csv
import cyvcf2
import getpass
import argparse

from database import utils
from modules import vcf_parsing
from modules import configuration

from cyvcf2 import VCF
from datetime import datetime
from multiprocessing import Pool
from collections import defaultdict

from cassandra import WriteFailure
from cassandra import InvalidRequest
from cassandra.cqlengine import connection
from cassandra.auth import PlainTextAuthProvider

from database.coveragestore import AmpliconCoverage
from database.coveragestore import SampleCoverage

def add_sample(data):
    sample = data[0]
    samples = data[1]
    config = data[2]
    address = data[3]
    auth = data[4]

    program = f"sambamba"

    connection.setup([address], "coveragestore", auth_provider=auth)

    with open("{}.sambamba_regioncoverage.bed".format(samples[sample]['library_name']), 'r') as coverage:
        reader = csv.reader(coverage, delimiter='\t')
        header = next(reader)
        threshold_indices = list()
        thresholds = list()
        index = 0
        for element in header:
            if element.startswith("percentage"):
                threshold = element.replace('percentage', '')
                threshold_indices.append(index)
                thresholds.append(int(threshold))
            index += 1

        for row in reader:
            threshold_data = defaultdict(float)
            index = 0
            for threshold in thresholds:
                threshold_data[threshold] = row[threshold_indices[index]]
                index += 1

            sample_data = SampleCoverage.create(sample=samples[sample]['sample_name'],
                                                library_name=samples[sample]['library_name'],
                                                run_id=samples[sample]['run_id'],
                                                num_libraries_in_run=samples[sample]['num_libraries_in_run'],
                                                sequencer_id=samples[sample]['sequencer'],
                                                program_name=program,
                                                extraction=samples[sample]['extraction'],
                                                panel=samples[sample]['panel'],
                                                target_pool=samples[sample]['target_pool'],
                                                amplicon=row[3],
                                                num_reads=row[4],
                                                mean_coverage=row[5],
                                                thresholds=thresholds,
                                                perc_bp_cov_at_thresholds=threshold_data)

            amplicon_data = AmpliconCoverage.create(amplicon=row[3],
                                                    sample=samples[sample]['sample_name'],
                                                    library_name=samples[sample]['library_name'],
                                                    run_id=samples[sample]['run_id'],
                                                    num_libraries_in_run=samples[sample]['num_libraries_in_run'],
                                                    sequencer_id=samples[sample]['sequencer'],
                                                    program_name=program,
                                                    extraction=samples[sample]['extraction'],
                                                    panel=samples[sample]['panel'],
                                                    target_pool=samples[sample]['target_pool'],
                                                    num_reads=row[4],
                                                    mean_coverage=row[5],
                                                    thresholds=thresholds,
                                                    perc_bp_cov_at_thresholds=threshold_data)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--samples_file', help="Input file for samples")
    parser.add_argument('-a', '--address', help="IP Address for Cassandra connection", default='127.0.0.1')
    parser.add_argument('-u', '--username', help='Cassandra username for login', default=None)
    parser.add_argument('-c', '--configuration', help="Configuration file for various settings")
    args = parser.parse_args()
    args.logLevel = "INFO"

    num_cores = 24

    if args.username:
        password = getpass.getpass()
        auth_provider = PlainTextAuthProvider(username=args.username, password=password)
    else:
        auth_provider = None

    pool = Pool(num_cores)
    sample_data = list()

    sys.stdout.write("Parsing configuration data\n")
    config = configuration.configure_runtime(args.configuration)
    sys.stdout.write("Parsing sample data\n")
    samples = configuration.configure_samples(args.samples_file, config)

    for sample in samples:
        config_data = (sample, samples, config, args.address, auth_provider)
        sample_data.append(config_data)

    sys.stdout.write("Adding variants\n")
    results = pool.map(add_sample, sample_data)
