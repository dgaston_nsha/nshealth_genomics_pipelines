#!/usr/bin/env python

# Standard packages
import os
import re
import sys
import csv
import cyvcf2
import getpass
import argparse

from database import utils_updated as utils
from modules import vcf_parsing_updated as vcf_parsing
from modules import configuration

from cyvcf2 import VCF
from datetime import datetime
from multiprocessing import Pool
from collections import defaultdict

from cassandra import WriteFailure
from cassandra import InvalidRequest
from cassandra.cqlengine import connection
from cassandra.auth import PlainTextAuthProvider

from database.variantstore import Variant
from database.variantstore import SampleVariant
from database.variantstore import TargetVariant

def add_sample(data):
    sample = data[0]
    samples = data[1]
    config = data[2]
    address = data[3]
    auth = data[4]

    connection.setup([address], "variantstore", auth_provider=auth)

    parse_functions = {'mutect': vcf_parsing.parse_mutect_vcf_record,
                       'freebayes': vcf_parsing.parse_freebayes_vcf_record,
                       'vardict': vcf_parsing.parse_vardict_vcf_record,
                       'scalpel': vcf_parsing.parse_scalpel_vcf_record,
                       'platypus': vcf_parsing.parse_platypus_vcf_record,
                       'pindel': vcf_parsing.parse_pindel_vcf_record}

    caller_records = defaultdict(lambda: dict())

    sys.stdout.write("Parsing Caller VCF Files\n")
    vcf_parsing.parse_vcf(f"{sample}.mutect.low_support_filtered.vcf", "mutect", caller_records)
    vcf_parsing.parse_vcf(f"{sample}.vardict.low_support_filtered.vcf", "vardict", caller_records)
    vcf_parsing.parse_vcf(f"{sample}.freebayes.low_support_filtered.vcf", "freebayes", caller_records)
    vcf_parsing.parse_vcf(f"{sample}.scalpel.low_support_filtered.vcf", "scalpel", caller_records)
    vcf_parsing.parse_vcf(f"{sample}.platypus.low_support_filtered.vcf", "platypus", caller_records)
    vcf_parsing.parse_vcf(f"{sample}.pindel.low_support_filtered.vcf", "pindel", caller_records)

    annotated_vcf = f"{sample}.vcfanno.snpEff.GRCh37.75.vcf"

    vcf = VCF(annotated_vcf)

    reader = cyvcf2.VCFReader(annotated_vcf)
    desc = reader["ANN"]["Description"]
    annotation_keys = [x.strip("\"'") for x in re.split(r"\s*\|\s*", desc.split(":", 1)[1].strip('" '))]
    add_error_log = open(f"{sample}_db_add_errors.log", 'w')

    # Filter out variants with minor allele frequencies above the threshold but
    # retain any that are above the threshold but in COSMIC or in ClinVar and
    # not listed as benign.
    added = 0
    failed = 0
    for variant in vcf:
        # Parsing VCF and creating data structures for Cassandra model
        callers = variant.INFO.get('CALLERS').split(',')
        effects = utils.get_effects(variant, annotation_keys)
        top_impact = utils.get_top_impact(effects)
        population_freqs = utils.get_population_freqs(variant)
        amplicon_data = utils.get_amplicon_data(variant)

        key = (str("chr{}".format(variant.CHROM)), int(variant.start), int(variant.end), str(variant.REF),
               str(variant.ALT[0]))

        caller_variant_data_dicts = defaultdict(dict)
        max_som_aaf = -1.00
        max_depth = -1
        min_depth = 100000000

        for caller in callers:
            try:
                caller_variant_data_dicts[caller] = parse_functions[caller](caller_records[caller][key])
            except KeyError:
                sys.stderr.write(f"ERROR Parsing data from {caller} for variant ({key}). Key not found\n")
                add_error_log.write(f"ERROR Parsing data from {caller} for variant ({key}). Key not found\n")
                continue

            if float(caller_variant_data_dicts[caller]['AAF']) > max_som_aaf:
                max_som_aaf = float(caller_variant_data_dicts[caller]['AAF'])
            if int(caller_variant_data_dicts[caller]['DP']) < min_depth:
                min_depth = int(caller_variant_data_dicts[caller]['DP'])
            if int(caller_variant_data_dicts[caller]['DP']) > max_depth:
                max_depth = int(caller_variant_data_dicts[caller]['DP'])

        if min_depth == 100000000:
            min_depth = -1

        # Create Cassandra Objects
        # Create the general variant ordered table
        try:
            cassandra_variant = Variant.create(
                    reference_genome=config['genome_version'],
                    chr=variant.CHROM,
                    pos=variant.start,
                    end=variant.end,
                    ref=variant.REF,
                    alt=variant.ALT[0],
                    sample=samples[sample]['sample_name'],
                    extraction=samples[sample]['extraction'],
                    library_name=samples[sample]['library_name'],
                    run_id=samples[sample]['run_id'],
                    panel_name=samples[sample]['panel'],
                    # initial_report_panel=samples[sample]['report'],
                    target_pool=samples[sample]['target_pool'],
                    sequencer=samples[sample]['sequencer'],
                    rs_id=variant.ID,
                    date_annotated=datetime.now(),
                    subtype=variant.INFO.get('sub_type'),
                    type=variant.INFO.get('type'),

                    gene=top_impact.gene,
                    transcript=top_impact.transcript,
                    exon=top_impact.exon,
                    codon_change=top_impact.codon_change,
                    biotype=top_impact.biotype,
                    aa_change=top_impact.aa_change,
                    severity=top_impact.effect_severity,
                    impact=top_impact.top_consequence,
                    impact_so=top_impact.so,

                    max_maf_all=variant.INFO.get('max_aaf_all') or -1,
                    max_maf_no_fin=variant.INFO.get('max_aaf_no_fin') or -1,
                    transcripts_data=utils.get_transcript_effects(effects),
                    clinvar_data=utils.get_clinvar_info(variant, samples, sample),
                    cosmic_data=utils.get_cosmic_info(variant),
                    in_clinvar=vcf_parsing.var_is_in_clinvar(variant),
                    in_cosmic=vcf_parsing.var_is_in_cosmic(variant),
                    is_pathogenic=vcf_parsing.var_is_pathogenic(variant),
                    is_lof=vcf_parsing.var_is_lof(variant),
                    is_coding=vcf_parsing.var_is_coding(variant),
                    is_splicing=vcf_parsing.var_is_splicing(variant),
                    rs_ids=vcf_parsing.parse_rs_ids(variant),
                    cosmic_ids=vcf_parsing.parse_cosmic_ids(variant),
                    callers=callers,
                    population_freqs=population_freqs,
                    amplicon_data=amplicon_data,
                    max_som_aaf=max_som_aaf,
                    min_depth=min_depth,
                    max_depth=max_depth,
                    mutect=caller_variant_data_dicts['mutect'] or dict(),
                    freebayes=caller_variant_data_dicts['freebayes'] or dict(),
                    scalpel=caller_variant_data_dicts['scalpel'] or dict(),
                    platypus=caller_variant_data_dicts['platypus'] or dict(),
                    pindel=caller_variant_data_dicts['pindel'] or dict(),
                    vardict=caller_variant_data_dicts['vardict'] or dict()
                    )
        except WriteFailure:
            with open("{}.sample_variant_add.log".format(samples[sample]['library_name']), "a") as err:
                err.write("Failed to write variant to variantstore:\n")
                err.write("Sample: {}\t Library: {}\n".format(samples[sample]['sample_name'],
                                                              samples[sample]['library_name'],))
                err.write("{}\n".format(variant))
        except InvalidRequest:
            with open("{}.sample_variant_add.log".format(samples[sample]['library_name']), "a") as err:
                err.write("Failed to write variant to variantstore:\n")
                err.write("Sample: {}\t Library: {}\n".format(samples[sample]['sample_name'],
                                                              samples[sample]['library_name'], ))
                err.write("{}\n".format(variant))

        # Create Cassandra Object
        try:
            sample_variant = SampleVariant.create(
                    sample=samples[sample]['sample_name'],
                    run_id=samples[sample]['run_id'],
                    library_name=samples[sample]['library_name'],
                    reference_genome=config['genome_version'],
                    chr=variant.CHROM,
                    pos=variant.start,
                    end=variant.end,
                    ref=variant.REF,
                    alt=variant.ALT[0],
                    extraction=samples[sample]['extraction'],
                    panel_name=samples[sample]['panel'],
                    # initial_report_panel=samples[sample]['report'],
                    target_pool=samples[sample]['target_pool'],
                    sequencer=samples[sample]['sequencer'],
                    rs_id=variant.ID,
                    date_annotated=datetime.now(),
                    subtype=variant.INFO.get('sub_type'),
                    type=variant.INFO.get('type'),
                    gene=top_impact.gene,
                    transcript=top_impact.transcript,
                    exon=top_impact.exon,
                    codon_change=top_impact.codon_change,
                    biotype=top_impact.biotype,
                    aa_change=top_impact.aa_change,
                    severity=top_impact.effect_severity,
                    impact=top_impact.top_consequence,
                    impact_so=top_impact.so,
                    max_maf_all=variant.INFO.get('max_aaf_all') or -1,
                    max_maf_no_fin=variant.INFO.get('max_aaf_no_fin') or -1,
                    transcripts_data=utils.get_transcript_effects(effects),
                    clinvar_data=utils.get_clinvar_info(variant, samples, sample),
                    cosmic_data=utils.get_cosmic_info(variant),
                    in_clinvar=vcf_parsing.var_is_in_clinvar(variant),
                    in_cosmic=vcf_parsing.var_is_in_cosmic(variant),
                    is_pathogenic=vcf_parsing.var_is_pathogenic(variant),
                    is_lof=vcf_parsing.var_is_lof(variant),
                    is_coding=vcf_parsing.var_is_coding(variant),
                    is_splicing=vcf_parsing.var_is_splicing(variant),
                    rs_ids=vcf_parsing.parse_rs_ids(variant),
                    cosmic_ids=vcf_parsing.parse_cosmic_ids(variant),
                    callers=callers,
                    population_freqs=population_freqs,
                    amplicon_data=amplicon_data,
                    max_som_aaf=max_som_aaf,
                    min_depth=min_depth,
                    max_depth=max_depth,
                    mutect=caller_variant_data_dicts['mutect'] or dict(),
                    freebayes=caller_variant_data_dicts['freebayes'] or dict(),
                    scalpel=caller_variant_data_dicts['scalpel'] or dict(),
                    platypus=caller_variant_data_dicts['platypus'] or dict(),
                    pindel=caller_variant_data_dicts['pindel'] or dict(),
                    vardict=caller_variant_data_dicts['vardict'] or dict(),
                    manta=caller_variant_data_dicts['manta'] or dict()
                    )
        except WriteFailure:
            failed += 1
            with open("{}.sample_variant_add.log".format(samples[sample]['library_name']), "a") as err:
                err.write("Failed to write variant to variantstore:\n")
                err.write("Sample: {}\t Library: {}\n".format(samples[sample]['sample_name'],
                                                              samples[sample]['library_name'],))
                err.write("{}\n".format(variant))
        except InvalidRequest:
            with open("{}.sample_variant_add.log".format(samples[sample]['library_name']), "a") as err:
                err.write("Failed to write variant to variantstore:\n")
                err.write("Sample: {}\t Library: {}\n".format(samples[sample]['sample_name'],
                                                              samples[sample]['library_name'], ))
        added += 1

    with open("{}.sample_variant_add.log".format(samples[sample]['library_name']), "a") as err:
        err.write("Sample: {}\t Library: {}\n".format(samples[sample]['sample_name'],
                                                      samples[sample]['library_name'], ))
        err.write("Wrote {} variants to variantstore\n".format(added))
        err.write("Failed to add {} variants to variantstore\n".format(failed))

    add_error_log.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--samples_file', help="Input file for samples")
    parser.add_argument('-a', '--address', help="IP Address for Cassandra connection", default='127.0.0.1')
    parser.add_argument('-u', '--username', help='Cassandra username for login', default=None)
    parser.add_argument('-c', '--configuration', help="Configuration file for various settings")
    args = parser.parse_args()
    args.logLevel = "INFO"

    num_cores = 24

    if args.username:
        password = getpass.getpass()
        auth_provider = PlainTextAuthProvider(username=args.username, password=password)
    else:
        auth_provider = None

    pool = Pool(num_cores)
    sample_data = list()

    sys.stdout.write("Parsing configuration data\n")
    config = configuration.configure_runtime(args.configuration)
    sys.stdout.write("Parsing sample data\n")
    samples = configuration.configure_samples(args.samples_file, config)

    for sample in samples:
        config_data = (sample, samples, config, args.address, auth_provider)
        sample_data.append(config_data)

    sys.stdout.write("Adding variants\n")
    results = pool.map(add_sample, sample_data)
