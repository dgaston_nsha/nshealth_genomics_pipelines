#!/usr/bin/env python

import os
import csv
import sys
import getpass
import argparse
import xlsxwriter

import numpy as np
from scipy import stats

from multiprocessing import Pool
from collections import defaultdict

from modules import configuration

from cassandra import InvalidRequest
from cassandra.cqlengine import connection
from cassandra.auth import PlainTextAuthProvider

from database import utils_updated as utils

from database.datastore import Target
from database.datastore import TargetCovData
from database.datastore import Variant
from database.datastore import VariantCallerVAFs
from database.datastore import VariantStats

VERSION = "1.0"

def create_dict_defaultdict():
    return defaultdict(dict)

def write_variant_to_sheet(sample_id, variant, vaf_data, vaf_db_stats, coverage_data, panel, sheet, style, row):
    variant_id = f"{variant['Chr']}-{variant['Pos']}-{variant['Ref']}-{variant['Alt']}"

    column_index = 0
    sheet.write(row, column_index, f"{sample_id}", style)
    column_index += 1

    sheet.write(row, column_index, f"{variant['gene']}", style)
    column_index += 1

    sheet.write(row, column_index, f"{variant['amplicons']}", style)
    column_index += 1

    sheet.write(row, column_index, f"{variant['transcript']}", style)
    column_index += 1

    sheet.write(row, column_index, f"{variant['Ref']}", style)
    column_index += 1

    sheet.write(row, column_index, f"{variant['Alt']}", style)
    column_index += 1

    sheet.write(row, column_index, f"{variant['codon_change']}", style)
    column_index += 1

    sheet.write(row, column_index, f"{variant['aa_change']}", style)
    column_index += 1

    #Ensembl ID
    sheet.write(row, column_index, f"-", style)
    column_index += 1

    # RefSeq ID
    sheet.write(row, column_index, f"-", style)
    column_index += 1

    # Representative VAF
    sheet.write(row, column_index, f"-", style)
    column_index += 1

    sheet.write(row, column_index, round(float(variant['max_som_aaf']), 3), style)
    column_index += 1

    # Num Times in DB
    sheet.write(row, column_index, f"{vaf_db_stats[variant_id][panel]['num_samples']}", style)
    column_index += 1

    # Num Times in Run
    sheet.write(row, column_index, int(vaf_data[0]), style)
    column_index += 1

    # Median VAF in DB
    sheet.write(row, column_index, f"{vaf_db_stats[variant_id][panel]['median_max_vaf']}", style)
    column_index += 1

    # Median VAF in Run
    sheet.write(row, column_index, round(float(vaf_data[1]), 3), style)
    column_index += 1

    # Std Dev in Run
    sheet.write(row, column_index, round(float(vaf_data[3]), 3), style)
    column_index += 1

    run_vafs = [float(x) for x in vaf_data[4].split("|")]
    # Percentile Rank in Run
    sheet.write(row, column_index, round(stats.percentileofscore(run_vafs, float(variant['max_som_aaf']), kind="mean"), 0), style)
    column_index += 1

    sheet.write(row, column_index, f"{variant['callers']}", style)
    column_index += 1

    # Filters
    sheet.write(row, column_index, f"{'.'}", style)
    column_index += 1

    # Caller Counts
    mutect_count = vaf_db_stats[variant_id][panel]['mutect_count']
    freebayes_count = vaf_db_stats[variant_id][panel]['freebayes_count']
    vardict_count = vaf_db_stats[variant_id][panel]['vardict_count']
    scalpel_count = vaf_db_stats[variant_id][panel]['scalpel_count']
    pindel_count = vaf_db_stats[variant_id][panel]['pindel_count']
    platypus_count = vaf_db_stats[variant_id][panel]['platypus_count']

    cntstr = f"MuTect: {mutect_count} | freebayes: {freebayes_count} | VarDict: {vardict_count} | Scalpel: {scalpel_count} | Pindel: {pindel_count} | Platypus: {platypus_count}"

    sheet.write(row, column_index, f"{cntstr}", style)
    column_index += 1

    sheet.write(row, column_index, f"{variant['cosmic_ids']}", style)
    column_index += 1

    sheet.write(row, column_index, f"{variant['cosmic_numsamples']}", style)
    column_index += 1

    sheet.write(row, column_index, f"{variant['cosmic_aa']}", style)
    column_index += 1

    sheet.write(row, column_index, f"{variant['clinvar_pathogenic']}", style)
    column_index += 1

    sheet.write(row, column_index, f"{variant['clinvar_hgvs']}", style)
    column_index += 1

    sheet.write(row, column_index, f"{variant['clinvar_disease']}", style)
    column_index += 1

    # Coverage of Amplicon
    mean_coverage = []
    num_reads = []
    for amplicon in variant['amplicons'].split(','):
        try:
            mean_coverage.append(str(round(coverage_data[amplicon]['mean_coverage'], 0)))
        except KeyError:
            sys.stderr.write(f"Missing data for Mean Coverage of Amplicon {amplicon} for sample {sample_id}\n")
        try:
            num_reads.append(str(coverage_data[amplicon]['num_reads']))
        except KeyError:
            sys.stderr.write(f"Missing data for Num Reads of Amplicon {amplicon} for sample {sample_id}\n")

    sheet.write(row, column_index, f"{'|'.join(mean_coverage)}", style)
    column_index += 1

    # Number of Reads for Amplicon
    sheet.write(row, column_index, f"{'|'.join(num_reads)}", style)
    column_index += 1

    sheet.write(row, column_index, f"{variant['impact']}", style)
    column_index += 1

    sheet.write(row, column_index, f"{variant['severity']}", style)
    column_index += 1

    sheet.write(row, column_index, float(f"{variant['max_maf_all']}"), style)
    column_index += 1

    sheet.write(row, column_index, int(f"{variant['min_depth']}"), style)
    column_index += 1

    sheet.write(row, column_index, int(f"{variant['max_depth']}"), style)
    column_index += 1

    sheet.write(row, column_index, f"{variant['Chr']}", style)
    column_index += 1

    sheet.write(row, column_index, int(f"{variant['Pos']}"), style)
    column_index += 1

    # End Position
    end_position = int(variant['Pos']) + len(variant['Ref'])
    if len(variant['Ref']) == len(variant['Alt']):
        end_position = int(variant['Pos']) + len(variant['Ref'])
    elif len(variant['Ref']) > len(variant['Alt']):
        end_position = int(variant['Pos']) + len(variant['Ref'])
    elif len(variant['Alt']) > len(variant['Ref']):
        end_position = int(variant['Pos']) + len(variant['Alt'])
    else:
        end_position = "ERROR"

    sheet.write(row, column_index, f"{end_position}", style)
    column_index += 1

    sheet.write(row, column_index, f"{variant['rs_ids']}", style)
    column_index += 1

    # Other Samples in Run with this Variant
    sheet.write(row, column_index, f"{vaf_data[5]}", style)
    column_index += 1

    if variant['mutect_AF']:
        sheet.write(row, column_index, float(f"{variant['mutect_AF']}"), style)
        column_index += 1
        sheet.write(row, column_index, int(f"{variant['mutect_DP']}"), style)
        column_index += 1
        # sheet.write(row, column_index, f"{variant['mutect_Filters']}", style)
        # column_index += 1
    else:
        sheet.write(row, column_index, f"{variant['mutect_AF']}", style)
        column_index += 1
        sheet.write(row, column_index, f"{variant['mutect_DP']}", style)
        column_index += 1
        # sheet.write(row, column_index, f"{variant['mutect_Filters']}", style)
        # column_index += 1

    if variant['vardict_AF']:
        sheet.write(row, column_index, float(f"{variant['vardict_AF']}"), style)
        column_index += 1
        sheet.write(row, column_index, int(f"{variant['vardict_DP']}"), style)
        column_index += 1
        # sheet.write(row, column_index, f"{variant['vardict_Filters']}", style)
        # column_index += 1
    else:
        sheet.write(row, column_index, f"{variant['vardict_AF']}", style)
        column_index += 1
        sheet.write(row, column_index, f"{variant['vardict_DP']}", style)
        column_index += 1
        # sheet.write(row, column_index, f"{variant['mutect_Filters']}", style)
        # column_index += 1

    if variant['freebayes_AF']:
        sheet.write(row, column_index, float(f"{variant['freebayes_AF']}"), style)
        column_index += 1
        sheet.write(row, column_index, int(f"{variant['freebayes_DP']}"), style)
        column_index += 1
        # sheet.write(row, column_index, f"{variant['freebayes_Filters']}", style)
        # column_index += 1
    else:
        sheet.write(row, column_index, f"{variant['freebayes_AF']}", style)
        column_index += 1
        sheet.write(row, column_index, f"{variant['freebayes_DP']}", style)
        column_index += 1
        # sheet.write(row, column_index, f"{variant['mutect_Filters']}", style)
        # column_index += 1

    if variant['scalpel_AF']:
        sheet.write(row, column_index, float(f"{variant['scalpel_AF']}"), style)
        column_index += 1
        sheet.write(row, column_index, int(f"{variant['scalpel_DP']}"), style)
        column_index += 1
        # sheet.write(row, column_index, f"{variant['scalpel_Filters']}", style)
        # column_index += 1
    else:
        sheet.write(row, column_index, f"{variant['scalpel_AF']}", style)
        column_index += 1
        sheet.write(row, column_index, f"{variant['scalpel_DP']}", style)
        column_index += 1
        # sheet.write(row, column_index, f"{variant['mutect_Filters']}", style)
        # column_index += 1

    if variant['platypus_AF']:
        sheet.write(row, column_index, float(f"{variant['platypus_AF']}"), style)
        column_index += 1
        sheet.write(row, column_index, int(f"{variant['platypus_DP']}"), style)
        column_index += 1
        # sheet.write(row, column_index, f"{variant['platypus_Filters']}", style)
        # column_index += 1
    else:
        sheet.write(row, column_index, f"{variant['platypus_AF']}", style)
        column_index += 1
        sheet.write(row, column_index, f"{variant['platypus_DP']}", style)
        column_index += 1
        # sheet.write(row, column_index, f"{variant['mutect_Filters']}", style)
        # column_index += 1

    if variant['pindel_AF']:
        sheet.write(row, column_index, float(f"{variant['pindel_AF']}"), style)
        column_index += 1
        sheet.write(row, column_index, int(f"{variant['pindel_DP']}"), style)
        column_index += 1
        # sheet.write(row, column_index, f"{variant['pindel_Filters']}", style)
        # column_index += 1
    else:
        sheet.write(row, column_index, f"{variant['pindel_AF']}", style)
        column_index += 1
        sheet.write(row, column_index, f"{variant['pindel_DP']}", style)
        column_index += 1
        # sheet.write(row, column_index, f"{variant['mutect_Filters']}", style)
        # column_index += 1

def classify_variant(variant, sheets):
    sheet = "med_sheet"

    tier1_clinvar_terms = ("Pathogenic", "Likely_pathogenic", "drug_response", "_drug_response", "Pathogenic/Likely_pathogenic")
    benign_clinvar_terms = ("Benign", "Likely_benign", "likely_benign", "Benign/Likely_benign")

    if variant['severity'] == 'HIGH':
        if int(variant['max_depth']) >= 100:
            sheet = "high_sheet"
        else:
            sheet = "high_fail_sheet"
    elif variant['severity'] == 'MED':
        if int(variant['max_depth']) >= 100:
            sheet = "med_sheet"
        else:
            sheet = "med_fail_sheet"
    else:
        if int(variant['max_depth']) >= 100:
            sheet = "low_sheet"
        else:
            sheet = "low_fail_sheet"

    if variant['in_cosmic'] == "True":
        if int(variant['max_depth']) >= 100:
            sheet = "cosvar_sheet"
        else:
            sheet = "cosvar_fail_sheet"

    if variant['in_clinvar'] == "True":
        pathogenicity_terms_string = variant['clinvar_pathogenic']
        pathogenicity_terms = pathogenicity_terms_string.split(',')
        if any(i in benign_clinvar_terms for i in pathogenicity_terms):
            if int(variant['max_depth']) >= 100:
                sheet = "benign_sheet"
            else:
                sheet = "benign_fail_sheet"
        else:
            if int(variant['max_depth']) >= 100:
                sheet = "cosvar_sheet"
            else:
                sheet = "cosvar_fail_sheet"

    if variant['callers'] == "freebayes" or variant['callers'] == "pindel":
        if int(variant['max_depth']) >= 100:
            sheet = "freepin_sheet"
        else:
            sheet = "freepin_fail_sheet"

    return sheet

def variant_style(variant, variant_id, styles, run_vaf_data):
    style = styles['default_style']

    num_times_run = int(run_vaf_data[0])
    median_vaf = float(run_vaf_data[1])
    std_dev = float(run_vaf_data[3])

    if float(variant['max_som_aaf']) > 0.05:
        style = styles['interest_style']
        if int(run_vaf_data[0]) >= 10:
            style = styles['error_style']
    if variant['cosmic_ids']:
        num_list = variant['cosmic_numsamples'].strip("()")
        nums = num_list.split(',')
        for num in nums:
            if num == 'None':
                num = 0
            if int(num) >= 5:
                style = styles['interest_style']
        if int(run_vaf_data[0]) >= 10:
            style = styles['error_style']

    if len(variant['callers'].split(',')) == 1:
        style = styles['warning_style']
    if len(variant['Ref']) > 200:
        style = styles['warning_style']
    if len(variant['Alt']) > 200:
        style = styles['warning_style']
    if len(variant['codon_change']) > 200:
        style = styles['warning_style']

    return style

def set_up_sheet_header(column_index, sheet, callers, style):
    sheet.autofilter('A1:AY10000')
    sheet.write(0, column_index, "Library", style)
    column_index += 1

    sheet.write(0, column_index, "Gene", style)
    column_index += 1

    sheet.write(0, column_index, "Amplicon", style)
    column_index += 1

    sheet.write(0, column_index, "Ensembl Transcript", style)
    column_index += 1

    sheet.write(0, column_index, "Ref", style)
    column_index += 1

    sheet.write(0, column_index, "Alt", style)
    column_index += 1

    sheet.write(0, column_index, "SnpEff c.HGVS (auto)", style)
    column_index += 1

    sheet.write(0, column_index, "SnpEff p.HGVS (auto)", style)
    column_index += 1

    sheet.write(0, column_index, "Ensembl HGVS", style)
    column_index += 1

    sheet.write(0, column_index, "RefSeq HGVS", style)
    column_index += 1

    sheet.write(0, column_index, "Representative VAF", style)
    column_index += 1

    sheet.write(0, column_index, "Max Caller Somatic VAF", style)
    column_index += 1

    sheet.write(0, column_index, "Num Times in Database", style)
    column_index += 1

    sheet.write(0, column_index, "Num Times in Run", style)
    column_index += 1

    sheet.write(0, column_index, "Median VAF in DB", style)
    column_index += 1

    sheet.write(0, column_index, "Median VAF in Run", style)
    column_index += 1

    sheet.write(0, column_index, "StdDev VAF", style)
    column_index += 1

    sheet.write(0, column_index, "VAF Percentile Rank in Run", style)
    column_index += 1

    sheet.write(0, column_index, "Callers", style)
    column_index += 1

    sheet.write(0, column_index, "Filters", style)
    column_index += 1

    sheet.write(0, column_index, "Caller Counts", style)
    column_index += 1

    sheet.write(0, column_index, "COSMIC IDs", style)
    column_index += 1

    sheet.write(0, column_index, "Num COSMIC Samples", style)
    column_index += 1

    sheet.write(0, column_index, "COSMIC AA", style)
    column_index += 1

    sheet.write(0, column_index, "Clinvar Pathogenicity", style)
    column_index += 1

    sheet.write(0, column_index, "Clinvar HGVS", style)
    column_index += 1

    sheet.write(0, column_index, "Clinvar Disease", style)
    column_index += 1

    sheet.write(0, column_index, "Coverage", style)
    column_index += 1

    sheet.write(0, column_index, "Num Reads", style)
    column_index += 1

    sheet.write(0, column_index, "Impact", style)
    column_index += 1

    sheet.write(0, column_index, "Severity", style)
    column_index += 1

    sheet.write(0, column_index, "Maximum Population AF", style)
    column_index += 1

    sheet.write(0, column_index, "Min Caller Depth", style)
    column_index += 1

    sheet.write(0, column_index, "Max Caller Depth", style)
    column_index += 1

    sheet.write(0, column_index, "Chrom", style)
    column_index += 1

    sheet.write(0, column_index, "Start", style)
    column_index += 1

    sheet.write(0, column_index, "End", style)
    column_index += 1

    sheet.write(0, column_index, "rsIDs", style)
    column_index += 1

    sheet.write(0, column_index, "Matching Samples in Run", style)
    column_index += 1


    if 'mutect' in callers:
        sheet.write(0, column_index, "MuTect_AF", style)
        column_index += 1

        sheet.write(0, column_index, "MuTect_DP", style)
        column_index += 1

    if 'vardict' in callers:
        sheet.write(0, column_index, "VarDict_AF", style)
        column_index += 1

        sheet.write(0, column_index, "VarDict_DP", style)
        column_index += 1

    if 'freebayes' in callers:
        sheet.write(0, column_index, "FreeBayes_AF", style)
        column_index += 1

        sheet.write(0, column_index, "FreeBayes_DP", style)
        column_index += 1

    if 'scalpel' in callers:
        sheet.write(0, column_index, "Scalpel_AF", style)
        column_index += 1

        sheet.write(0, column_index, "Scalpel_DP", style)
        column_index += 1

    if 'platypus' in callers:
        sheet.write(0, column_index, "Platypus_AF", style)
        column_index += 1

        sheet.write(0, column_index, "Platypus_DP", style)
        column_index += 1

    if 'pindel' in callers:
        sheet.write(0, column_index, "Pindel_AF", style)
        column_index += 1

        sheet.write(0, column_index, "Pindel_DP", style)
        column_index += 1

def set_up_reporting_sheet(sheet, callers, styles):
    column_index = 0

    sheet.write(0, column_index, "IGV Review")
    column_index += 1
    sheet.write(0, column_index, "Review Status")
    column_index += 1

    set_up_sheet_header(column_index, sheet, callers, styles['header_style'])

    igv_list = ['Unreviewed', 'IGV/Manual', 'Prev Reviewed']
    status_list = ['Unreviewed', 'R - Pass', 'R - Suspicious', 'R - Artifact',
    'F - Pass', 'F - Suspicious', 'F -Artifact']

    sheet.write('A2', 'Unreviewed')
    sheet.data_validation('A2', {'validate': 'list', 'source': igv_list})

    sheet.write('B2', 'Unreviewed')
    sheet.data_validation('B2', {'validate': 'list', 'source':status_list})

    sheet.write(10, 0, "Suspicious Variants")
    sheet.write('A12', 'Unreviewed')
    sheet.data_validation('A12', {'validate': 'list', 'source':igv_list})

    sheet.write('B12', 'Unreviewed')
    sheet.data_validation('B12', {'validate': 'list', 'source':status_list})

    sheet.write(15, 0, "VUS")
    sheet.write('A17', 'Unreviewed')
    sheet.data_validation('A17', {'validate': 'list', 'source':igv_list})

    sheet.write('B17', 'Unreviewed')
    sheet.data_validation('B17', {'validate': 'list', 'source':status_list})

def parse_run_vafs(file, panels_list, addresses, auth):
    connection.setup(addresses, "datastore", auth_provider=auth)
    run_vafs = defaultdict(list)
    vaf_db_stats = defaultdict(create_dict_defaultdict)
    # Structure: ["Variant_ID", "Num Times in Run", "Median VAF", "Mean VAF", "StdDev", "VAFs", "Samples"]
    with open(file, 'r') as csvfile:
        reader = csv.reader(csvfile)
        header = next(reader)
        for row in reader:
            run_vafs[row[0]] = [row[1], row[2], row[3], row[4], row[5], row[6]]
            for panel in panels_list:
                variant_data = VariantCallerVAFs.objects.filter(variant_id=row[0],
                                                             panel_name=panel,
                                                             sequencer='NextSeq')
                vaf_db_stats[row[0]][panel]['num_samples'] = 0
                vaf_db_stats[row[0]][panel]['mutect_count'] = 0
                vaf_db_stats[row[0]][panel]['freebayes_count'] = 0
                vaf_db_stats[row[0]][panel]['vardict_count'] = 0
                vaf_db_stats[row[0]][panel]['scalpel_count'] = 0
                vaf_db_stats[row[0]][panel]['pindel_count'] = 0
                vaf_db_stats[row[0]][panel]['platypus_count'] = 0

                vaf_db_stats[row[0]][panel]['median_max_vaf'] = 0.0
                vaf_db_stats[row[0]][panel]['mutect_median_vaf'] = 0.0
                vaf_db_stats[row[0]][panel]['freebayes_median_vaf'] = 0.0
                vaf_db_stats[row[0]][panel]['vardict_median_vaf'] = 0.0
                vaf_db_stats[row[0]][panel]['scalpel_median_vaf'] = 0.0
                vaf_db_stats[row[0]][panel]['pindel_median_vaf'] = 0.0
                vaf_db_stats[row[0]][panel]['platypus_median_vaf'] = 0.0

                vaf_db_stats[row[0]][panel]['max_vaf_stddev'] = 0.0
                vaf_db_stats[row[0]][panel]['mutect_vaf_stddev'] = 0.0
                vaf_db_stats[row[0]][panel]['freebayes_vaf_stddev'] = 0.0
                vaf_db_stats[row[0]][panel]['vardict_vaf_stddev'] = 0.0
                vaf_db_stats[row[0]][panel]['scalpel_vaf_stddev'] = 0.0
                vaf_db_stats[row[0]][panel]['pindel_vaf_stddev'] = 0.0
                vaf_db_stats[row[0]][panel]['platypus_vaf_stddev'] = 0.0

                max_vafs = []
                mutect_vafs = []
                freebayes_vafs = []
                vardict_vafs = []
                scalpel_vafs = []
                pindel_vafs = []
                platypus_vafs = []

                for variant in variant_data:
                    vaf_db_stats[row[0]][panel]['num_samples'] += 1
                    max_vafs.append(variant['max_vaf'])

                    if variant['mutect_vaf']:
                        vaf_db_stats[row[0]][panel]['mutect_count'] += 1
                        mutect_vafs.append(variant['mutect_vaf'])
                    if variant['freebayes_vaf']:
                        vaf_db_stats[row[0]][panel]['freebayes_count'] += 1
                        freebayes_vafs.append(variant['freebayes_vaf'])
                    if variant['vardict_vaf']:
                        vaf_db_stats[row[0]][panel]['vardict_count'] += 1
                        vardict_vafs.append(variant['vardict_vaf'])
                    if variant['scalpel_vaf']:
                        vaf_db_stats[row[0]][panel]['scalpel_count'] += 1
                        scalpel_vafs.append(variant['scalpel_vaf'])
                    if variant['pindel_vaf']:
                        vaf_db_stats[row[0]][panel]['pindel_count'] += 1
                        pindel_vafs.append(variant['pindel_vaf'])
                    if variant['platypus_vaf']:
                        vaf_db_stats[row[0]][panel]['platypus_count'] += 1
                        platypus_vafs.append(variant['platypus_vaf'])

                if len(max_vafs) >= 1:
                    vaf_db_stats[row[0]][panel]['median_max_vaf'] = np.median(max_vafs)
                    vaf_db_stats[row[0]][panel]['max_vaf_stddev'] = np.std(max_vafs)

    return run_vafs, vaf_db_stats

def get_coverage_data(library_id, target_amplicons):
    coverage_data = defaultdict(lambda: dict())
    coverage_file = f"{library_id}.sambamba_regioncoverage.bed"

    with open(coverage_file, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t')
        header = next(reader)
        for row in reader:
            if row[3] in target_amplicons:
                coverage_data[row[3]]['num_reads'] = int(row[4])
                coverage_data[row[3]]['mean_coverage'] = float(row[5])
                coverage_data[row[3]]['perc_bp_100'] = float(row[6])
                coverage_data[row[3]]['perc_bp_200'] = float(row[7])
                coverage_data[row[3]]['perc_bp_500'] = float(row[8])
                coverage_data[row[3]]['perc_bp_1000'] = float(row[9])
                coverage_data[row[3]]['perc_bp_1500'] = float(row[10])
                coverage_data[row[3]]['perc_bp_2000'] = float(row[11])

    return coverage_data

def get_db_coverage_data(amplicons_list, panels_list, addresses, auth):
    connection.setup(addresses, "datastore", auth_provider=auth)

    amplicon_coverage_stats = defaultdict(dict)

    sys.stdout.write("Retrieving Coverage data for amplicons in panels\n")
    for panel in panels_list:
        report_panel_path = (f"/home/dgaston/ddb-configs/disease_panels/{panel}/{panel}.bed")
        target_amplicons = utils.get_target_amplicons(report_panel_path)

        for amplicon in target_amplicons:
            # sys.stdout.write(f"Retrieving data for amplicon {amplicon} on {panel} panel\n")
            coverage_data = TargetCovData.objects.filter(target_name=amplicon,
                                                         panel_name=panel,
                                                         sequencer='NextSeq')
            coverage_values = list()
            count = 0
            for result in coverage_data:
                count += 1
                coverage_values.append(result.mean_coverage)

            # sys.stdout.write(f"Retrieved {count} entries for amplicon\n")
            amplicon_coverage_stats[amplicon]['median'] = (np.median(coverage_values))
            amplicon_coverage_stats[amplicon]['std_dev'] = np.std(coverage_values)
            amplicon_coverage_stats[amplicon]['min'] = np.amin(coverage_values)
            amplicon_coverage_stats[amplicon]['max'] = np.amax(coverage_values)

    return amplicon_coverage_stats

def write_info_sheet(info_sheet, samples, sample, library, config, styles):
    sample_id = samples[sample][library]['sample_name']
    library_id = samples[sample][library]['library_name']

    info_sheet.write(0, 0, "Sample")
    info_sheet.write(0, 1, f"{sample_id}")

    info_sheet.write(1, 0, "Libraries")
    info_sheet.write(1, 1, f"{library_id}")

    info_sheet.write(2, 0, "Run ID")
    info_sheet.write(2, 1, f"{samples[sample][library]['run_id']}")

    info_sheet.write(3, 0, "Reporting Templates")
    info_sheet.write(3, 1, f"{samples[sample][library]['report']}")

    info_sheet.write(4, 0, "Minimum Somatic Allele Frequency")
    info_sheet.write(4, 1, f"1%")

    info_sheet.write(5, 0, "Minimum Amplicon Depth")
    info_sheet.write(5, 1, f"100X")

    info_sheet.write(6, 0, "Maximum Population Allele Frequency")
    info_sheet.write(6, 1, f"0.5%")

    info_sheet.write(7, 0, "Depth Fail Criteria: Max Caller DP")
    info_sheet.write(7, 1, f"100X")

    info_sheet.write(8, 0, "Bioinformatics Pipeline Version")
    info_sheet.write(8, 1, f"{config['pipeline_version']}")

    info_sheet.write(9, 0, "COSMIC Version")
    info_sheet.write(9, 1, f"{config['cosmic_version']}")

    info_sheet.write(10, 0, "ClinVar Version")
    info_sheet.write(10, 1, f"{config['clinvar_version']}")

    info_sheet.write("F1", "1st Reviewer:")
    info_sheet.write("F2", "2nd Reviewer:")
    info_sheet.write("F3", "Final Review:")

    reviewers_list = ['DG', 'JW', 'RX', 'MC', 'SF', 'TG']

    info_sheet.write('G1', 'Reviewer')
    info_sheet.data_validation('G1', {'validate': 'list', 'source': reviewers_list})

    info_sheet.write('G2', 'Reviewer')
    info_sheet.data_validation('G2', {'validate': 'list', 'source': reviewers_list})

    info_sheet.write('G3', 'Reviewer')
    info_sheet.data_validation('G3', {'validate': 'list', 'source': reviewers_list})

def write_coverage_sheet(coverage_sheet, coverage_data, amplicon_stats, samples, sample, library, styles, auth, address):
    library_id = samples[sample][library]['library_name']
    sample_id = samples[sample][library]['sample_name']

    coverage_sheet.write(5, 0, "Sample")
    coverage_sheet.write(5, 1, "Library")
    coverage_sheet.write(5, 2, "Amplicon")
    coverage_sheet.write(5, 3, "Num Reads")
    coverage_sheet.write(5, 4, "Mean Coverage")
    coverage_sheet.write(5, 5, "Database Median")
    coverage_sheet.write(5, 6, "Database Std Dev")
    coverage_sheet.write(5, 7, "Percent bp > 100X")
    coverage_sheet.write(5, 8, "Percent bp > 200X")
    coverage_sheet.write(5, 9, "Percent bp > 500X")
    coverage_sheet.write(5, 10, "Percent bp > 1000X")

    row_num = 6

    failed_coverage_amplicons = list()

    for amplicon in coverage_data.keys():
        if coverage_data[amplicon]['mean_coverage'] < 250:
            style = styles['error_style']
            failed_coverage_amplicons.append(amplicon)
        elif coverage_data[amplicon]['mean_coverage'] < 500:
            style = styles['warning_style']
        else:
            style = styles['pass_style']

        coverage_sheet.write(row_num, 0, f"{sample_id}", style)
        coverage_sheet.write(row_num, 1, f"{library_id}", style)
        coverage_sheet.write(row_num, 2, f"{amplicon}", style)
        coverage_sheet.write(row_num, 3, f"{coverage_data[amplicon]['num_reads']}", style)
        coverage_sheet.write(row_num, 4, f"{round(coverage_data[amplicon]['mean_coverage'], 0)}", style)
        coverage_sheet.write(row_num, 5, f"{amplicon_stats[amplicon]['median']}", style)
        coverage_sheet.write(row_num, 6, f"{amplicon_stats[amplicon]['std_dev']}", style)
        coverage_sheet.write(row_num, 7, f"{coverage_data[amplicon]['perc_bp_100']}", style)
        coverage_sheet.write(row_num, 8, f"{coverage_data[amplicon]['perc_bp_200']}", style)
        coverage_sheet.write(row_num, 9, f"{coverage_data[amplicon]['perc_bp_500']}", style)
        coverage_sheet.write(row_num, 10, f"{coverage_data[amplicon]['perc_bp_1000']}", style)

        row_num += 1

    coverage_sheet.write(0, 13, "Failed Amplicons (< 250X Median Coverage)")
    coverage_sheet.write(0, 14, "Coverage")
    row_num = 1
    for amplicon in failed_coverage_amplicons:
        coverage_sheet.write(row_num, 12, f"{amplicon}")
        coverage_sheet.write(row_num, 13, f"{round(coverage_data[amplicon]['mean_coverage'], 0)}")
        row_num += 1

def create_sample_report(data):
    samples = data[0]
    sample = data[1]
    library = data[2]
    run_vafs = data[3]
    amplicon_stats = data[4]
    vaf_db_stats = data[5]
    config = data[6]
    auth = data[7]
    address = data[8]

    library_id = samples[sample][library]['library_name']
    sample_id = samples[sample][library]['sample_name']

    sys.stdout.write(f"Creating excel report for {sample_id}...\n")
    callers = "mutect, freebayes, vardict, scalpel, platypus, pindel"
    report_name = f"{sample_id}.xlsx"
    sample_variants = f"{library_id}.variants.csv"
    panel = samples[sample][library]['panel']
    report_panel_path = f"/home/dgaston/ddb-configs/disease_panels/{panel}/{samples[sample][library]['report']}"
    target_amplicons = utils.get_target_amplicons(report_panel_path)
    amplicon_coverage_data = get_coverage_data(library_id, target_amplicons)

    report_data = dict()
    filtered_variant_data = defaultdict(list)
    off_target_amplicon_counts = defaultdict(int)
    target_amplicon_coverage = dict()
    ordered_amplicon_coverage = list()
    reportable_amplicons = list()

    iterated = 0
    passing_variants = 0
    filtered_low_freq = 0
    filtered_low_depth = 0
    filtered_off_target = 0

    wb = xlsxwriter.Workbook(report_name, {'strings_to_numbers': True,
                                           'nan_inf_to_errors': True})

    # Set up Styles
    styles = dict()
    styles['error_style'] = wb.add_format({"bg_color": "#FFC7CE", "font_color": "#9C0006"})
    styles['pass_style'] = wb.add_format({"bg_color": "#C6EFCE", "font_color": "#006100"})
    styles['warning_style'] = wb.add_format({'bg_color': '#FF9900'})
    styles['interest_style'] = wb.add_format({'bg_color': '#808080'})
    styles['default_style'] = wb.add_format({'bg_color': '#FFFFFF'})
    styles['header_style'] = wb.add_format({'bold': 1})

    sheets = dict()
    # Set up Worksheets
    sheets['info_sheet'] = wb.add_worksheet("Info and QC")
    sheets['coverage_sheet'] = wb.add_worksheet("Coverage")
    sheets['report_sheet'] = wb.add_worksheet("Reporting Variants")
    sheets['cosvar_sheet'] = wb.add_worksheet("ClinVar COSMIC Pass")
    sheets['high_sheet'] = wb.add_worksheet("High Impact Pass")
    sheets['med_sheet'] = wb.add_worksheet("Med Impact Pass")
    sheets['low_sheet'] = wb.add_worksheet("Low Impact Pass")
    sheets['benign_sheet'] = wb.add_worksheet("Benign Pass")
    sheets['freepin_sheet'] = wb.add_worksheet("FreeBayes Pindel Only")
    sheets['cosvar_fail_sheet'] = wb.add_worksheet("ClinVar COSMIC Fail")
    sheets['high_fail_sheet'] = wb.add_worksheet("High Impact Fail")
    sheets['med_fail_sheet'] = wb.add_worksheet("Med Impact Fail")
    sheets['low_fail_sheet'] = wb.add_worksheet("Low Impact Fail")
    sheets['freepin_fail_sheet'] = wb.add_worksheet("FreeBayes Pindel Only Fail")
    sheets['benign_fail_sheet'] = wb.add_worksheet("Benign Fail")

    write_info_sheet(sheets['info_sheet'], samples, sample, library, config, styles)
    write_coverage_sheet(sheets['coverage_sheet'], amplicon_coverage_data, amplicon_stats, samples, sample, library, styles, auth, address)
    set_up_reporting_sheet(sheets['report_sheet'], callers, styles)

    for sheet in sheets:
        column_index = 0
        row = 1
        if sheet != "coverage_sheet" and sheet != "info_sheet" and sheet != "report_sheet":
            set_up_sheet_header(column_index, sheets[sheet], callers, styles['header_style'])

    with open(sample_variants, 'r') as csvfile:
        reader = csv.DictReader(csvfile)
        row_counts = defaultdict(lambda: 1)
        for variant in reader:
            if float(variant['max_som_aaf']) >= 0.01:
                amplicons = variant['amplicons'].split(',')
                if any(i in target_amplicons for i in amplicons):
                    variant_id = f"{variant['Chr']}-{variant['Pos']}-{variant['Ref']}-{variant['Alt']}"
                    vaf_data = run_vafs[variant_id]
                    style = variant_style(variant, variant_id, styles, vaf_data)
                    sheet = classify_variant(variant, sheets)
                    write_variant_to_sheet(sample_id, variant, vaf_data, vaf_db_stats, amplicon_coverage_data, panel, sheets[sheet], style, row_counts[sheet])
                    row_counts[sheet] += 1

    wb.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--samples_file', help="Input file for samples")
    parser.add_argument('-c', '--configuration', help="Configuration file for various settings")
    parser.add_argument('-t', '--threads', help="Number of CPU Threads", type=int, default=1)
    parser.add_argument('-u', '--username', help="Cassandra DB username")
    parser.add_argument('-a', '--address', help="IP Address for Cassandra connection", default='127.0.0.1')

    args = parser.parse_args()
    args.logLevel = "INFO"

    auth_provider = None
    if args.username:
        password = getpass.getpass()
        auth_provider = PlainTextAuthProvider(username=args.username, password=password)
    else:
        sys.stderr("Failed to provide Cassandra DB username")
        exit()

    sys.stdout.write("Parsing configuration data\n")
    config = configuration.configure_runtime(args.configuration)

    sys.stdout.write("Parsing sample data\n")
    libraries = configuration.configure_samples(args.samples_file, config)
    samples = configuration.merge_library_configs_samples(libraries)

    panels_list = list()

    for sample in samples:
        for library in samples[sample]:
            panel = samples[sample][library]['panel']
            if panel not in panels_list:
                sys.stdout.write(f"Adding Panel: {panel} to list of panels\n")
                panels_list.append(panel)

    sys.stdout.write("Processing VAF data for all variants in run and retrieving information from Database\n")
    run_vafs, vaf_db_stats = parse_run_vafs("run_var_frequencies_summary.csv", panels_list, [args.address], auth_provider)

    sys.stdout.write("Processing database coverage data\n")
    amplicon_stats = get_db_coverage_data(samples, panels_list, [args.address], auth_provider)

    pool = Pool(args.threads)
    sample_data = list()

    sys.stdout.write("Processing sample level data and generating reports\n")
    for sample in samples:
        for library in samples[sample]:
            data = ((samples, sample, library, run_vafs, amplicon_stats, vaf_db_stats, config, auth_provider, args.address))
            sample_data.append(data)

    results = pool.map(create_sample_report, sample_data)
