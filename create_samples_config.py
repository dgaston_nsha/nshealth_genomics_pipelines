#!/usr/bin/env python

# Standard packages
import os
import sys
import argparse

if __name__ == "__main__":
   parser = argparse.ArgumentParser()
   parser.add_argument('-i', '--input', help="Input SamplesSheet")
   parser.add_argument('-o', '--output', help="Output samples configuration file name")
   parser.add_argument('-p', '--panel', help="Panel name")
   parser.add_argument('-s', '--sequencer', help="Sequencer name")
   parser.add_argument('-r', '--run_id', help="Run ID")
   parser.add_argument('-f', '--fastq_dir', help="Full path to the FastQs directory")
   args = parser.parse_args()
   args.logLevel = "INFO"

   num_samples = 0
   sample_lines = list()
   with open(args.input, 'r') as samples_file:
       for sample_line in samples_file:
           num_samples += 1
           sample_lines.append(sample_line)

   with open(args.output, 'w') as output:
       lib_num = 1
       for sample_line in sample_lines:
           info = sample_line.split(',')
           if args.panel == "ts15":
               sample_name = info[0][:-1]
               pool = info[0][-1]
           else:
               sample_name = info[0]
               pool = ""
           library_name = f"{info[0]}_S{lib_num}"

           fastq1 = os.path.join(args.fastq_dir, f"{library_name}_L001_R1_001.fastq.gz")
           fastq2 = os.path.join(args.fastq_dir, f"{library_name}_L001_R2_001.fastq.gz")

           sys.stdout.write(f"Processing library {library_name}\n")
           output.write(f"[{library_name}]\n")
           output.write(f"fastq1: {fastq1}\n")
           output.write(f"fastq2: {fastq2}\n")
           output.write(f"library_name: {library_name}\n")
           output.write(f"sample_name: {sample_name}\n")
           output.write(f"extraction: default\n")
           output.write(f"panel: {args.panel}\n")

           if args.panel == "brca":
               output.write("report: brca.bed\n")
           elif args.panel == "ampliseq_focus":
               output.write("report: ampliseq_focus.bed\n")
           elif args.panel == "myeloid":
               output.write("report: new_comb_myeloid.bed\n")
           elif args.panel == "ts15":
               if pool == 'A':
                   output.write("report: reportingA.bed\n")
               elif pool == 'B':
                   output.write("report: reportingB.bed\n")
               else:
                   sys.stderr.write(f"Sample line {sample_line} has unknown pool {info[-2]}\n")
           else:
               output.write("report:\n")

           output.write(f"target_pool: {pool}\n")
           output.write(f"sequencer: {args.sequencer}\n")
           output.write(f"run_id: {args.run_id}\n")
           output.write(f"num_libraries_in_run: {num_samples}\n")

           if args.panel == "myeloid":
               output.write("regions: /mnt/shared-data/Resources/MiSeqPanels/trusight-myeloid.bed\n")
               output.write("vcfanno_config: /mnt/shared-data/ddb-configs/annotation/vcfanno-tsmyeloid.conf\n")
           elif args.panel == "brca":
               output.write("regions: /mnt/shared-data/Resources/MiSeqPanels/brca.bed\n")
               output.write("vcfanno_config: /mnt/shared-data/ddb-configs/annotation/vcfanno-brca.conf\n")
           elif args.panel == "ampliseq_focus":
               output.write("regions: /mnt/shared-data/Resources/MiSeqPanels/ampliseq_focus.bed\n")
               output.write("vcfanno_config: /mnt/shared-data/ddb-configs/annotation/vcfanno-ampliseq_focus.conf\n")
           elif args.panel == "ts15":
               if pool == 'A':
                   output.write("regions: /mnt/shared-data/Resources/MiSeqPanels/tst15-regionsA.bed\n")
                   output.write("vcfanno_config: /mnt/shared-data/ddb-configs/annotation/vcfanno-ts15A.conf\n")
               elif pool == 'B':
                   output.write("regions: /mnt/shared-data/Resources/MiSeqPanels/tst15-regionsB.bed\n")
                   output.write("vcfanno_config: /mnt/shared-data/ddb-configs/annotation/vcfanno-ts15B.conf\n")
               else:
                   sys.stderr.write(f"Sample line {sample_line} has unknown pool {info[-2]}\n")
           else:
               output.write("regions:\n")
               output.write("vcfanno_config:\n")
           output.write("\n")

           lib_num += 1
