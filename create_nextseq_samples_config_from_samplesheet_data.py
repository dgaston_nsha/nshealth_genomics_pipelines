#!/usr/bin/env python

# Standard packages
import os
import csv
import sys
import glob
import argparse

if __name__ == "__main__":
   parser = argparse.ArgumentParser()
   parser.add_argument('-s', '--sequencer', help="Sequencer name")
   parser.add_argument('-r', '--run_id', help="Run ID")
   parser.add_argument('-f', '--fastqs_dir', help="Full path to FastQs")
   parser.add_argument('-i', '--input', help="Pared down SampleSheet Data CSV")
   args = parser.parse_args()
   args.logLevel = "INFO"

   with open(f"{args.run_id}-DNA.config", 'w') as dna_output:
       with open(f"{args.run_id}-RNA.config", 'w') as rna_output:
           with open(args.input, 'r') as input:
               reader = csv.reader(input, delimiter=',', quotechar='"')
               next(reader)
               library_num = 0
               for row in reader:
                   library_num += 1
                   sample_name = row[0]
                   library_name = f"{row[0]}_{row[1]}"
                   fastq1 = f"{args.fastqs_dir}/{library_name}_R1_001.fastq.gz"
                   fastq2 = f"{args.fastqs_dir}/{library_name}_R2_001.fastq.gz"

                   if row[2] == "DNA":
                       output = dna_output
                   elif row[2] == "RNA":
                       output = rna_output
                   else:
                       sys.stdout.write(f"Error, could not determine output for {sample_name}\n")
                       continue

                   sys.stdout.write(f"Processing library {library_name}\n")
                   output.write(f"[{library_name}]\n")
                   output.write(f"fastq1: {fastq1}\n")
                   output.write(f"fastq2: {fastq2}\n")
                   output.write(f"library_name: {library_name}\n")
                   output.write(f"sample_name: {sample_name}\n")
                   output.write(f"extraction: default\n")
                   output.write(f"Index1:\n")
                   output.write(f"Index2:\n")
                   output.write(f"panel: {row[3]}\n")

                   if row[3] == "ampliseq_focus":
                       output.write("report: ampliseq_focus.bed\n")
                   elif row[3] == "myeloid":
                       output.write("report: new_comb_myeloid.bed\n")
                   else:
                       output.write("report:\n")

                   output.write(f"target_pool: \n")
                   output.write(f"sequencer: {args.sequencer}\n")
                   output.write(f"run_id: {args.run_id}\n")
                   output.write(f"num_libraries_in_run: \n")

                   if row[3] == "myeloid":
                       output.write("regions: /mnt/shared-data/Resources/MiSeqPanels/trusight-myeloid.bed\n")
                       output.write("vcfanno_config: /mnt/shared-data/ddb-configs/annotation/vcfanno-tsmyeloid.conf\n")
                   elif row[3] == "ampliseq_focus":
                       output.write("regions: /mnt/shared-data/Resources/MiSeqPanels/ampliseq_focus.bed\n")
                       output.write("vcfanno_config: /mnt/shared-data/ddb-configs/annotation/vcfanno-ampliseq_focus.conf\n")
                   else:
                       output.write("regions:\n")
                       output.write("vcfanno_config:\n")
                   output.write("\n")
