import execute

def bcftools_norm(ref, input, output):
    cmd = f"bcftools norm -f {ref} -m- -o {output} -O v {input}"
    execute.execute_cmd(cmd)
