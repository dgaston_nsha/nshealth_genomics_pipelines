import sys

def execute_cmd(cmd_string):
    sys.stdout.write(
        f"Running: {cmd_string}\n"
    )
    subprocess.run(
        f"{cmd_string}", stderr=subprocess.PIPE, shell=True
    )
