import execute

def snpEff(sample, input, output, build):
    cmd = f"snpEff -Xmx64g eff -strict -no-downstream -no-intergenic -lof -canon {build} {input} > {output}"
    execute.execute_cmd(cmd)
