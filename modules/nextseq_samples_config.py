#!/usr/bin/env python

import sys
import csv

def parse(file):
    data = []
    with open(file, 'r') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            data.append(dict(row))
    return data
