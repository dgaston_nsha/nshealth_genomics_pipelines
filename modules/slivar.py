import execute

def slivar_gnotate(input, output, gnotate):
    cmd = f"slivar expr --gnotate {gnotate} --out-vcf {output} -v {input}"
    execute.execute_cmd(cmd)
