#!/usr/bin/env python

import os
import csv
import sys
import argparse

from collections import defaultdict
from itertools import combinations

def editDistDP(str1, str2, m, n):
    # Create a table to store results of subproblems
    dp = [[0 for x in range(n + 1)] for x in range(m + 1)]

    # Fill d[][] in bottom up manner
    for i in range(m + 1):
        for j in range(n + 1):

            # If first string is empty, only option is to
            # insert all characters of second string
            if i == 0:
                dp[i][j] = j    # Min. operations = j

            # If second string is empty, only option is to
            # remove all characters of second string
            elif j == 0:
                dp[i][j] = i    # Min. operations = i

            # If last characters are same, ignore last char
            # and recur for remaining string
            elif str1[i-1] == str2[j-1]:
                dp[i][j] = dp[i-1][j-1]

            # If last character are different, consider all
            # possibilities and find minimum
            else:
                dp[i][j] = 1 + min(dp[i][j-1],        # Insert
                                   dp[i-1][j],        # Remove
                                   dp[i-1][j-1])    # Replace

    return dp[m][n]

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--output', help="Output file name",
                        default="BarcodeAssignments.txt")

    args = parser.parse_args()
    args.logLevel = "INFO"

    total_read_count = 0
    total_unknown_pair_count = 0

    with open(args.output, 'w') as outfile:
        outfile.write(f"Run\tIndex1\tIndex2\tCount\tSample or Index IDs\n")
        for root, dirs, files in os.walk("."):
            for dir in dirs:
                dir_path = os.path.join(root, dir)
                if os.path.exists(os.path.join(dir_path, "DemultiplexSummaryF1L1.txt")):
                    sample_barcodes = defaultdict(lambda: defaultdict(lambda: None))
                    index1_barcodes = {}
                    index2_barcodes = {}

                    run_read_count = 0
                    run_unknown_pair_count = 0
                    run_invalid_pair_count = 0

                    sys.stdout.write(f"##### Run: {dir} #####\n")
                    with open(os.path.join(dir_path, "SampleSheetUsed.csv"), 'r') as samplesheet:
                        reader = csv.reader(samplesheet, delimiter=',')
                        for skip in range(18):
                            next(reader)
                        for row in reader:
                            sample_barcodes[row[4]][row[6]] = row[0]
                            index1_barcodes[row[4]] = row[3]
                            index2_barcodes[row[6]] = row[5]

                    # Get Edit Distances of all Pairs of i5 and i7 indices
                    barcode1_edit_distances = defaultdict(lambda: defaultdict(dict))
                    barcode1_edit_distances_table = defaultdict(lambda: defaultdict(dict))
                    dist1_counts = defaultdict(int)
                    pairs = list(combinations(index1_barcodes, 2))

                    for pair in pairs:
                            dist = editDistDP(pair[0], pair[1], len(pair[0]), len(pair[1]))
                            barcode1_edit_distances[pair[0]][pair[1]] = dist
                            barcode1_edit_distances_table[pair[0]][pair[1]] = dist
                            barcode1_edit_distances_table[pair[0]][pair[1]] = dist
                            dist1_counts[dist] += 1

                    barcode2_edit_distances = defaultdict(lambda: defaultdict(dict))
                    barcode2_edit_distances_table = defaultdict(lambda: defaultdict(dict))
                    dist2_counts = defaultdict(int)
                    pairs = list(combinations(index2_barcodes, 2))

                    for pair in pairs:
                            dist = editDistDP(pair[0], pair[1], len(pair[0]), len(pair[1]))
                            barcode2_edit_distances[pair[0]][pair[1]] = dist
                            barcode2_edit_distances_table[pair[0]][pair[1]] = dist
                            barcode2_edit_distances_table[pair[0]][pair[1]] = dist
                            dist2_counts[dist] += 1

                    with open(os.path.join(dir_path, "DemultiplexSummaryF1L1.txt"), 'r') as demultiplex:
                        reader = csv.reader(demultiplex, delimiter=',')
                        header = next(reader)

                        index1_unknown_edit_distance_counts = defaultdict(int)
                        index2_unknown_edit_distance_counts = defaultdict(int)
                        for row in reader:
                            total_read_count += int(row[4])
                            run_read_count += int(row[4])

                            if "." in row[0] or "." in row[2]:
                                continue

                            if sample_barcodes[row[0]][row[2]]:
                                outfile.write(f"{dir}\t{row[0]}\t{row[2]}\t{row[4]}\t{sample_barcodes[row[0]][row[2]]}\n")
                            else:
                                total_unknown_pair_count += int(row[4])
                                run_unknown_pair_count += int(row[4])

                                outfile.write(f"{dir}\t{row[0]}\t{row[2]}\t{row[4]}\t")
                                if index1_barcodes.get(row[0]):
                                    outfile.write(f"{index1_barcodes[row[0]]} - ")
                                else:
                                    outfile.write(f"Unknown - ")
                                    for i1 in index1_barcodes:
                                        dist = editDistDP(i1, row[0], len(i1), len(row[0]))
                                        index1_unknown_edit_distance_counts[dist] += int(row[4])

                                if index2_barcodes.get(row[2]):
                                    outfile.write(f"{index2_barcodes[row[2]]}\n")
                                else:
                                    outfile.write(f"Unknown\n")
                                    for i2 in index2_barcodes:
                                        dist = editDistDP(i2, row[2], len(i2), len(row[2]))
                                        index2_unknown_edit_distance_counts[dist] += int(row[4])

                                if index1_barcodes.get(row[0]) and index2_barcodes.get(row[2]):
                                    run_invalid_pair_count += int(row[4])


                    sys.stdout.write(f"Number of reads: {run_read_count}\n")
                    sys.stdout.write(f"Number with Unknown Barcode: {run_unknown_pair_count}\n")
                    sys.stdout.write(f"Percentage Unknown: {100 * float(run_unknown_pair_count) / float(run_read_count)}\n")
                    sys.stdout.write(f"Number of reads with invalid barcode pair: {run_invalid_pair_count}\n")
                    sys.stdout.write(f"Percentage Unknown: {100 * float(run_invalid_pair_count) / float(run_read_count)}\n\n")

                    sys.stdout.write(f"Index Set 1 Edit Distance Distribution:\n")
                    for dist in dist1_counts:
                        sys.stdout.write(f"{dist}: {dist1_counts[dist]}\n")
                    sys.stdout.write("\n")

                    sys.stdout.write(f"Index Set 2 Edit Distance Distribution:\n")
                    for dist in dist2_counts:
                        sys.stdout.write(f"{dist}: {dist2_counts[dist]}\n")
                    sys.stdout.write("\n")

                    sys.stdout.write(f"Unknown Index 1 Edit Distance Distribution:\n")
                    for dist in index1_unknown_edit_distance_counts:
                        sys.stdout.write(f"{dist}: {index1_unknown_edit_distance_counts[dist]}\n")
                    sys.stdout.write("\n")

                    sys.stdout.write(f"Unknown Index 2 Edit Distance Distribution:\n")
                    for dist in index2_unknown_edit_distance_counts:
                        sys.stdout.write(f"{dist}: {index2_unknown_edit_distance_counts[dist]}\n")
                    sys.stdout.write("\n")

    sys.stdout.write(f"##### All Runs #####\n")
    sys.stdout.write(f"Number of reads: {total_read_count}\n")
    sys.stdout.write(f"Number with Unknown Barcode: {total_unknown_pair_count}\n")
    sys.stdout.write(f"Percentage Unknown: {100 * float(total_unknown_pair_count) / float(total_read_count)}\n\n")
