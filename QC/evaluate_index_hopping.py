#!/usr/bin/env python

import os
import csv
import sys
import argparse

from collections import defaultdict
from itertools import combinations

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--output', help="Output file name",
                        default="BarcodeAssignments.txt")

    args = parser.parse_args()
    args.logLevel = "INFO"

    total_read_count = 0
    total_unknown_pair_count = 0

    with open(args.output, 'w') as outfile:
        outfile.write(f"Run\tIndex1\tIndex2\tCount\tSample or Index IDs\n")
        for root, dirs, files in os.walk("."):
            for dir in dirs:
                dir_path = os.path.join(root, dir)
                if os.path.exists(os.path.join(dir_path, "DemultiplexSummaryF1L1.txt")):
                    sample_barcodes = defaultdict(lambda: defaultdict(lambda: None))
                    index1_barcodes = {}
                    index2_barcodes = {}

                    run_read_count = 0
                    run_invalid_pair_count = 0

                    sys.stdout.write(f"##### Run: {dir} #####\n")
                    with open(os.path.join(dir_path, "SampleSheetUsed.csv"), 'r') as samplesheet:
                        reader = csv.reader(samplesheet, delimiter=',')
                        for skip in range(18):
                            next(reader)
                        for row in reader:
                            sample_barcodes[row[4]][row[6]] = row[0]
                            index1_barcodes[row[4]] = row[3]
                            index2_barcodes[row[6]] = row[5]

                    with open(os.path.join(dir_path, "DemultiplexSummaryF1L1.txt"), 'r') as demultiplex:
                        reader = csv.reader(demultiplex, delimiter='\t')
                        header = next(reader)

                        for row in reader:
                            if "." in row[0] or "." in row[2]:
                                continue

                            if sample_barcodes[row[0]][row[2]]:
                                outfile.write(f"{dir}\t{row[0]}\t{row[2]}\t{row[4]}\t{sample_barcodes[row[0]][row[2]]}\n")
                                run_read_count += int(row[4])
                            else:
                                if index1_barcodes.get(row[0]) and index2_barcodes.get(row[2]):
                                    outfile.write(f"{dir}\t{row[0]}\t{row[2]}\t{row[4]}\t{index1_barcodes[row[0]]} - {index2_barcodes[row[2]]} - Index Hop\n")
                                    run_read_count += int(row[4])
                                    run_invalid_pair_count += int(row[4])


                    sys.stdout.write(f"Number of reads with barcodes from run: {run_read_count}\n")
                    sys.stdout.write(f"Number of reads with invalid barcode pair: {run_invalid_pair_count}\n")
                    sys.stdout.write(f"Percentage Invalid: {100 * float(run_invalid_pair_count) / float(run_read_count)}\n\n")
