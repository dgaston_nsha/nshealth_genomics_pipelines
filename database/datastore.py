from cassandra.cqlengine import columns
from cassandra.cqlengine.models import Model

VERSION = 1.0

class Target(Model):
    __keyspace__ = 'datastore'
    target_name = columns.Text(primary_key=True, partition_key=True)
    panel_name = columns.Text(primary_key=True)
    chromosome = columns.Text()
    start = columns.Integer()
    end = columns.Integer()

class TargetCovData(Model):
    __keyspace__ = 'datastore'
    target_name = columns.Text(primary_key=True, partition_key=True)
    panel_name = columns.Text(primary_key=True)
    sequencer = columns.Text(primary_key=True)
    sample = columns.Text(primary_key=True)
    library_name = columns.Text(primary_key=True)

    num_dna_samples_run = columns.Integer()
    num_reads = columns.Integer()
    mean_coverage = columns.Float()
    percent_bp_100 = columns.Float()
    percent_bp_200 = columns.Float()
    percent_bp_500 = columns.Float()
    percent_bp_1000 = columns.Float()
    percent_bp_1500 = columns.Float()
    percent_bp_2000 = columns.Float()

class Variant(Model):
    __keyspace__ = 'datastore'
    variant_id = columns.Text(primary_key=True, partition_key=True)
    chromosome = columns.Text()
    ref = columns.Text()
    alt = columns.Text()
    gene = columns.Text()
    ensembl_transcript = columns.Text()
    refseq_transcript = columns.Text()
    hgvs_c = columns.Text()
    hgvs_g = columns.Text()
    hgvs_p = columns.Text()
    is_artifact_site = columns.Boolean()
    is_hotspot_site = columns.Boolean()
    is_reportable = columns.Boolean()
    is_benign = columns.Boolean()
    artifact_hotspot_vaf_cutoff = columns.Float()
    interpretations = columns.Text()

class VariantCallerVAFs(Model):
    __keyspace__ = 'datastore'
    variant_id = columns.Text(primary_key=True, partition_key=True)
    panel_name = columns.Text(primary_key=True)
    sequencer = columns.Text(primary_key=True)
    sample = columns.Text(primary_key=True)
    library_name = columns.Text(primary_key=True)

    max_vaf = columns.Float()
    min_vaf = columns.Float()
    freebayes_vaf = columns.Float()
    mutect_vaf = columns.Float()
    mutect2_vaf = columns.Float()
    scalpel_vaf = columns.Float()
    pindel_vaf = columns.Float()
    platypus_vaf = columns.Float()
    vardict_vaf = columns.Float()

class VariantStats(Model):
    __keyspace__ = 'datastore'
    variant_id = columns.Text(primary_key=True, partition_key=True)
    panel_name = columns.Text(primary_key=True)
    sequencer = columns.Text(primary_key=True)

    num_times_db = columns.Integer()
    median_max_vaf = columns.Float()
    median_min_vaf = columns.Float()
    median_freebayes_vaf = columns.Float()
    median_mutect_vaf = columns.Float()
    median_mutect2_vaf = columns.Float()
    median_scalpel_vaf = columns.Float()
    median_pindel_vaf = columns.Float()
    median_platypus_vaf = columns.Float()
    median_vardict_vaf = columns.Float()

    std_max_vaf = columns.Float()
    std_min_vaf = columns.Float()
    std_freebayes_vaf = columns.Float()
    std_mutect_vaf = columns.Float()
    std_mutect2_vaf = columns.Float()
    std_scalpel_vaf = columns.Float()
    std_pindel_vaf = columns.Float()
    std_platypus_vaf = columns.Float()
    std_vardict_vaf = columns.Float()
