from cassandra.cqlengine import columns
from cassandra.cqlengine.models import Model

class GeneVariant(Model):
    __keyspace__ = 'variantstore_gene'

    gene = columns.Text(primary_key=True, partition_key=True)
    reference_genome = columns.Text(primary_key=True, partition_key=True)
    library_name = columns.Text(primary_key=True)

    chr = columns.Text(primary_key=True)
    pos = columns.Integer(primary_key=True)

    # Cluster Keys
    ref = columns.Text(primary_key=True)
    alt = columns.Text(primary_key=True)

    callers = columns.List(columns.Text)
    codon_change = columns.Text()
    aa_change = columns.Text()
    severity = columns.Text()
    impact = columns.Text()

    population_freqs = columns.Map(columns.Text, columns.Float)
    amplicon_data = columns.Map(columns.Text, columns.Text)
    min_depth = columns.Float()
    max_depth = columns.Float()
    min_som_aaf = columns.Float()
    max_som_aaf = columns.Float()

    # Variant Caller Data
    freebayes = columns.Map(columns.Text, columns.Text)
    mutect = columns.Map(columns.Text, columns.Text)
    scalpel = columns.Map(columns.Text, columns.Text)
    vardict = columns.Map(columns.Text, columns.Text)
    pindel = columns.Map(columns.Text, columns.Text)
    platypus = columns.Map(columns.Text, columns.Text)
