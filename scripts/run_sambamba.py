#!/usr/bin/env python

# Standard packages
import sys
import csv
import argparse
import subprocess

from collections import defaultdict

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', help="Input file for samples")
    parser.add_argument('-t', '--targets', help="BED format file of targets to assess for coverage")
    parser.add_argument('-n', '--num_cores', help="Number of CPU threads to use")
    args = parser.parse_args()
    args.logLevel = "INFO"

    cmd = f"sambamba depth region"
    ref = f"/mnt/shared-data/Resources/ReferenceData/GRCh38/Homo_sapiens_assembly38.fasta"

    with open(args.input, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t')
        header = next(reader)
        for row in reader:
            sample = row[0]

            bam = f"{sample}.recalibrated.bam"
            output = f"{sample}.coverage.bed"

            sys.stdout.write(
                f"Running: {cmd} -L {args.targets} -t {args.num_cores} -o {output} -T 10 -T 20 -T 50 -T 100 -T 200 -T 500 -T 1000 {bam}\n"
            )
            subprocess.run(
                f"{cmd} -L {args.targets} -t {args.num_cores} -o {output} -T 10 -T 20 -T 50 -T 100 -T 200 -T 500 -T 1000 {bam}", stderr=subprocess.PIPE, shell=True
            )
