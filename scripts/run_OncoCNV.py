#!/usr/bin/env python

# Standard packages
import os
import sys
import argparse
import subprocess

from modules import configuration


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--samples_file', help="Input file for samples")
    parser.add_argument('-c', '--configuration', help="Configuration file for various settings")
    parser.add_argument('-t', '--threads', help="Number of Threads", default=24)

    args = parser.parse_args()
    args.logLevel = "INFO"

    sys.stdout.write("Parsing configuration data\n")
    config = configuration.configure_runtime(args.configuration)

    sys.stdout.write("Parsing sample data\n")
    samples = configuration.configure_samples(args.samples_file, config)

    pwd = os.getcwd()

    sys.stdout.write("Running Analyses...\n")
    for sample in samples:
        oncocnv_cmd = f""
        sys.stdout.write(f"Running: {oncocnv_cmd}\n")
        subprocess.run(f"{oncocnv_cmd}", stderr=subprocess.PIPE, shell=True)
