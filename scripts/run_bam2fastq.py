#!/usr/bin/env python

# Standard packages
import sys
import glob
import argparse
import subprocess

from collections import defaultdict

if __name__ == "__main__":
    bam_files = glob.glob('*.bam')
    for bam in bam_files:
        elements = bam.split('.bam')
        sample = elements[0]
        read_sorted = f"{sample}.readsorted.bam"
        fastq1 = f"{sample}.R1.fastq.gz"
        fastq2 = f"{sample}.R2.fastq.gz"

        sys.stdout.write(
            f"Running: samtools sort -@ 24 -n -o {read_sorted} {bam}\n"
        )
        subprocess.run(
            f"samtools sort -@ 24 -n -o {read_sorted} {bam}", stderr=subprocess.PIPE, shell=True
        )

        sys.stdout.write(
            f"Running: samtools fastq -1 {fastq1} -2 {fastq2} {read_sorted}\n"
        )
        subprocess.run(
            f"samtools fastq -1 {fastq1} -2 {fastq2} {read_sorted}", stderr=subprocess.PIPE, shell=True
        )
