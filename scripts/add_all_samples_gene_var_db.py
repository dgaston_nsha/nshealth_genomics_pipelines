#!/usr/bin/env python

# Standard packages
import os
import re
import sys
import fnmatch
import cyvcf2
import getpass
import argparse

from database import utils_updated as utils
from modules import vcf_parsing

from cyvcf2 import VCF
from datetime import datetime
from multiprocessing import Pool
from collections import defaultdict

from cassandra import WriteFailure
from cassandra import InvalidRequest
from cassandra.cqlengine import connection
from cassandra.auth import PlainTextAuthProvider

from database.varstore_by_gene import GeneVariant

def add_sample(sample_data):
    connection.setup([sample_data[-2]], "variantstore_research", auth_provider=sample_data[-1])

    library_id = sample_data[0]
    root = sample_data[1]

    parse_functions = {'mutect': vcf_parsing.parse_mutect_vcf_record,
                       'freebayes': vcf_parsing.parse_freebayes_vcf_record,
                       'vardict': vcf_parsing.parse_vardict_vcf_record,
                       'scalpel': vcf_parsing.parse_scalpel_vcf_record,
                       'platypus': vcf_parsing.parse_platypus_vcf_record,
                       'pindel': vcf_parsing.parse_pindel_vcf_record}

    caller_records = defaultdict(lambda: dict())

    vcf_parsing.parse_vcf(os.path.join(root, f"{library_id}.mutect.normalized.vcf"), "mutect", caller_records)
    vcf_parsing.parse_vcf(os.path.join(root, f"{library_id}.vardict.normalized.vcf"), "vardict", caller_records)
    vcf_parsing.parse_vcf(os.path.join(root, f"{library_id}.freebayes.normalized.vcf"), "freebayes", caller_records)
    vcf_parsing.parse_vcf(os.path.join(root, f"{library_id}.scalpel.normalized.vcf"), "scalpel", caller_records)
    vcf_parsing.parse_vcf(os.path.join(root, f"{library_id}.platypus.normalized.vcf"), "platypus", caller_records)
    vcf_parsing.parse_vcf(os.path.join(root, f"{library_id}.pindel.normalized.vcf"), "pindel", caller_records)

    annotated_vcf = os.path.join(root, f"{library_id}.vcfanno.snpEff.GRCh37.75.vcf")

    vcf = VCF(annotated_vcf)

    reader = cyvcf2.VCFReader(annotated_vcf)
    desc = reader["ANN"]["Description"]
    annotation_keys = [x.strip("\"'") for x in re.split("\s*\|\s*", desc.split(":", 1)[1].strip('" '))]

    for variant in vcf:
        # Parsing VCF and creating data structures for Cassandra model
        callers = variant.INFO.get('CALLERS').split(',')
        effects = utils.get_effects(variant, annotation_keys)
        top_impact = utils.get_top_impact(effects)
        population_freqs = utils.get_population_freqs(variant)
        amplicon_data = utils.get_amplicon_data(variant)

        key = (str("chr{}".format(variant.CHROM)), int(variant.start), int(variant.end), str(variant.REF),
               str(variant.ALT[0]))

        caller_variant_data_dicts = defaultdict(dict)
        max_som_aaf = -1.00
        max_depth = -1
        min_depth = 100000000

        for caller in callers:
            caller_variant_data_dicts[caller] = parse_functions[caller](caller_records[caller][key])
            if float(caller_variant_data_dicts[caller]['AAF']) > max_som_aaf:
                max_som_aaf = float(caller_variant_data_dicts[caller]['AAF'])
            if int(caller_variant_data_dicts[caller]['DP']) < min_depth:
                min_depth = int(caller_variant_data_dicts[caller]['DP'])
            if int(caller_variant_data_dicts[caller]['DP']) > max_depth:
                max_depth = int(caller_variant_data_dicts[caller]['DP'])

        if min_depth == 100000000:
            min_depth = -1

        fail_filter = False
        if max_depth < 100:
            fail_filter = True
        if max_som_aaf < 0.01:
            fail_filter = True
        if len(callers) == 1:
            fail_filter = True

        if fail_filter:
            continue

        try:
            cassandra_variant = GeneVariant.create(
                gene=top_impact.gene,
                reference_genome="GRCh37.75",
                library_name=library_id,
                chr=variant.CHROM,
                pos=variant.start,
                ref=variant.REF,
                alt=variant.ALT[0],
                callers=callers,
                codon_change=top_impact.codon_change,
                aa_change=top_impact.aa_change,
                severity=top_impact.effect_severity,
                impact=top_impact.top_consequence,
                population_freqs=population_freqs,
                amplicon_data=amplicon_data,
                max_som_aaf=max_som_aaf,
                min_depth=min_depth,
                max_depth=max_depth,
                mutect=caller_variant_data_dicts['mutect'] or dict(),
                freebayes=caller_variant_data_dicts['freebayes'] or dict(),
                scalpel=caller_variant_data_dicts['scalpel'] or dict(),
                platypus=caller_variant_data_dicts['platypus'] or dict(),
                pindel=caller_variant_data_dicts['pindel'] or dict(),
                vardict=caller_variant_data_dicts['vardict'] or dict()
            )
        except KeyError:
            sys.stderr.write(f"KeyError: {library_id}\n")
        except WriteFailure:
            with open(f"{library_id}.sample_variant_add.log", "a") as err:
                err.write("Failed to write variant to variantstore:\n")
                err.write(f"Library: {library_id}\n")
                err.write(f"{variant}\n")
        except InvalidRequest:
            with open(f"{library_id}.sample_variant_add.log", "a") as err:
                err.write("Failed to write variant to variantstore:\n")
                err.write(f"Library: {library_id}\n")
                err.write(f"{variant}\n")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--address', help="IP Address for Cassandra connection", default='127.0.0.1')
    parser.add_argument('-u', '--username', help='Cassandra username for login', default=None)
    args = parser.parse_args()
    args.logLevel = "INFO"

    num_cores = 24

    if args.username:
        password = getpass.getpass()
        auth_provider = PlainTextAuthProvider(username=args.username, password=password)
    else:
        auth_provider = None

    pool = Pool(num_cores)
    samples_data = list()
    for root, dirs, files in os.walk("."):
        for sample_vcf in fnmatch.filter(files, "*.vcfanno.snpEff.GRCh37.75.vcf"):
            elements = sample_vcf.split('.')
            library_id = elements[0]
            samples_data.append([library_id, root, args.address, auth_provider])

    results = pool.map(add_sample, samples_data)
