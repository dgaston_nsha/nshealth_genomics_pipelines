#!/usr/bin/env python
import argparse

from cyvcf2 import VCF, Writer

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', help="Input VCF file")
    parser.add_argument('-o', '--output', help="Output VCF file")
    args = parser.parse_args()
    args.logLevel = "INFO"

    input_vcf = VCF(args.input)
    output_vcf = Writer(args.output, input_vcf)


    for variant in input_vcf:
        gnomad_af = variant.INFO.get('gnomad_popmax_af')
        topmed_af = variant.INFO.get('topmed_af')
        qd = variant.INFO.get('QD')
        depth = variant.INFO.get('DP')
        sor = variant.INFO.get('SOR')
        rmsq = variant.INFO.get('MQ')
        annotations = variant.INFO.get('ANN')
        if gnomad_af < 0.01 and topmed_af < 0.01 and qd > 2 and depth > 20:
            if sor < 3 and rmsq > 40:
                if annotations:
                    output_vcf.write_record(variant)

    output_vcf.close()
    input_vcf.close()
