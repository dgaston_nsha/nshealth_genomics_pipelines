#!/usr/bin/env python

import getpass
import argparse

from cassandra.cluster import Cluster
from cassandra.cqlengine import connection
from cassandra.auth import PlainTextAuthProvider

from database.variantstore_research import Variant

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-g', '--gene', help="Gene name to query for")
    parser.add_argument('-a', '--address', help="IP Address for Cassandra connection",
    default='127.0.0.1')
    parser.add_argument('-u', '--username', help='Cassandra username for login',
    default=None)
    parser.add_argument('-o', '--outfile', help="Output file name")
    args = parser.parse_args()
    args.logLevel = "INFO"

    if args.username:
        password = getpass.getpass()
        auth_provider = PlainTextAuthProvider(username=args.username, password=password)
    else:
        auth_provider = None

    tier1_clinvar_terms = ("pathogenic", "likely-pathogenic", "drug-response")

    with open(args.outfile, 'w') as output_file:
        output_file.write(f"Sample\t"
                          f"Library\t"
                          f"Run\t"
                          f"Gene\t"
                          f"Chr\t"
                          f"Start\t"
                          f"End\t"
                          f"Ref\t"
                          f"Alt\t"
                          f"Codon\t"
                          f"AA\t"
                          f"VAF\t"
                          f"Callers\t"
                          f"Num COSMIC\t"
                          f"COSMIC IDs\t"
                          f"COSMIC AAs\t"
                          f"ClinVar Sig\t"
                          f"ClinVar HGVS\t"
                          f"ClinVar Disease\t"
                          f"Max Pop AF\t"
                          f"Impact\t"
                          f"Severity\t"
                          f"Min Caller Depth\t"
                          f"Max Caller Depth\t"
                          f"rsIDs\n")
        connection.setup(['localhost'], "variantstore_research", auth_provider=auth_provider)
        variants = Variant.objects(gene=args.gene).timeout(None).limit(None)
        print(f"There are {variants.count()} results for gene: {args.gene}\n")
        passing = 0
        iterated = 0
        for variant in variants:
            iterated += 1
            if variant.max_maf_all <= 0.005:
                reportable = False
                if variant.cosmic_ids:
                    reportable = True
                if any(i in tier1_clinvar_terms for i in variant.clinvar_data['significance']):
                    reportable = True
                if variant.severity == 'HIGH':
                    reportable = True
                if variant.severity == 'MED':
                    reportable = True

                if reportable:
                    passing += 1
                    output_file.write(f"{variant.sample}\t"
                                      f"{variant.library_name}\t"
                                      f"{variant.run_id}\t"
                                      f"{variant.gene}\t"
                                      f"{variant.chr}\t"
                                      f"{variant.pos}\t"
                                      f"{variant.end}\t"
                                      f"{variant.ref}\t"
                                      f"{variant.alt}\t"
                                      f"{variant.codon_change}\t"
                                      f"{variant.aa_change}\t"
                                      f"{variant.max_som_aaf}\t"
                                      f"{','.join(variant.callers)}\t"
                                      f"{variant.cosmic_data['num_samples']}\t"
                                      f"{','.join(variant.cosmic_ids)}\t"
                                      f"{variant.cosmic_data['aa']}\t"
                                      f"{variant.clinvar_data['significance']}\t"
                                      f"{variant.clinvar_data['hgvs']}\t"
                                      f"{variant.clinvar_data['disease']}\t"
                                      f"{variant.max_maf_all}\t"
                                      f"{variant.impact}\t"
                                      f"{variant.severity}\t"
                                      f"{variant.min_depth}\t"
                                      f"{variant.max_depth}\t"
                                      f"{','.join(variant.rs_ids)}\n")


        print(f"There were {passing} pasing variants of {iterated} iterated over\n")
