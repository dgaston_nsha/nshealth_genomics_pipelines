#!/usr/bin/env python
import camelot
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', help="Input PDF")
    parser.add_argument('-o', '--output', help="Output Excel File")
    parser.add_argument('-p', '--pages', help="String of pages to extract from")
    args = parser.parse_args()
    args.logLevel = "INFO"

    tables = camelot.read_pdf(args.input, pages=args.pages)
    tables.export(args.output, f='excel')
