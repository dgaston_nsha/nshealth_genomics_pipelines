#!/usr/bin/env python

import sys
import csv
import getpass
import argparse

import numpy as np
from cassandra.cluster import Cluster
from cassandra.cqlengine import connection
from cassandra.auth import PlainTextAuthProvider

from database.variantstore import Variant


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-l', '--list', help="File containing list of variants to check")
    parser.add_argument('-r', '--report', help="Root name for reports", default='report')
    parser.add_argument('-a', '--address', help="IP Address for Cassandra connection", default='127.0.0.1')
    parser.add_argument('-u', '--username', help='Cassandra username for login', default=None)
    args = parser.parse_args()
    args.logLevel = "INFO"

    if args.username:
        password = getpass.getpass()
        auth_provider = PlainTextAuthProvider(username=args.username, password=password)
        connection.setup([args.address], "variantstore", auth_provider=auth_provider)
    else:
        connection.setup([args.address], "variantstore")

    sys.stdout.write("Proccessing through selected variants\n")
    with open(args.list, "r") as variants_list:
        with open("variant_analysis_tracking_{}.txt".format(args.report), "w") as output:
            output.write("Sample\tLibrary\tRun\tGene\tAmplicon\tRef\tAlt\tCodon\tAA\t"
                         "max_somatic_aaf\tCallers\n")
            reader = csv.reader(variants_list, dialect='excel-tab')
            for row in reader:
                # sys.stdout.write("Processing row: {}\n".format(row))
                match_variants = Variant.objects.timeout(None).filter(
                    Variant.reference_genome == "GRCh37.75",
                    Variant.chr == row[0],
                    Variant.pos == row[1],
                    Variant.ref == row[2],
                    Variant.alt == row[3]
                ).allow_filtering()

                num_matches = match_variants.count()
                ordered_var = match_variants.order_by('pos', 'ref', 'alt', 'sample', 'library_name',
                                                      'run_id').limit(num_matches + 1000)

                for var in ordered_var:
                    output.write(f"{var.sample}\t{var.library_name}\t{var.run_id}\t{var.gene}\t{var.amplicon_data['amplicon']}\t{var.ref}\t{var.alt}\t{var.codon_change}\t{var.aa_change}\t{var.max_som_aaf}\t{var.callers}\n")
