#!/usr/bin/env python

# Standard packages
import sys
import csv
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', help="Input file")
    parser.add_argument('-o', '--output', help="Output file name")
    args = parser.parse_args()
    args.logLevel = "INFO"

    with open(args.input, 'r') as inputfile:
        reader = csv.reader(inputfile, delimiter='\t')
        with open(args.output, 'w') as outfile:
            for row in reader:
                if row[2].startswith("rs"):
                    info = row[-1]
                    elements = info.split(';')
                    af_strings = elements[1].split(',')
                    for af_string in af_strings:
                        af = float(af_string[3:])
                        if af > 0.10:
                            string = "\t".join(row)
                            outfile.write(f"{string}\n")