#!/usr/bin/env python

import os
import sys
import csv
import glob
import subprocess

if __name__ == "__main__":
    vcfs = glob.glob('*.freebayes.low_support_filtered.vcf')
    for vcf in vcfs:
        sys.stdout.write(f"Processing {vcf}\n")
        backup = f"{vcf}.bk"
        os.rename(vcf, backup)
        with open(backup, 'r') as input_file:
            with open(vcf, 'w') as output_file:
                reader = csv.reader(input_file, delimiter='\t')
                for row in reader:
                    if row[0].startswith("#"):
                        outstr = "\t".join(row)
                        output_file.write(f"{outstr}\n")
                    else:
                        fields = row[-1].split(':')
                        gq_float = float(fields[1])
                        gq_int = int(gq_float)
                        field_string = f"{fields[0]}:{gq_int}:{fields[2]}:{fields[3]}:{fields[4]}:{fields[5]}:{fields[6]}:{fields[7]}"
                        output_row_string = f"{row[0]}\t{row[1]}\t{row[2]}\t{row[3]}\t{row[4]}\t{row[5]}\t{row[6]}\t{row[7]}\t{row[8]}\t{field_string}"
                        output_file.write(f"{output_row_string}\n")

        cmd = f"bgzip -c {vcf} > {vcf}.gz"
        sys.stdout.write(f"Running bgzip: {cmd}\n")
        subprocess.run(f"{cmd}", stderr=subprocess.PIPE, shell=True)

        cmd = f"tabix -p vcf {vcf}.gz"
        sys.stdout.write(f"Running tabix: {cmd}\n")
        subprocess.run(f"{cmd}", stderr=subprocess.PIPE, shell=True)
