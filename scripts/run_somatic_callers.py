#!/usr/bin/env python

# Standard packages
import sys
import csv
import argparse
import subprocess

from multiprocessing import Pool
from collections import defaultdict

def execute_cmd(cmd_string):
    sys.stdout.write(
        f"Running: {cmd_string}\n"
    )
    subprocess.run(
        f"{cmd_string}", stderr=subprocess.PIPE, shell=True
    )

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', help="Input file for samples")
    args = parser.parse_args()
    args.logLevel = "INFO"

    ref = f"/mnt/shared-data/Resources/ReferenceData/GRCh38/Homo_sapiens_assembly38.fasta"
    gnomad = f"/mnt/shared-data/Resources/ReferenceData/GRCh38/af-only-gnomad.hg38.vcf.gz"
    dbsnp = f"/mnt/shared-data/Resources/ReferenceData/GRCh38/dbsnp_146.hg38.vcf.gz"

    pool = Pool(3)
    mutect2_commands = []
    lofreq_commands = []
    pisces_commands = []
    freebayes_commands = []

    with open(args.input, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t')
        header = next(reader)
        for row in reader:
            sample = row[0]
            bam = f"{sample}.recalibrated.bam"

            mutect2_string = f"gatk Mutect2 -R {ref} -I {bam} -O {sample}.mutect2.vcf --germline-resource {gnomad} --native-pair-hmm-threads 8 --f1r2-tar-gz {f1r2} -G StandardMutectAnnotation"
            mutect2_commands.append(mutect2_string)

            lofreq_string = f"lofreq call-parallel --call-indels --pp-threads 8 -f {ref} -o {sample}.lofreq.vcf -s -S {dbsnp} {bam}"
            lofreq_commands.append(lofreq_string)

            pisces_string = f"dotnet Pisces.dll -bam {bam} -g {ref} -gVCF false -OutFolder {sample}_pisces_out -RMxNFilter 5,9,0.35 -CallMNVs false"
            pisces_commands.append(pisces_string)

            freebayes_string = f"freebayes"
            freebayes_commands.append(freebayes_string)

        mutect2_results = pool.map(execute_cmd, mutect2_commands)
        lofreq_results = pool.map(execute_cmd, lofreq_commands)
        pisces_results = pool.map(execute_cmd, pisces_commands)
        freebayes_results = pool.map(execute_cmd, freebayes_commands)
