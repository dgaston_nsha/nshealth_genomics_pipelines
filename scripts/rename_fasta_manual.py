#!/usr/bin/env python

import argparse

from Bio import SeqIO

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', help="Input FASTA file")
    parser.add_argument('-o', '--output', help="Output filename")
    args = parser.parse_args()
    args.logLevel = "INFO"

    with open(args.input) as in_fasta:
        with open(args.output, 'w') as out_fasta:
            for record in SeqIO.parse(in_fasta, "fasta"):
                print(f"{record.id}, {record.name}, {record.description}")
                new_name = input("Input new sequence record name: ")
                out_fasta.write(f">{new_name}\n{record.seq}\n")
