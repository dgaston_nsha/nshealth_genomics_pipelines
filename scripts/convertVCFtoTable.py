#!/usr/bin/env python
import csv
import argparse

from cyvcf2 import VCF

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', help="Input VCF file")
    parser.add_argument('-o', '--output', help="Output file root")
    args = parser.parse_args()
    args.logLevel = "INFO"

    input_vcf = VCF(args.input)

    impact_fh = f"{args.output}_med_high.txt"
    low_fh = f"{args.output}_low.txt"
    mod_fh = f"{args.output}_modifier.txt"

    samples = input_vcf.samples

    header = ["Chr", "Pos", "Ref", "Alt", "Filter", "Gene", "Transcript", "HGVSc", "HGVSp", "Impact", "ClinVar_ID",
              "ClinVar_Disease", "ClinVar_Origin", "ClinVar_Sig", "ClinVar_Path", "COSMIC_ID", "COSMIC_Num",
              "GnomAD_AF", "TopMed_AF", "Annotations"]

    for sample in samples:
        header.extend([f"{sample}_Ref_Cnt", f"{sample}_Alt_Cnt", f"{sample}_DP", f"{sample}_AF"])

    with open(impact_fh, 'w') as impact_file:
        with open(low_fh, 'w') as low_file:
            with open(mod_fh, 'w') as mod_file:
                impact_writer = csv.DictWriter(impact_file, fieldnames=header, restval='.', dialect='excel-tab')
                low_writer = csv.DictWriter(low_file, fieldnames=header, restval='.', dialect='excel-tab')
                mod_writer = csv.DictWriter(mod_file, fieldnames=header, restval='.', dialect='excel-tab')

                impact_writer.writeheader()
                low_writer.writeheader()
                mod_writer.writeheader()

                for variant in input_vcf:
                    annotations_string = variant.INFO.get('ANN')
                    transcript_annotations = annotations_string.split(',')

                    for annotation_line in transcript_annotations:
                        annotations = annotation_line.split('|')
                        impact = str(annotations[2])

                        var_dict = dict()
                        var_dict['Chr'] = variant.CHROM
                        var_dict['Pos'] = variant.start
                        var_dict['Ref'] = variant.REF
                        var_dict['Alt'] = variant.ALT[0]
                        var_dict['Filter'] = variant.FILTER
                        var_dict['Gene'] = annotations[3]
                        var_dict['Transcript'] = annotations[6]
                        var_dict['HGVSc'] = annotations[9]
                        var_dict['HGVSp'] = annotations[10]
                        var_dict['Impact'] = annotations[2]
                        var_dict['ClinVar_ID'] = variant.INFO.get('clinvar_alleleid')
                        var_dict['ClinVar_Disease'] = variant.INFO.get('clinvar_diseasename')
                        var_dict['ClinVar_Origin'] = variant.INFO.get('clinvar_origin')
                        var_dict['ClinVar_Sig'] = variant.INFO.get('clinvar_significance')
                        var_dict['ClinVar_Path'] = variant.INFO.get('clinvar_pathogenic')
                        var_dict['COSMIC_ID'] = variant.INFO.get('cosmic_id')
                        var_dict['COSMIC_Num'] = variant.INFO.get('cosmic_numsamples')
                        var_dict['GnomAD_AF'] = variant.INFO.get('gnomad_popmax_af')
                        var_dict['TopMed_AF'] = variant.INFO.get('topmed_af')
                        var_dict['Annotations'] = annotations_string

                        sample_index = 0
                        ref_depths = variant.gt_ref_depths
                        alt_depths = variant.gt_alt_depths
                        depths = variant.gt_depths

                        for sample in samples:
                            var_dict[f"{sample}_Ref_Cnt"] = ref_depths[sample_index]
                            var_dict[f"{sample}_Alt_Cnt"] = alt_depths[sample_index]
                            var_dict[f"{sample}_DP"] = depths[sample_index]

                            if depths[sample_index] > 0:
                                var_dict[f"{sample}_AF"] = float(alt_depths[sample_index]) / float(depths[sample_index])
                            else:
                                var_dict[f"{sample}_AF"] = 0
                            sample_index += 1

                        if impact == "HIGH" or impact == "MODERATE":
                            impact_writer.writerow(var_dict)
                        elif impact == "LOW":
                            low_writer.writerow(var_dict)
                        elif impact == "MODIFIER":
                            mod_writer.writerow(var_dict)
                        else:
                            print(f"Unknown Impact Severity of {impact}")
