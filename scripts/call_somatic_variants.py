#!/usr/bin/env python

# Standard packages
import sys
import csv
import glob
import argparse
import subprocess

from multiprocessing import Pool
from collections import defaultdict

def execute_cmd(cmd_string):
    sys.stdout.write(
        f"Running: {cmd_string}\n"
    )
    subprocess.run(
        f"{cmd_string}", stderr=subprocess.PIPE, shell=True
    )

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', help="Input file for samples")
    args = parser.parse_args()
    args.logLevel = "INFO"

    ref = f"/mnt/shared-data/Resources/ReferenceData/GRCh38/Homo_sapiens_assembly38.fasta"
    gnomad = f"/mnt/shared-data/Resources/ReferenceData/GRCh38/af-only-gnomad.hg38.vcf.gz"
    pool = Pool(3)
    commands = []

    with open(args.input, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t')
        header = next(reader)
        for row in reader:
            sample = row[0]
            bam = f"{sample}.recalibrated.bam"
            vcf = f"{sample}.mutect2.vcf.gz"
            f1r2 = f"{sample}.f1r2.tar.gz"

            cmd_string = f"gatk Mutect2 -R {ref} -I {bam} -O {vcf} --germline-resource {gnomad} --native-pair-hmm-threads 8 --f1r2-tar-gz {f1r2} -G StandardMutectAnnotation"
            commands.append(cmd_string)

        results = pool.map(execute_cmd, commands)
