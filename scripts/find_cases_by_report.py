#!/usr/bin/env python

import os
import sys
import csv
import fnmatch
import argparse

from modules import configuration
from collections import defaultdict

if __name__ == "__main__":
    type = "lung"
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--configuration',
                        help="Configuration file for various settings")
    parser.add_argument('-o', '--output',
                        help="Name for output file")
    args = parser.parse_args()

    # type_cases = defaultdict(defaultdict(list))
    # counts = defaultdict(int)
    with open(args.output, 'w') as outfile:
        outfile.write(f"Sample\tLibrary\tConfig\tCoverage\tVCF\tMuTect\tVarDict\tFreeBayes\tScalpel\tPlatypus\tPindel\n")
        for root, dirs, files in os.walk("."):
            for samples_file in fnmatch.filter(files, "*_M0373?.config"):
                sys.stderr.write("Reading file: {}\n".format(os.path.join(root, samples_file)))
                sys.stdout.write("Parsing sample data\n")
                components = os.path.split(root)
                run_path_root = components[0]

                config = configuration.configure_runtime(args.configuration)
                libraries = configuration.configure_samples(os.path.join(root, samples_file), config)
                samples = configuration.merge_library_configs_samples(libraries)

                for sample in samples:
                    for library in samples[sample]:
                        if samples[sample][library]['report'].startswith(type):
                            conf_file = os.path.join(root, samples_file)
                            cov_file = os.path.join(run_path_root, f"Coverage/{library}.sambamba_coverage.bed")
                            vcf_file = os.path.join(run_path_root, f"FinalVCFs/{library}.vcfanno.snpEff.GRCh37.75.vcf")
                            mutect_file = os.path.join(run_path_root, f"FinalVCFs/{library}.mutect.normalized.vcf")
                            freebayes_file = os.path.join(run_path_root, f"FinalVCFs/{library}.freebayes.normalized.vcf")
                            platypus_file = os.path.join(run_path_root, f"FinalVCFs/{library}.platypus.normalized.vcf")
                            vardict_file = os.path.join(run_path_root, f"FinalVCFs/{library}.vardict.normalized.vcf")
                            scalpel_file = os.path.join(run_path_root, f"FinalVCFs/{library}.scalpel.normalized.vcf")
                            pindel_file = os.path.join(run_path_root, f"FinalVCFs/{library}.pindel.normalized.vcf")
                            outfile.write(f"{sample}\t{library}\t{conf_file}\t{cov_file}\t{vcf_file}\t{mutect_file}\t{vardict_file}\t{freebayes_file}\t{scalpel_file}\t{platypus_file}\t{pindel_file}\n")

    # sys.stderr.write("Type\tCount\n")
    # for report_type in counts:
    #     sys.stdout.write("{}\t{}\n".format(report_type, counts[report_type]))
