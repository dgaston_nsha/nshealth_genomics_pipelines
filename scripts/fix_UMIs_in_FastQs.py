#!/usr/bin/env python

import os
import sys
import gzip
import argparse

from multiprocessing import Pool

def process_fastq(params):
    input_file_path, output_file_path = params

    sys.stdout.write(f"Processing file {input_file_path} and writing modified file to {output_file_path}\n")
    with gzip.open(input_file_path, 'rt') as file, gzip.open(output_file_path, 'wt') as out_file:
        while True:
            try:
                identifier = next(file).strip()
                sequence = next(file).strip()
                plus = next(file).strip()
                quality = next(file).strip()

                # Manipulate the sequence and quality lines
                umi = sequence[:10]
                new_sequence = sequence[10:]

                umi_quality = quality[:10]
                new_quality = quality[10:]

                # Manipulate the identifier line
                identifier_elements = identifier.split(' ')
                new_identifier = f"{identifier_elements[0]}:{umi} {identifier_elements[1]}"

                # Write the manipulated lines to the output file
                out_file.write('\n'.join([new_identifier, new_sequence, plus, new_quality]) + '\n')

            except StopIteration:
                # End of file
                break

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-t', '--threads', help="Number of Threads", default=24)
    parser.add_argument('-i', '--input_dir', help="Input directory to read data from")
    parser.add_argument('-e', '--extension', help="File extension pattern of FastQs to use", default=".fastq.gz")

    args = parser.parse_args()
    args.logLevel = "INFO"

    pool = Pool(int(args.threads))

    base_dir = os.path.dirname(args.input_dir)
    output_folder = os.path.join(base_dir, '../UMI_mod_FastQs')
    output_folder = os.path.normpath(output_folder)

    processing_queue = list()

    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    sys.stdout.write("Walking directory and subdirectories to gather files\n")
    for dirpath, dirnames, filenames in os.walk(base_dir):
        for filename in filenames:
            if filename.endswith(args.extension):
                input_file_path = os.path.join(dirpath, filename)
                relative_path = os.path.relpath(dirpath, base_dir)
                output_subfolder_path = os.path.join(output_folder, relative_path)

                if not os.path.exists(output_subfolder_path):
                    os.makedirs(output_subfolder_path)

                output_file_path = os.path.join(output_subfolder_path, filename)
                file_data = (input_file_path, output_file_path)
                processing_queue.append(file_data)

    sys.stdout.write("Processing Files\n")
    results = pool.map(process_fastq, processing_queue)
