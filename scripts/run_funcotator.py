#!/usr/bin/env python

# Standard packages
import sys
import csv
import glob
import argparse
import subprocess

from multiprocessing import Pool
from collections import defaultdict

def execute_cmd(cmd_string):
    sys.stdout.write(
        f"Running: {cmd_string}\n"
    )
    subprocess.run(
        f"{cmd_string}", stderr=subprocess.PIPE, shell=True
    )

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', help="Input file for samples")
    args = parser.parse_args()
    args.logLevel = "INFO"

    ref = f"/mnt/shared-data/Resources/ReferenceData/GRCh38/Homo_sapiens_assembly38.fasta"
    gnomad = f"/mnt/shared-data/Resources/ReferenceData/GRCh38/af-only-gnomad.hg38.vcf.gz"
    funcotator_data = f"/mnt/shared-data/Resources/ReferenceData/funcotator_dataSources.v1.7.20200521s"
    small_pool = Pool(3)

    funcotator_commands = []

    with open(args.input, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t')
        header = next(reader)
        for row in reader:
            sample = row[0]
            library = row[1]

            filtered_vcf = f"{library}.filtered.mutect2.vcf"
            funcotator_vcf = f"{library}.funcotator.vcf"

            funcotator_cmd_string = f"gatk Funcotator -V {filtered_vcf} -R {ref} --ref-version hg38 --data-sources-path {funcotator_data} -O {funcotator_vcf} --output-file-format VCF"
            funcotator_commands.append(funcotator_cmd_string)
        results = small_pool.map(execute_cmd, funcotator_commands)
