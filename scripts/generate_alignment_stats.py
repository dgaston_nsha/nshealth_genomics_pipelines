#!/usr/bin/env python

# Standard packages
import sys
import glob
import argparse
import subprocess

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-t', '--threads', help="Number of CPU threads")
    args = parser.parse_args()
    args.logLevel = "INFO"

    alignments = glob.glob('*.bam')

    for alignment in alignments:
        sys.stdout.write(
            f"Calculating statistics for: {alignment}\n"
        )
        subprocess.run(
            f"samtools stats -d -p -@ {args.threads} -S RG -P {alignment} {alignment} > {alignment}.bamstat", stderr=subprocess.PIPE, shell=True
        )
