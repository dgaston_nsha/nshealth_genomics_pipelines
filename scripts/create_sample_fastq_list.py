#!/usr/bin/env python

# Standard packages
import os
import glob
import argparse

from collections import defaultdict

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--output', help="Output file for samples")
    args = parser.parse_args()
    args.logLevel = "INFO"

    dir = os.getcwd()

    with open(args.output, 'w') as outfile:
        outfile.write(f"Sample\tFastQ1\tFastQ2\n")
        fastq1_files = glob.glob('*_R1_001.fastq.gz')
        for fastq1 in fastq1_files:
            elements = fastq1.split('_L001_R1_001')
            library = elements[0]
            elements2 = library.split('_')
            sample = elements2[0]

            fastq2 = f"{library}_L001_R2_001.fastq.gz"
            outfile.write(f"{library}\t{dir}/{fastq1}\t{dir}/{fastq2}\n")
