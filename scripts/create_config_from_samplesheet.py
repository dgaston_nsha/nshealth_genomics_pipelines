#!/usr/bin/env python

# Standard packages
import os
import sys
import argparse

from collections import defaultdict
from sample_sheet import SampleSheet

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--output', help="Output file for samples")
    parser.add_argument('-i', '--input', help="Input SampleSheet")
    args = parser.parse_args()
    args.logLevel = "INFO"

    sample_sheet = SampleSheet(args.input)
    path = os.getcwd()
    parent_dir = os.path.abspath(os.path.join(path, os.pardir))
    fastqs_dir = os.path.join(parent_dir, "FastQs")

    with open(args.output, 'w') as outfile:
        outfile.write(f"Sample\tLibrary\tFastQ1\tFastQ2\n")
        lib_count = 1
        for sample in sample_sheet.samples:
            library = f"{sample['Sample_ID']}_S{lib_count}"
            fastq1 = f"{library}_L001_R1_001.fastq.gz"
            fastq2 = f"{library}_L001_R2_001.fastq.gz"

            fastq1_path = os.path.join(fastqs_dir, fastq1)
            fastq2_path = os.path.join(fastqs_dir, fastq2)

            outfile.write(f"{sample['Sample_ID']}\t{library}\t{fastq1_path}\t{fastq2_path}\n")
            lib_count += 1
