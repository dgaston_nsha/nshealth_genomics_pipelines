#!/usr/bin/env python

# Standard packages
import os
import sys
import glob
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--output', help="Output file for samples")
    parser.add_argument('-p', '--panel', help="Panel name (TS15/Myeloid/BRCA)")
    args = parser.parse_args()
    args.logLevel = "INFO"

    path = os.getcwd()
    fastq1_files = glob.glob('*_R1_001.fastq.gz')

    with open(args.output, 'w') as outfile:
        for fastq1 in fastq1_files:
            elements = fastq1.split('_L001_')
            library = elements[0]
            elements2 = library.split('_')
            sample = elements2[0]

            fastq2 = f"{library}_L001_R2_001.fastq.gz"

            fastq1_path = os.path.join(path, fastq1)
            fastq2_path = os.path.join(path, fastq2)

            outfile.write(f"[{library}]\n")
            outfile.write(f"fastq1: {fastq1_path}\n")
            outfile.write(f"fastq2: {fastq2_path}\n")
            outfile.write(f"library_name: {library}\n")
            outfile.write(f"sample_name: {sample}\n")
            outfile.write(f"extraction: default\n")
            outfile.write(f"panel: {args.panel}\n")
            outfile.write(f"report: \n")
            outfile.write(f"target_pool: \n")
            outfile.write(f"sequencer: test\n")
            outfile.write(f"run_id: test\n")
            outfile.write(f"num_libraries_in_run: test\n")
            outfile.write(f"regions: \n")
            outfile.write(f"vcfanno_config: \n\n")
