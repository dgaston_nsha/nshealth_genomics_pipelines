#!/usr/bin/env python

# Standard packages
import sys
import glob
import argparse
import subprocess

from collections import defaultdict

if __name__ == "__main__":
    files = glob.glob('*recalibrated*')
    for file in files:
        elements = file.split('.recalibrated.sorted.')
        sample = elements[0]
        end = elements[1]
        out = f"{sample}.{end}"
        sys.stdout.write(
            f"Running: mv {file} {out}\n"
        )
        subprocess.run(
            f"mv {file} {out}", stderr=subprocess.PIPE, shell=True
        )
