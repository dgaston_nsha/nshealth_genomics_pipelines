#!/usr/bin/env python

# Standard packages
import os
import sys
import argparse
import subprocess

from modules import configuration

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--samples_file', help="Input file for samples")
    parser.add_argument('-c', '--configuration', help="Configuration file for various settings")
    parser.add_argument('-t', '--threads', help="Number of Threads", default=24)

    args = parser.parse_args()
    args.logLevel = "INFO"

    analysis_root_dir = os.getcwd()

    sys.stdout.write("Parsing configuration data\n")
    config = configuration.configure_runtime(args.configuration)

    sys.stdout.write("Parsing sample data\n")
    samples = configuration.configure_samples(args.samples_file, config)

    ref = f""

    sys.stdout.write("Converting Recalibrated BAMs to CRAM...\n")
    for sample in samples:
        bam = f"{samples[sample]['library_name']}.recalibrated.bam"
        cram = f"{samples[sample]['library_name']}.final.cram"

        command = f""