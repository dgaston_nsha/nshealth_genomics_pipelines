#!/usr/bin/env python

import os
import sys
import fnmatch

from collections import defaultdict

if __name__ == "__main__":
    counts = defaultdict(int)
    sys.stdout.write("Sample\tLibrary\tConfig\n")
    for root, dirs, files in os.walk("."):
        for config_file in fnmatch.filter(files, "*_M0373?.config"):
            with open(os.path.join(root, config_file).rstrip(), 'r') as data_file:
                library_name = ""
                for line in data_file.readlines():
                    if line.startswith("library_name: "):
                        elements = line.split()
                        library_name = elements[1]
