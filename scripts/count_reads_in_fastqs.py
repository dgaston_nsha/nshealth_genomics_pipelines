#!/usr/bin/env python

# Standard packages
import sys
import glob
import argparse
import subprocess

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--output', help="Output filename")
    args = parser.parse_args()
    args.logLevel = "INFO"

    fastq_files1 = glob.glob('*.fastq.gz')
    fastq_files2 = glob.glob('*.fq.gz')

    fastqs = fastq_files1 + fastq_files2

    with open(args.output, 'w') as outfile:
        outfile.write(f"FastQ File\tNumber of Reads\n")
        for fastq in fastqs:
            print(f"Counting reads in file: {fastq}\n")
            result = subprocess.run(
                f"zcat {fastq} | wc -l", shell=True, capture_output=True, text=True
            )
            num_reads = int(result.stdout) / 4
            outfile.write(f"{fastq}\t{num_reads}\n")
