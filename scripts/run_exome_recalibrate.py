#!/usr/bin/env python

# Standard packages
import sys
import csv
import glob
import argparse
import subprocess

from multiprocessing import Pool
from collections import defaultdict

def execute_cmd(cmd_string):
    sys.stdout.write(
        f"Running: {cmd_string}\n"
    )
    subprocess.run(
        f"{cmd_string}", stderr=subprocess.PIPE, shell=True
    )

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', help="Input file for samples")
    parser.add_argument('-n', '--num_cpu', help="Number of CPU cores to use for multiprocessing")
    args = parser.parse_args()
    args.logLevel = "INFO"

    ref = f"/mnt/shared-data/Resources/ReferenceData/GRCh38/Homo_sapiens_assembly38.fasta"
    dbsnp_sites = f"/mnt/shared-data/Resources/ReferenceData/GRCh38/dbsnp_146.hg38.vcf.gz"
    gnomad_sites = f"/mnt/shared-data/Resources/ReferenceData/GRCh38/af-only-gnomad.hg38.vcf.gz"

    pool = Pool(int(args.num_cpu))

    recal_commands = []
    bqsr_commands = []

    with open(args.input, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t')
        header = next(reader)
        for row in reader:
            sample = row[0]

            fixed = f"{sample}.fixed.bam"
            recal_table = f"{sample}.recal.table"
            recalibrated = f"{sample}.recalibrated.bam"

            recal_commands.append(f"gatk BaseRecalibrator -I {fixed} -R {ref} -O {recal_table} --known-sites {dbsnp_sites} --known-sites {gnomad_sites}")
            bqsr_commands.append(f"gatk ApplyBQSR -R {ref} -I {fixed} -O {recalibrated} --bqsr-recal-file {recal_table}")

    recal_results = pool.map(execute_cmd, recal_commands)
    bqsr_results = pool.map(execute_cmd, bqsr_commands)
