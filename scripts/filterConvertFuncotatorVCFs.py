#!/usr/bin/env python
import csv
import sys
import argparse

from cyvcf2 import VCF
from collections import defaultdict

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', help="Input list of samples")
    parser.add_argument('-o', '--output', help="Output file name")
    args = parser.parse_args()
    args.logLevel = "INFO"

    variants = defaultdict(dict)

    fieldnames = ["chrom", "start", "end", "ref", "alt", "filter", "transcript", "strand", "exon",
    "codon", "cdna", "protein", "gc_content", "gnomad_exome_popmax_af",
    "gnomad_genome_popmax_af", "dbsnp_id", "entrez", "refseq", "ensembl", "cosmic",
    "clinvar_id", "clinvar_sig", "Uniprot_acc", "Uniprot_BP", "Uniprot_CC", "Uniprot_MF"]

    with open(args.input, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t')
        header = next(reader)
        for row in reader:
            library = row[0]
            vcf = f"{library}.funcotator.vcf"
            input_vcf = VCF(vcf)

            fieldnames.append(f"{library}_AF")
            fieldnames.append(f"{library}_DP")

            sys.stdout.write(f"Processing VCF: {vcf}\n")

            for variant in input_vcf:
                if variant.FILTER:
                    continue
                if "_decoy" in variant.CHROM:
                    continue

                sample_depths = variant.format('DP')
                funcotator_annotations = variant.INFO.get('FUNCOTATION').split('|')

                if funcotator_annotations[198]:
                    # This is a temporary hack for dealing with HTML encoded pipes/lists
                    # encoded in the Funcotator outputs
                    if "%7C" in funcotator_annotations[198]:
                        freqs = funcotator_annotations[198].split("_%7C_")
                        if freqs[0]:
                            gnomad_exome_popmax_af = float(freqs[0])
                        else:
                            gnomad_exome_popmax_af = -1.00
                    else:
                        gnomad_exome_popmax_af = float(funcotator_annotations[198])
                else:
                    gnomad_exome_popmax_af = -1.00

                if funcotator_annotations[233]:
                    if "%7C" in funcotator_annotations[233]:
                        freqs = funcotator_annotations[233].split("_%7C_")
                        if freqs[0]:
                            gnomad_genome_popmax_af = float(freqs[0])
                        else:
                            gnomad_genome_popmax_af = -1.00
                    else:
                        gnomad_genome_popmax_af = float(funcotator_annotations[233])
                else:
                    gnomad_genome_popmax_af = -1.00

                depths = variant.format('DP')
                freqs = variant.format('AF')

                if int(sample_depths[0]) > 20 and gnomad_exome_popmax_af < 0.05 and gnomad_genome_popmax_af < 0.05:
                    key = f"{variant.CHROM}-{variant.start}-{variant.end}-{variant.REF}-{variant.ALT[0]}"
                    variants[key]['chrom'] = variant.CHROM
                    variants[key]['start'] = variant.start
                    variants[key]['end'] = variant.end
                    variants[key]['ref'] = variant.REF
                    variants[key]['alt'] = variant.ALT[0]
                    variants[key]['filter'] = variant.FILTER
                    variants[key]['transcript'] = funcotator_annotations[12]
                    variants[key]['strand'] = funcotator_annotations[13]
                    variants[key]['exon'] = funcotator_annotations[14]
                    variants[key]['cdna'] = funcotator_annotations[16]
                    variants[key]['codon'] = funcotator_annotations[17]
                    variants[key]['protein'] = funcotator_annotations[18]
                    variants[key]['gc_content'] = funcotator_annotations[19]
                    variants[key]['gnomad_exome_popmax_af'] = gnomad_exome_popmax_af
                    variants[key]['gnomad_genome_popmax_af'] = gnomad_genome_popmax_af
                    variants[key]['dbsnp_id'] = funcotator_annotations[163]
                    variants[key]['entrez'] = funcotator_annotations[99]
                    variants[key]['refseq'] = funcotator_annotations[101]
                    variants[key]['ensembl'] = funcotator_annotations[103]
                    variants[key]['cosmic'] = funcotator_annotations[62]
                    variants[key]['clinvar_id'] = funcotator_annotations[60]
                    variants[key]['clinvar_sig'] = funcotator_annotations[48]
                    variants[key]['Uniprot_acc'] = funcotator_annotations[111]
                    variants[key]['Uniprot_BP'] = funcotator_annotations[112]
                    variants[key]['Uniprot_CC'] = funcotator_annotations[113]
                    variants[key]['Uniprot_MF'] = funcotator_annotations[114]
                    variants[key][f"{library}_AF"] = freqs[0]
                    variants[key][f"{library}_DP"] = depths[0]

    with open(args.output, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for key in variants:
            writer.writerow(variants[key])
