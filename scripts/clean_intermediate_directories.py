#!/usr/bin/env python

import os
import sys
import shutil
import fnmatch

from collections import defaultdict

if __name__ == "__main__":
    for root, dirs, files in os.walk("."):
        for dir in dirs:
            if dir == "Intermediates":
                dir_path = os.path.join(root, dir)
                sys.stdout.write(f"Deleting Directory: {dir_path}\n")
                shutil.rmtree(dir_path)
