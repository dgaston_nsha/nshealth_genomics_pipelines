#!/usr/bin/env python

# Standard packages
import os
import sys
import csv
import argparse
import subprocess
import xlsxwriter

from modules import configuration
from collections import defaultdict


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--amplicons', help="Amplicon file")
    parser.add_argument('-t', '--threads', help="Number of CPU threads to use")
    parser.add_argument('-o', '--output_dir', help="Output Directory")
    parser.add_argument('-s', '--samples_file', help="Input file for samples")
    parser.add_argument('-c', '--configuration', help="Configuration file for various settings")

    args = parser.parse_args()
    args.logLevel = "INFO"

    analysis_root_dir = os.getcwd()
    output_dir_path = os.path.join(analysis_root_dir, args.output_dir)

    sys.stdout.write("Parsing configuration data\n")
    config = configuration.configure_runtime(args.configuration)

    sys.stdout.write("Parsing sample data\n")
    samples = configuration.configure_samples(args.samples_file, config)

    ref = f"/mnt/shared-data/Resources/ReferenceData/b37/human_g1k_v37.fasta"

    command = f"cnvkit.py batch -m amplicon -n -t {args.amplicons} -f {ref} --drop-low-coverage --short-names\
         -p {args.threads} -d {args.output} --scatter --diagram *.recalibrated.sorted.bam"
    
    sys.stdout.write(f"Running: {command}\n")
    subprocess.run(f"{command}", stderr=subprocess.PIPE, shell=True)
    os.chdir(output_dir_path)

    sys.stdout.write(f"Parsing sample call.cns files and generating reports")
    cnvkit_header = ["Chr", "Start", "End", "Amplicons", "log2", "Copy Number", "Depth", "P-value (T-test)", "Num Probes", "Weight"]
    for sample in samples:
        for library in samples[sample]:
            library = samples[sample][library]['library_name']
            call_file = f"{library}.recalibrated.sorted.call.cns"
            report_name = f"{library}.cnvkit_calls.xlsx"

            wb = xlsxwriter.Workbook(report_name)
            
            error_style = wb.add_format({'bg_color': '#FF0000'})
            warning_style = wb.add_format({'bg_color': '#FF9900'})
            pass_style = wb.add_format({'bg_color': '#00FF00'})
            interest_style = wb.add_format({'bg_color': '#808080'})
            default_style = wb.add_format({'bg_color': '#FFFFFF'})
            
            cnvkit_sheet = wb.add_worksheet("CNVKit")
            cnvkit_sheet.write_row(0, 0, cnvkit_header)
            
            with open(call_file, 'r') as csvfile:
                reader = csv.reader(csvfile, delimiter='\t')
                header = next(reader)
                col_idx = 1
                for row in reader:
                    cnvkit_sheet.write_row(col_idx, 0, row)
                    col_idx += 1