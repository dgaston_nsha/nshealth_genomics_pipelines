#!/usr/bin/env python

import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', help="Input VCF file")
    parser.add_argument('-o', '--output', help="Output VCF file")
    args = parser.parse_args()
    args.logLevel = "INFO"

    with open(args.input, 'r') as input_vcf:
        with open(args.output, 'w') as output_vcf:
            for line in input_vcf:
                if line.startswith("##contig=<ID="):
                    continue
                elif line.startswith("#"):
                    output_vcf.write(line)
                else:
                    output_vcf.write(f"chr{line}")
