#!/usr/bin/env python

# Standard packages
import sys
import csv
import glob
import argparse
import subprocess

from multiprocessing import Pool
from collections import defaultdict

def execute_cmd(cmd_string):
    sys.stdout.write(
        f"Running: {cmd_string}\n"
    )
    subprocess.run(
        f"{cmd_string}", stderr=subprocess.PIPE, shell=True
    )

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', help="Input file for samples")
    args = parser.parse_args()
    args.logLevel = "INFO"

    ref = f"/mnt/shared-data/Resources/ReferenceData/GRCh38/Homo_sapiens_assembly38.fasta"
    gnomad = f"/mnt/shared-data/Resources/ReferenceData/GRCh38/af-only-gnomad.hg38.vcf.gz"
    funcotator_data = f"/mnt/shared-data/Resources/ReferenceData/funcotator_dataSources.v1.7.20200521s"

    pool = Pool(24)
    small_pool = Pool(3)
    learn_commands = []
    filter_commands = []
    pileup_commands = []
    contamination_commands = []
    funcotator_commands = []

    with open(args.input, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t')
        header = next(reader)
        for row in reader:
            library = row[0]
            bam = f"{library}.recalibrated.bam"
            mutect_vcf = f"{library}.mutect2.vcf"
            f1r2 = f"{library}.f1r2.tar.gz"
            model = f"{library}-read-orientation-model.tar.gz"
            # pile_up_summary = f"{library}.pileupsummaries.table"
            # contam_table = f"{library}.contamination.table"
            # segmentation_table = f"{library}.segmentation.table"
            filtered_vcf = f"{library}.filtered.mutect2.vcf.gz"
            funcotator_vcf = f"{library}.funcotator.vcf"

            learn_cmd_string = f"gatk LearnReadOrientationModel -I {f1r2} -O {model}"
            # pileup_cmd_string = f"gatk GetPileupSummaries -I {bam} -V {gnomad} -L {gnomad} -O {pile_up_summary}"
            # contam_cmd_string = f"gatk CalculateContamination -I {pile_up_summary} -tumor-segmentation {segmentation_table} -O {contam_table}"
            # filter_cmd_string = f"gatk FilterMutectCalls -R {ref} -V {mutect_vcf} --contamination-table {contam_table} --ob-priors {model} -O {filtered_vcf}\n"
            filter_cmd_string = f"gatk FilterMutectCalls -R {ref} -V {mutect_vcf} --ob-priors {model} -O {filtered_vcf}"
            functotator_cmd_string = f"gatk Funcotator -V {filtered_vcf} -R {ref} --ref-version hg38 --data-sources-path {funcotator_data} -O {funcotator_vcf} --output-file-format VCF"

            learn_commands.append(learn_cmd_string)
            # pileup_commands.append(pileup_cmd_string)
            # contamination_commands.append(contam_cmd_string)
            filter_commands.append(filter_cmd_string)
            funcotator_commands.append(functotator_cmd_string)

        results = pool.map(execute_cmd, learn_commands)

        # Execute GetPileupSummaries with its own multiprocessing
        # for command in pileup_commands:
        #    execute_cmd(command)

        # results = pool.map(execute_cmd, contamination_commands)
        results = pool.map(execute_cmd, filter_commands)
        results = small_pool.map(execute_cmd, funcotator_commands)
