#!/usr/bin/env python
import argparse

from gtfparse import read_gtf

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', help="Input GTF file")
    parser.add_argument('-o', '--output', help="Output file name")
    args = parser.parse_args()
    args.logLevel = "INFO"

    df = read_gtf(args.input)
    df_transcripts = df[df["feature"] == "transcript"]
    with open(args.output, 'w') as outfile:
        for index, row in df_transcripts.iterrows():
            id = row['transcript_id']
            outfile.write(f"{id}\n")
