#!/usr/bin/env python

# Standard packages
import sys
import csv
import getpass
import argparse
import xlsxwriter

from database import utils_updated as utils
from modules import configuration

from multiprocessing import Pool
from collections import defaultdict

from cassandra import InvalidRequest
from cassandra.cqlengine import connection
from cassandra.auth import PlainTextAuthProvider

from database.variantstore import Variant
from database.variantstore import SampleVariant
from database.coveragestore import AmpliconCoverage
from database.coveragestore import SampleCoverage

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--samples_file', help="Input file for samples")
    parser.add_argument('-a', '--address', help="IP Address for Cassandra connection", default='127.0.0.1')
    parser.add_argument('-u', '--username', help='Cassandra username for login', default=None)
    parser.add_argument('-c', '--configuration', help="Configuration file for various settings")
    parser.add_argument('-l', '--snps_list', help="Input SNP list")
    parser.add_argument('-r', '--report', help="Root name for reports (per sample)", default='snp_report.xlsx')

    args = parser.parse_args()
    args.logLevel = "INFO"

    if args.username:
        password = getpass.getpass()
        auth_provider = PlainTextAuthProvider(username=args.username, password=password)
    else:
        auth_provider = None
    
    sys.stdout.write("Parsing configuration data\n")
    config = configuration.configure_runtime(args.configuration)

    sys.stdout.write("Parsing sample data\n")
    libraries = configuration.configure_samples(args.samples_file, config)
    samples = configuration.merge_library_configs_samples(libraries)

    callers = ("mutect", "platypus", "vardict", "scalpel", "freebayes", "pindel")

    connection.setup([args.address], "variantstore", auth_provider=auth_provider)

    wb = xlsxwriter.Workbook(args.report)
    sheet = wb.add_worksheet("SNPs")
    all_callers = wb.add_worksheet("All Callers")

    snps = list()

    with open(args.snps_list, 'r') as snp_file:
        reader = csv.reader(snp_file, dialect='excel-tab')
        sheet.write(0, 0, "SNP")
        row_num = 1
        for row in reader:
            snps.append(row)
            sheet.write(row_num, 0, f"{row[0]}-{row[1]}-{row[2]}-{row[3]}")
            row_num += 1

    sys.stdout.write("Creating SNP report\n")
    col_num = 1
    for sample in samples:
        for library in samples[sample]:
            sheet.write(0, col_num, f"{samples[sample][library]['sample_name']}")
            row_num = 1
            for row in snps:
                variant = SampleVariant.objects.timeout(None).filter(
                    SampleVariant.reference_genome == config['genome_version'],
                    SampleVariant.chr == row[0],
                    SampleVariant.pos == int(row[1]) - 1,
                    SampleVariant.ref == row[2],
                    SampleVariant.alt == row[3],
                    SampleVariant.sample == samples[sample][library]['sample_name'],
                    SampleVariant.library_name == (samples[sample][library]['library_name']),
                    SampleVariant.run_id == samples[sample][library]['run_id']
                )
                if variant.count() > 1:
                    sys.stderr.write(f"Query for {row[0]}-{row[1]}-{row[2]}-{row[3]} in sample {samples[sample][library]['sample_name']} \
                    with run_id {samples[sample][library]['run_id']} produced {variant.count()} results. Were expecting 1\n")
                elif variant.count() == 0:
                    sheet.write(row_num, col_num, f"-")
                else:
                    sheet.write(row_num, col_num, round(variant[0].max_som_aaf, 3))
                row_num += 1
        col_num += 1
    
    wb.close()