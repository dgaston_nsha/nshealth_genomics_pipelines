#!/usr/bin/env python

import re
import sys
import csv
import cyvcf2
import argparse
import datetime

import numpy as np

from scipy import stats
from modules import configuration
from database import utils_updated as utils
from modules import vcf_parsing_updated as vcf_parsing

from collections import defaultdict

def convert_sample(samples, sample, library, variant_frequencies, variant_samples):
    library_id = samples[sample][library]['library_name']
    output_csv = f"{library_id}.variants.csv"

    parse_functions = {'mutect': vcf_parsing.parse_mutect_vcf_record,
                       'freebayes': vcf_parsing.parse_freebayes_vcf_record,
                       'vardict': vcf_parsing.parse_vardict_vcf_record,
                       'scalpel': vcf_parsing.parse_scalpel_vcf_record,
                       'platypus': vcf_parsing.parse_platypus_vcf_record,
                       'pindel': vcf_parsing.parse_pindel_vcf_record}

    caller_records = defaultdict(lambda: dict())

    sys.stdout.write(f"Processing VCF Files for {library_id}\n")
    vcf_parsing.parse_vcf(f"{library_id}.mutect.low_support_filtered.vcf", "mutect", caller_records)
    vcf_parsing.parse_vcf(f"{library_id}.vardict.low_support_filtered.vcf", "vardict", caller_records)
    vcf_parsing.parse_vcf(f"{library_id}.freebayes.low_support_filtered.vcf", "freebayes", caller_records)
    vcf_parsing.parse_vcf(f"{library_id}.scalpel.low_support_filtered.vcf", "scalpel", caller_records)
    vcf_parsing.parse_vcf(f"{library_id}.platypus.low_support_filtered.vcf", "platypus", caller_records)
    vcf_parsing.parse_vcf(f"{library_id}.pindel.low_support_filtered.vcf", "pindel", caller_records)

    annotated_vcf = f"{library_id}.vcfanno.snpEff.GRCh37.75.vcf"

    vcf = cyvcf2.VCF(annotated_vcf)

    reader = cyvcf2.VCFReader(annotated_vcf)
    desc = reader["ANN"]["Description"]
    annotation_keys = [x.strip("\"'") for x in re.split(r"\s*\|\s*", desc.split(":", 1)[1].strip('" '))]

    added = 0
    failed = 0

    fieldnames = ['Chr', 'Pos', 'Ref', 'Alt', 'callers', 'amplicons', 'rs_id',
    'type', 'subtype', 'gene', 'transcript', 'exon', 'codon_change', 'biotype',
    'aa_change', 'severity', 'impact', 'impact_so', 'clinvar_pathogenic', 'clinvar_hgvs',
    'clinvar_revstatus', 'clinvar_org', 'clinvar_disease', 'clinvar_accession',
    'clinvar_origin', 'cosmic_ids', 'cosmic_numsamples', 'cosmic_cds', 'cosmic_aa',
    'cosmic_gene', 'cosmic_hgvsc', 'cosmic_hgvsp', 'cosmic_hgvsg', 'cosmic_strand',
    'in_clinvar', 'in_cosmic', 'is_pathogenic', 'is_lof', 'is_coding', 'is_splicing',
    'rs_ids', 'cosmic_ids', 'max_maf_all', 'max_maf_no_fin', 'esp_ea', 'esp_aa',
    'esp_all', '1kg_amr', '1kg_eas', '1kg_sas', '1kg_afr', '1kg_eur', '1kg_all',
    'exac_all', 'adj_exac_all', 'adj_exac_afr', 'adj_exac_amr', 'adj_exac_eas',
    'adj_exac_fin', 'adj_exac_nfe', 'adj_exac_oth', 'adj_exac_sas', 'max_som_aaf',
    'min_depth', 'max_depth', 'mutect_AF', 'mutect_DP', 'mutect_Filters',
    'freebayes_AF', 'freebayes_DP', 'freebayes_Filters', 'scalpel_AF', 'scalpel_DP',
    'scalpel_Filters', 'platypus_AF', 'platypus_DP', 'platypus_Filters', 'pindel_AF',
    'pindel_DP', 'pindel_Filters', 'vardict_AF', 'vardict_DP', 'vardict_Filters']

    with open(output_csv, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()

        for variant in vcf:
            # Parsing VCF and creating data structures for Cassandra model
            output_data = dict()

            callers = variant.INFO.get('CALLERS').split(',')
            effects = utils.get_effects(variant, annotation_keys)
            top_impact = utils.get_top_impact(effects)
            population_freqs = utils.get_population_freqs(variant)
            transcript_data = utils.get_transcript_effects(effects)
            clinvar_data = utils.get_clinvar_info(variant, samples, sample)
            cosmic_data = utils.get_cosmic_info(variant)

            key = (str("chr{}".format(variant.CHROM)), int(variant.start), int(variant.end), str(variant.REF),
                   str(variant.ALT[0]))

            variant_id = f"{variant.CHROM}-{variant.POS}-{variant.REF}-{variant.ALT[0]}"

            caller_variant_data_dicts = defaultdict(dict)
            max_som_aaf = -1.00
            max_depth = -1
            min_depth = 100000000

            for caller in callers:
                caller_variant_data_dicts[caller] = parse_functions[caller](caller_records[caller][key])
                if float(caller_variant_data_dicts[caller]['AAF']) > max_som_aaf:
                    max_som_aaf = float(caller_variant_data_dicts[caller]['AAF'])
                if int(caller_variant_data_dicts[caller]['DP']) < min_depth:
                    min_depth = int(caller_variant_data_dicts[caller]['DP'])
                if int(caller_variant_data_dicts[caller]['DP']) > max_depth:
                    max_depth = int(caller_variant_data_dicts[caller]['DP'])

            if min_depth == 100000000:
                min_depth = -1

            variant_frequencies[variant_id].append(max_som_aaf)
            variant_samples[variant_id].append(library_id)

            output_data['Chr'] = variant.CHROM
            output_data['Pos'] = variant.POS
            output_data['Ref'] = variant.REF
            output_data['Alt'] = variant.ALT[0]

            output_data['callers'] = variant.INFO.get('CALLERS')
            output_data['amplicons'] = variant.INFO.get('amplicon_target')

            output_data['rs_id'] = variant.ID
            output_data['type'] = variant.INFO.get('type')
            output_data['subtype'] = variant.INFO.get('sub_type')

            output_data['gene'] = top_impact.gene
            output_data['transcript'] = top_impact.transcript
            output_data['exon'] = top_impact.exon
            output_data['codon_change'] = top_impact.codon_change
            output_data['biotype'] = top_impact.biotype
            output_data['aa_change'] = top_impact.aa_change
            output_data['severity'] = top_impact.effect_severity
            output_data['impact'] = top_impact.top_consequence
            output_data['impact_so'] = top_impact.so

            output_data['clinvar_pathogenic'] = variant.INFO.get('clinvar_pathogenic') or 'None'
            output_data['clinvar_hgvs'] = variant.INFO.get('clinvar_hgvs') or 'None'
            output_data['clinvar_revstatus'] = variant.INFO.get('clinvar_revstatus') or 'None'
            output_data['clinvar_org'] = variant.INFO.get('clinvar_org') or 'None'
            output_data['clinvar_disease'] = variant.INFO.get('clinvar_disease') or 'None'
            output_data['clinvar_accession'] = variant.INFO.get('clinvar_accession') or 'None'
            output_data['clinvar_origin'] = variant.INFO.get('clinvar_origin') or 'None'

            output_data['cosmic_ids'] = variant.INFO.get('cosmic_id') or 'None'
            output_data['cosmic_numsamples'] = str(variant.INFO.get('cosmic_numsamples')) or 'None'
            output_data['cosmic_cds'] = variant.INFO.get('cosmic_cds') or 'None'
            output_data['cosmic_aa'] = variant.INFO.get('cosmic_aa') or 'None'
            output_data['cosmic_gene'] = variant.INFO.get('cosmic_gene') or 'None'
            output_data['cosmic_hgvsc'] = variant.INFO.get('cosmic_hgvsc') or 'None'
            output_data['cosmic_hgvsp'] = variant.INFO.get('cosmic_hgvsp') or 'None'
            output_data['cosmic_hgvsg'] = variant.INFO.get('cosmic_hgvsg') or 'None'
            output_data['cosmic_strand'] = variant.INFO.get('cosmic_strand') or 'None'

            output_data['in_clinvar'] = vcf_parsing.var_is_in_clinvar(variant)
            output_data['in_cosmic'] = vcf_parsing.var_is_in_cosmic(variant)
            output_data['is_pathogenic'] = vcf_parsing.var_is_pathogenic(variant)
            output_data['is_lof'] = vcf_parsing.var_is_lof(variant)
            output_data['is_coding'] = vcf_parsing.var_is_coding(variant)
            output_data['is_splicing'] = vcf_parsing.var_is_splicing(variant)
            output_data['rs_ids'] = variant.INFO.get('rs_ids')
            output_data['cosmic_ids'] = variant.INFO.get('cosmic_id')

            output_data['max_maf_all'] = variant.INFO.get('max_aaf_all') or -1
            output_data['max_maf_no_fin'] = variant.INFO.get('max_aaf_no_fin') or -1
            output_data['esp_ea'] = population_freqs['esp_ea']
            output_data['esp_aa'] = population_freqs['esp_aa']
            output_data['esp_all'] = population_freqs['esp_all']
            output_data['1kg_amr'] = population_freqs['1kg_amr']
            output_data['1kg_eas'] = population_freqs['1kg_eas']
            output_data['1kg_sas'] = population_freqs['1kg_sas']
            output_data['1kg_afr'] = population_freqs['1kg_afr']
            output_data['1kg_eur'] = population_freqs['1kg_eur']
            output_data['1kg_all'] = population_freqs['1kg_all']
            output_data['exac_all'] = population_freqs['exac_all']
            output_data['adj_exac_all'] = population_freqs['adj_exac_all']
            output_data['adj_exac_afr'] = population_freqs['adj_exac_afr']
            output_data['adj_exac_amr'] = population_freqs['adj_exac_amr']
            output_data['adj_exac_eas'] = population_freqs['adj_exac_eas']
            output_data['adj_exac_fin'] = population_freqs['adj_exac_fin']
            output_data['adj_exac_nfe'] = population_freqs['adj_exac_nfe']
            output_data['adj_exac_oth'] = population_freqs['adj_exac_oth']
            output_data['adj_exac_sas'] = population_freqs['adj_exac_sas']

            output_data['max_som_aaf'] = max_som_aaf
            output_data['min_depth'] = min_depth
            output_data['max_depth'] = max_depth

            if caller_variant_data_dicts['mutect']:
                output_data['mutect_AF'] = caller_variant_data_dicts['mutect']['AAF']
                output_data['mutect_DP'] = caller_variant_data_dicts['mutect']['DP']
                output_data['mutect_Filters'] = caller_variant_data_dicts['mutect']['FILTER']
            else:
                output_data['mutect_AF'] = None
                output_data['mutect_DP'] = None
                output_data['mutect_Filters'] = None

            if caller_variant_data_dicts['freebayes']:
                output_data['freebayes_AF'] = caller_variant_data_dicts['freebayes']['AAF'] or 'None'
                output_data['freebayes_DP'] = caller_variant_data_dicts['freebayes']['DP'] or 'None'
                output_data['freebayes_Filters'] = caller_variant_data_dicts['freebayes']['FILTER'] or 'None'
            else:
                output_data['freebayes_AF'] = None
                output_data['freebayes_DP'] = None
                output_data['freebayes_Filters'] = None

            if caller_variant_data_dicts['scalpel']:
                output_data['scalpel_AF'] = caller_variant_data_dicts['scalpel']['AAF'] or 'None'
                output_data['scalpel_DP'] = caller_variant_data_dicts['scalpel']['DP'] or 'None'
                output_data['scalpel_Filters'] = caller_variant_data_dicts['scalpel']['FILTER'] or 'None'
            else:
                output_data['scalpel_AF'] = None
                output_data['scalpel_DP'] = None
                output_data['scalpel_Filters'] = None

            if caller_variant_data_dicts['platypus']:
                output_data['platypus_AF'] = caller_variant_data_dicts['platypus']['AAF'] or 'None'
                output_data['platypus_DP'] = caller_variant_data_dicts['platypus']['DP'] or 'None'
                output_data['platypus_Filters'] = caller_variant_data_dicts['platypus']['FILTER'] or 'None'
            else:
                output_data['platypus_AF'] = None
                output_data['platypus_DP'] = None
                output_data['platypus_Filters'] = None

            if caller_variant_data_dicts['pindel']:
                output_data['pindel_AF'] = caller_variant_data_dicts['pindel']['AAF'] or 'None'
                output_data['pindel_DP'] = caller_variant_data_dicts['pindel']['DP'] or 'None'
                output_data['pindel_Filters'] = caller_variant_data_dicts['pindel']['FILTER'] or 'None'
            else:
                output_data['pindel_AF'] = None
                output_data['pindel_DP'] = None
                output_data['pindel_Filters'] = None

            if caller_variant_data_dicts['vardict']:
                output_data['vardict_AF'] = caller_variant_data_dicts['vardict']['AAF'] or 'None'
                output_data['vardict_DP'] = caller_variant_data_dicts['vardict']['DP'] or 'None'
                output_data['vardict_Filters'] = caller_variant_data_dicts['vardict']['FILTER'] or 'None'
            else:
                output_data['vardict_AF'] = None
                output_data['vardict_DP'] = None
                output_data['vardict_Filters'] = None

            writer.writerow(output_data)

    return variant_frequencies, variant_samples


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--samples_file', help="Input file for samples")
    parser.add_argument('-c', '--configuration', help="Configuration file for various settings")
    parser.add_argument('-t', '--threads', help="Number of CPU Threads")

    args = parser.parse_args()
    args.logLevel = "INFO"

    sys.stdout.write("Parsing configuration data\n")
    config = configuration.configure_runtime(args.configuration)

    sys.stdout.write("Parsing sample data\n")
    libraries = configuration.configure_samples(args.samples_file, config)
    samples = configuration.merge_library_configs_samples(libraries)

    variant_frequencies = defaultdict(list)
    variant_samples = defaultdict(list)
    for sample in samples:
        for library in samples[sample]:
            variant_frequencies, variant_samples = convert_sample(samples, sample, library, variant_frequencies, variant_samples)

    with open("run_var_frequencies_summary.csv", 'w') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(["Variant_ID", "Num Times in Run", "Median VAF", "Mean VAF", "StdDev", "Percentile", "VAFs", "Samples"])
        for variant in variant_frequencies.keys():
            vafs = variant_frequencies[variant]
            samples = variant_samples[variant]
            vaf_string = "|".join([str(i) for i in vafs])
            samples_string = "|".join([str(i) for i in samples])
            writer.writerow([variant, len(vafs), np.median(vafs), np.mean(vafs), np.std(vafs), vaf_string, samples_string])
