#!/usr/bin/env python

import re
import sys
import csv
import cyvcf2
import argpars
import xlsxwriter

import numpy as np

from multiprocessing import Pool

from modules import utils
from modules import vcf_parsing
from modules import samples_config

from collections import defaultdict

def convert_sample(sample, variant_frequencies):
    sample_id = sample['sample_id']
    output_csv = f"{sample_id}.variants.csv"

    parse_functions = {'mutect': vcf_parsing.parse_mutect_vcf_record,
                       'freebayes': vcf_parsing.parse_freebayes_vcf_record,
                       'vardict': vcf_parsing.parse_vardict_vcf_record,
                       'scalpel': vcf_parsing.parse_scalpel_vcf_record,
                       'platypus': vcf_parsing.parse_platypus_vcf_record,
                       'pindel': vcf_parsing.parse_pindel_vcf_record}

    caller_records = defaultdict(lambda: dict())

    sys.stdout.write(f"Processing VCF Files for {sample_id}\n")
    vcf_parsing.parse_vcf(f"{sample_id}.mutect.low_support_filtered.vcf", "mutect", caller_records)
    vcf_parsing.parse_vcf(f"{sample_id}.vardict.low_support_filtered.vcf", "vardict", caller_records)
    vcf_parsing.parse_vcf(f"{sample_id}.freebayes.low_support_filtered.vcf", "freebayes", caller_records)
    vcf_parsing.parse_vcf(f"{sample_id}.scalpel.low_support_filtered.vcf", "scalpel", caller_records)
    vcf_parsing.parse_vcf(f"{sample_id}.platypus.low_support_filtered.vcf", "platypus", caller_records)
    vcf_parsing.parse_vcf(f"{sample_id}.pindel.low_support_filtered.vcf", "pindel", caller_records)

    annotated_vcf = f"{sample_id}.vcfanno.snpEff.GRCh37.75.vcf"

    vcf = cyvcf2.VCF(annotated_vcf)

    reader = cyvcf2.VCFReader(annotated_vcf)
    desc = reader["ANN"]["Description"]
    annotation_keys = [x.strip("\"'") for x in re.split("\s*\|\s*", desc.split(":", 1)[1].strip('" '))]

    added = 0
    failed = 0

    fieldnames = ['Chr', 'Pos', 'Ref', 'Alt', 'callers', 'amplicons', 'rs_id',
    'type', 'subtype', 'gene', 'transcript', 'exon', 'codon_change', 'biotype',
    'aa_change', 'severity', 'impact', 'impact_so', 'clinvar_pathogenic', 'clinvar_hgvs',
    'clinvar_revstatus', 'clinvar_org', 'clinvar_disease', 'clinvar_accession',
    'clinvar_origin', 'cosmic_ids', 'cosmic_numsamples', 'cosmic_cds', 'cosmic_aa',
    'cosmic_gene', 'cosmic_hgvsc', 'cosmic_hgvsp', 'cosmic_hgvsg', 'cosmic_strand',
    'in_clinvar', 'in_cosmic', 'is_pathogenic', 'is_lof', 'is_coding', 'is_splicing',
    'rs_ids', 'cosmic_ids', 'max_maf_all', 'max_maf_no_fin', 'esp_ea', 'esp_aa',
    'esp_all', '1kg_amr', '1kg_eas', '1kg_sas', '1kg_afr', '1kg_eur', '1kg_all',
    'exac_all', 'adj_exac_all', 'adj_exac_afr', 'adj_exac_amr', 'adj_exac_eas',
    'adj_exac_fin', 'adj_exac_nfe', 'adj_exac_oth', 'adj_exac_sas', 'max_som_aaf',
    'min_depth', 'max_depth', 'mutect_AF', 'mutect_DP', 'mutect_Filters',
    'freebayes_AF', 'freebayes_DP', 'freebayes_Filters', 'scalpel_AF', 'scalpel_DP',
    'scalpel_Filters', 'platypus_AF', 'platypus_DP', 'platypus_Filters', 'pindel_AF',
    'pindel_DP', 'pindel_Filters', 'vardict_AF', 'vardict_DP', 'vardict_Filters']

    with open(output_csv, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()

        for variant in vcf:
            # Parsing VCF and creating data structures for Cassandra model
            output_data = dict()

            callers = variant.INFO.get('CALLERS').split(',')
            effects = utils.get_effects(variant, annotation_keys)
            top_impact = utils.get_top_impact(effects)
            population_freqs = utils.get_population_freqs(variant)
            transcript_data = utils.get_transcript_effects(effects)
            clinvar_data = utils.get_clinvar_info(variant, samples, sample)
            cosmic_data = utils.get_cosmic_info(variant)

            key = (str("chr{}".format(variant.CHROM)), int(variant.start), int(variant.end), str(variant.REF),
                   str(variant.ALT[0]))

            variant_id = f"{variant.CHROM}-{variant.POS}-{variant.REF}-{variant.ALT[0]}"

            caller_variant_data_dicts = defaultdict(dict)
            max_som_aaf = -1.00
            max_depth = -1
            min_depth = 100000000

            for caller in callers:
                caller_variant_data_dicts[caller] = parse_functions[caller](caller_records[caller][key])
                if float(caller_variant_data_dicts[caller]['AAF']) > max_som_aaf:
                    max_som_aaf = float(caller_variant_data_dicts[caller]['AAF'])
                if int(caller_variant_data_dicts[caller]['DP']) < min_depth:
                    min_depth = int(caller_variant_data_dicts[caller]['DP'])
                if int(caller_variant_data_dicts[caller]['DP']) > max_depth:
                    max_depth = int(caller_variant_data_dicts[caller]['DP'])

            if min_depth == 100000000:
                min_depth = -1

            variant_frequencies[variant_id].append(max_som_aaf)

            output_data['Chr'] = variant.CHROM
            output_data['Pos'] = variant.POS
            output_data['Ref'] = variant.REF
            output_data['Alt'] = variant.ALT[0]

            output_data['callers'] = variant.INFO.get('CALLERS')
            output_data['amplicons'] = variant.INFO.get('amplicon_target')

            output_data['rs_id'] = variant.ID
            output_data['type'] = variant.INFO.get('type')
            output_data['subtype'] = variant.INFO.get('sub_type')

            output_data['gene'] = top_impact.gene
            output_data['transcript'] = top_impact.transcript
            output_data['exon'] = top_impact.exon
            output_data['codon_change'] = top_impact.codon_change
            output_data['biotype'] = top_impact.biotype
            output_data['aa_change'] = top_impact.aa_change
            output_data['severity'] = top_impact.effect_severity
            output_data['impact'] = top_impact.top_consequence
            output_data['impact_so'] = top_impact.so

            output_data['clinvar_pathogenic'] = variant.INFO.get('clinvar_pathogenic') or 'None'
            output_data['clinvar_hgvs'] = variant.INFO.get('clinvar_hgvs') or 'None'
            output_data['clinvar_revstatus'] = variant.INFO.get('clinvar_revstatus') or 'None'
            output_data['clinvar_org'] = variant.INFO.get('clinvar_org') or 'None'
            output_data['clinvar_disease'] = variant.INFO.get('clinvar_disease') or 'None'
            output_data['clinvar_accession'] = variant.INFO.get('clinvar_accession') or 'None'
            output_data['clinvar_origin'] = variant.INFO.get('clinvar_origin') or 'None'

            output_data['cosmic_ids'] = variant.INFO.get('cosmic_id') or 'None'
            output_data['cosmic_numsamples'] = str(variant.INFO.get('cosmic_numsamples')) or 'None'
            output_data['cosmic_cds'] = variant.INFO.get('cosmic_cds') or 'None'
            output_data['cosmic_aa'] = variant.INFO.get('cosmic_aa') or 'None'
            output_data['cosmic_gene'] = variant.INFO.get('cosmic_gene') or 'None'
            output_data['cosmic_hgvsc'] = variant.INFO.get('cosmic_hgvsc') or 'None'
            output_data['cosmic_hgvsp'] = variant.INFO.get('cosmic_hgvsp') or 'None'
            output_data['cosmic_hgvsg'] = variant.INFO.get('cosmic_hgvsg') or 'None'
            output_data['cosmic_strand'] = variant.INFO.get('cosmic_strand') or 'None'

            output_data['in_clinvar'] = vcf_parsing.var_is_in_clinvar(variant)
            output_data['in_cosmic'] = vcf_parsing.var_is_in_cosmic(variant)
            output_data['is_pathogenic'] = vcf_parsing.var_is_pathogenic(variant)
            output_data['is_lof'] = vcf_parsing.var_is_lof(variant)
            output_data['is_coding'] = vcf_parsing.var_is_coding(variant)
            output_data['is_splicing'] = vcf_parsing.var_is_splicing(variant)
            output_data['rs_ids'] = variant.INFO.get('rs_ids')
            output_data['cosmic_ids'] = variant.INFO.get('cosmic_id')

            output_data['max_maf_all'] = variant.INFO.get('max_aaf_all') or -1
            output_data['max_maf_no_fin'] = variant.INFO.get('max_aaf_no_fin') or -1
            output_data['esp_ea'] = population_freqs['esp_ea']
            output_data['esp_aa'] = population_freqs['esp_aa']
            output_data['esp_all'] = population_freqs['esp_all']
            output_data['1kg_amr'] = population_freqs['1kg_amr']
            output_data['1kg_eas'] = population_freqs['1kg_eas']
            output_data['1kg_sas'] = population_freqs['1kg_sas']
            output_data['1kg_afr'] = population_freqs['1kg_afr']
            output_data['1kg_eur'] = population_freqs['1kg_eur']
            output_data['1kg_all'] = population_freqs['1kg_all']
            output_data['exac_all'] = population_freqs['exac_all']
            output_data['adj_exac_all'] = population_freqs['adj_exac_all']
            output_data['adj_exac_afr'] = population_freqs['adj_exac_afr']
            output_data['adj_exac_amr'] = population_freqs['adj_exac_amr']
            output_data['adj_exac_eas'] = population_freqs['adj_exac_eas']
            output_data['adj_exac_fin'] = population_freqs['adj_exac_fin']
            output_data['adj_exac_nfe'] = population_freqs['adj_exac_nfe']
            output_data['adj_exac_oth'] = population_freqs['adj_exac_oth']
            output_data['adj_exac_sas'] = population_freqs['adj_exac_sas']

            output_data['max_som_aaf'] = max_som_aaf
            output_data['min_depth'] = min_depth
            output_data['max_depth'] = max_depth

            if caller_variant_data_dicts['mutect']:
                output_data['mutect_AF'] = caller_variant_data_dicts['mutect']['AAF']
                output_data['mutect_DP'] = caller_variant_data_dicts['mutect']['DP']
                output_data['mutect_Filters'] = caller_variant_data_dicts['mutect']['FILTER']
            else:
                output_data['mutect_AF'] = None
                output_data['mutect_DP'] = None
                output_data['mutect_Filters'] = None

            if caller_variant_data_dicts['freebayes']:
                output_data['freebayes_AF'] = caller_variant_data_dicts['freebayes']['AAF'] or 'None'
                output_data['freebayes_DP'] = caller_variant_data_dicts['freebayes']['DP'] or 'None'
                output_data['freebayes_Filters'] = caller_variant_data_dicts['freebayes']['FILTER'] or 'None'
            else:
                output_data['freebayes_AF'] = None
                output_data['freebayes_DP'] = None
                output_data['freebayes_Filters'] = None

            if caller_variant_data_dicts['scalpel']:
                output_data['scalpel_AF'] = caller_variant_data_dicts['scalpel']['AAF'] or 'None'
                output_data['scalpel_DP'] = caller_variant_data_dicts['scalpel']['DP'] or 'None'
                output_data['scalpel_Filters'] = caller_variant_data_dicts['scalpel']['FILTER'] or 'None'
            else:
                output_data['scalpel_AF'] = None
                output_data['scalpel_DP'] = None
                output_data['scalpel_Filters'] = None

            if caller_variant_data_dicts['platypus']:
                output_data['platypus_AF'] = caller_variant_data_dicts['platypus']['AAF'] or 'None'
                output_data['platypus_DP'] = caller_variant_data_dicts['platypus']['DP'] or 'None'
                output_data['platypus_Filters'] = caller_variant_data_dicts['platypus']['FILTER'] or 'None'
            else:
                output_data['platypus_AF'] = None
                output_data['platypus_DP'] = None
                output_data['platypus_Filters'] = None

            if caller_variant_data_dicts['pindel']:
                output_data['pindel_AF'] = caller_variant_data_dicts['pindel']['AAF'] or 'None'
                output_data['pindel_DP'] = caller_variant_data_dicts['pindel']['DP'] or 'None'
                output_data['pindel_Filters'] = caller_variant_data_dicts['pindel']['FILTER'] or 'None'
            else:
                output_data['pindel_AF'] = None
                output_data['pindel_DP'] = None
                output_data['pindel_Filters'] = None

            if caller_variant_data_dicts['vardict']:
                output_data['vardict_AF'] = caller_variant_data_dicts['vardict']['AAF'] or 'None'
                output_data['vardict_DP'] = caller_variant_data_dicts['vardict']['DP'] or 'None'
                output_data['vardict_Filters'] = caller_variant_data_dicts['vardict']['FILTER'] or 'None'
            else:
                output_data['vardict_AF'] = None
                output_data['vardict_DP'] = None
                output_data['vardict_Filters'] = None

            writer.writerow(output_data)

    return variant_frequencies

def write_variant_to_sheet(sample_id, variant, vaf_data, sheet, style, row):
    column_index = 0
    sheet.write(row, column_index, f"{sample_id}", style)
    column_index += 1

    sheet.write(row, column_index, f"{variant['gene']}", style)
    column_index += 1

    sheet.write(row, column_index, f"{variant['amplicons']}", style)
    column_index += 1

    sheet.write(row, column_index, f"{variant['transcript']}", style)
    column_index += 1

    sheet.write(row, column_index, f"{variant['Ref']}", style)
    column_index += 1

    sheet.write(row, column_index, f"{variant['Alt']}", style)
    column_index += 1

    sheet.write(row, column_index, f"{variant['codon_change']}", style)
    column_index += 1

    sheet.write(row, column_index, f"{variant['aa_change']}", style)
    column_index += 1

    #Ensembl ID
    sheet.write(row, column_index, f"-", style)
    column_index += 1

    # RefSeq ID
    sheet.write(row, column_index, f"-", style)
    column_index += 1

    # Representative VAF
    sheet.write(row, column_index, f"-", style)
    column_index += 1

    sheet.write(row, column_index, round(float(variant['max_som_aaf']), 3), style)
    column_index += 1

    # Num Times in DB
    sheet.write(row, column_index, f"-", style)
    column_index += 1

    # Num Times in Run
    sheet.write(row, column_index, int(vaf_data[0]), style)
    column_index += 1

    # Median VAF in DB
    sheet.write(row, column_index, f"-", style)
    column_index += 1

    # Median VAF in Run
    sheet.write(row, column_index, round(float(vaf_data[1]), 3), style)
    column_index += 1

    # Std Dev in Run
    sheet.write(row, column_index, round(float(vaf_data[3]), 3), style)
    column_index += 1

    # Percentile Rank in DB
    sheet.write(row, column_index, f"-", style)
    column_index += 1

    sheet.write(row, column_index, f"{variant['callers']}", style)
    column_index += 1

    # Filters
    sheet.write(row, column_index, f"{'.'}", style)
    column_index += 1

    # Caller Counts
    sheet.write(row, column_index, f"-", style)
    column_index += 1

    sheet.write(row, column_index, f"{variant['cosmic_ids']}", style)
    column_index += 1

    sheet.write(row, column_index, f"{variant['cosmic_numsamples']}", style)
    column_index += 1

    sheet.write(row, column_index, f"{variant['cosmic_aa']}", style)
    column_index += 1

    sheet.write(row, column_index, f"{variant['clinvar_pathogenic']}", style)
    column_index += 1

    sheet.write(row, column_index, f"{variant['clinvar_hgvs']}", style)
    column_index += 1

    sheet.write(row, column_index, f"{variant['clinvar_disease']}", style)
    column_index += 1

    # Coverage of Amplicon
    sheet.write(row, column_index, f"-", style)
    column_index += 1

    # Number of Reads for Amplicon
    sheet.write(row, column_index, f"-", style)
    column_index += 1

    sheet.write(row, column_index, f"{variant['impact']}", style)
    column_index += 1

    sheet.write(row, column_index, f"{variant['severity']}", style)
    column_index += 1

    sheet.write(row, column_index, float(f"{variant['max_maf_all']}"), style)
    column_index += 1

    sheet.write(row, column_index, int(f"{variant['min_depth']}"), style)
    column_index += 1

    sheet.write(row, column_index, int(f"{variant['max_depth']}"), style)
    column_index += 1

    sheet.write(row, column_index, f"{variant['Chr']}", style)
    column_index += 1

    sheet.write(row, column_index, int(f"{variant['Pos']}"), style)
    column_index += 1

    # End Position
    sheet.write(row, column_index, f"-", style)
    column_index += 1

    sheet.write(row, column_index, f"{variant['rs_ids']}", style)
    column_index += 1

    # Other Samples in Run with this Variant
    sheet.write(row, column_index, f"-", style)
    column_index += 1

    if variant['mutect_AF']:
        sheet.write(row, column_index, float(f"{variant['mutect_AF']}"), style)
        column_index += 1
        sheet.write(row, column_index, int(f"{variant['mutect_DP']}"), style)
        column_index += 1
        # sheet.write(row, column_index, f"{variant['mutect_Filters']}", style)
        # column_index += 1
    else:
        sheet.write(row, column_index, f"{variant['mutect_AF']}", style)
        column_index += 1
        sheet.write(row, column_index, f"{variant['mutect_DP']}", style)
        column_index += 1
        # sheet.write(row, column_index, f"{variant['mutect_Filters']}", style)
        # column_index += 1

    if variant['vardict_AF']:
        sheet.write(row, column_index, float(f"{variant['vardict_AF']}"), style)
        column_index += 1
        sheet.write(row, column_index, int(f"{variant['vardict_DP']}"), style)
        column_index += 1
        # sheet.write(row, column_index, f"{variant['vardict_Filters']}", style)
        # column_index += 1
    else:
        sheet.write(row, column_index, f"{variant['vardict_AF']}", style)
        column_index += 1
        sheet.write(row, column_index, f"{variant['vardict_DP']}", style)
        column_index += 1
        # sheet.write(row, column_index, f"{variant['mutect_Filters']}", style)
        # column_index += 1

    if variant['freebayes_AF']:
        sheet.write(row, column_index, float(f"{variant['freebayes_AF']}"), style)
        column_index += 1
        sheet.write(row, column_index, int(f"{variant['freebayes_DP']}"), style)
        column_index += 1
        # sheet.write(row, column_index, f"{variant['freebayes_Filters']}", style)
        # column_index += 1
    else:
        sheet.write(row, column_index, f"{variant['freebayes_AF']}", style)
        column_index += 1
        sheet.write(row, column_index, f"{variant['freebayes_DP']}", style)
        column_index += 1
        # sheet.write(row, column_index, f"{variant['mutect_Filters']}", style)
        # column_index += 1

    if variant['scalpel_AF']:
        sheet.write(row, column_index, float(f"{variant['scalpel_AF']}"), style)
        column_index += 1
        sheet.write(row, column_index, int(f"{variant['scalpel_DP']}"), style)
        column_index += 1
        # sheet.write(row, column_index, f"{variant['scalpel_Filters']}", style)
        # column_index += 1
    else:
        sheet.write(row, column_index, f"{variant['scalpel_AF']}", style)
        column_index += 1
        sheet.write(row, column_index, f"{variant['scalpel_DP']}", style)
        column_index += 1
        # sheet.write(row, column_index, f"{variant['mutect_Filters']}", style)
        # column_index += 1

    if variant['platypus_AF']:
        sheet.write(row, column_index, float(f"{variant['platypus_AF']}"), style)
        column_index += 1
        sheet.write(row, column_index, int(f"{variant['platypus_DP']}"), style)
        column_index += 1
        # sheet.write(row, column_index, f"{variant['platypus_Filters']}", style)
        # column_index += 1
    else:
        sheet.write(row, column_index, f"{variant['platypus_AF']}", style)
        column_index += 1
        sheet.write(row, column_index, f"{variant['platypus_DP']}", style)
        column_index += 1
        # sheet.write(row, column_index, f"{variant['mutect_Filters']}", style)
        # column_index += 1

    if variant['pindel_AF']:
        sheet.write(row, column_index, float(f"{variant['pindel_AF']}"), style)
        column_index += 1
        sheet.write(row, column_index, int(f"{variant['pindel_DP']}"), style)
        column_index += 1
        # sheet.write(row, column_index, f"{variant['pindel_Filters']}", style)
        # column_index += 1
    else:
        sheet.write(row, column_index, f"{variant['pindel_AF']}", style)
        column_index += 1
        sheet.write(row, column_index, f"{variant['pindel_DP']}", style)
        column_index += 1
        # sheet.write(row, column_index, f"{variant['mutect_Filters']}", style)
        # column_index += 1

def classify_variant(variant, sheets):
    sheet = "med_sheet"

    tier1_clinvar_terms = ("Pathogenic", "Likely_pathogenic", "drug_response", "_drug_response", "Pathogenic/Likely_pathogenic")
    benign_clinvar_terms = ("Benign", "Likely_benign", "likely_benign", "Benign/Likely_benign")

    if variant['severity'] == 'HIGH':
        if int(variant['max_depth']) >= 100:
            sheet = "high_sheet"
        else:
            sheet = "high_fail_sheet"
    elif variant['severity'] == 'MED':
        if int(variant['max_depth']) >= 100:
            sheet = "med_sheet"
        else:
            sheet = "med_fail_sheet"
    else:
        if int(variant['max_depth']) >= 100:
            sheet = "low_sheet"
        else:
            sheet = "low_fail_sheet"

    if variant['in_cosmic'] == "True":
        if int(variant['max_depth']) >= 100:
            sheet = "cosvar_sheet"
        else:
            sheet = "cosvar_fail_sheet"

    if variant['in_clinvar'] == "True":
        pathogenicity_terms_string = variant['clinvar_pathogenic']
        pathogenicity_terms = pathogenicity_terms_string.split(',')
        if any(i in benign_clinvar_terms for i in pathogenicity_terms):
            if int(variant['max_depth']) >= 100:
                sheet = "benign_sheet"
            else:
                sheet = "benign_fail_sheet"
        else:
            if int(variant['max_depth']) >= 100:
                sheet = "cosvar_sheet"
            else:
                sheet = "cosvar_fail_sheet"

    if variant['callers'] == "freebayes" or variant['callers'] == "pindel":
        if int(variant['max_depth']) >= 100:
            sheet = "freepin_sheet"
        else:
            sheet = "freepin_fail_sheet"

    return sheet

def variant_style(variant, variant_id, styles, run_vaf_data):
    style = styles['default_style']

    num_times_run = int(run_vaf_data[0])
    median_vaf = float(run_vaf_data[1])
    std_dev = float(run_vaf_data[3])

    if float(variant['max_som_aaf']) > 0.05:
        style = styles['interest_style']
        if int(run_vaf_data[0]) >= 10:
            style = styles['error_style']
    if variant['cosmic_ids']:
        num_list = variant['cosmic_numsamples'].strip("()")
        nums = num_list.split(',')
        for num in nums:
            if num == 'None':
                num = 0
            if int(num) >= 5:
                style = styles['interest_style']
        if int(run_vaf_data[0]) >= 10:
            style = styles['error_style']

    if len(variant['callers'].split(',')) == 1:
        style = styles['warning_style']
    if len(variant['Ref']) > 200:
        style = styles['warning_style']
    if len(variant['Alt']) > 200:
        style = styles['warning_style']
    if len(variant['codon_change']) > 200:
        style = styles['warning_style']

    return style

def set_up_sheet_header(column_index, sheet, callers, style):
    sheet.autofilter('A1:AY10000')
    sheet.write(0, column_index, "Library", style)
    column_index += 1

    sheet.write(0, column_index, "Gene", style)
    column_index += 1

    sheet.write(0, column_index, "Amplicon", style)
    column_index += 1

    sheet.write(0, column_index, "Ensembl Transcript", style)
    column_index += 1

    sheet.write(0, column_index, "Ref", style)
    column_index += 1

    sheet.write(0, column_index, "Alt", style)
    column_index += 1

    sheet.write(0, column_index, "SnpEff c.HGVS (auto)", style)
    column_index += 1

    sheet.write(0, column_index, "SnpEff p.HGVS (auto)", style)
    column_index += 1

    sheet.write(0, column_index, "Ensembl HGVS", style)
    column_index += 1

    sheet.write(0, column_index, "RefSeq HGVS", style)
    column_index += 1

    sheet.write(0, column_index, "Representative VAF", style)
    column_index += 1

    sheet.write(0, column_index, "Max Caller Somatic VAF", style)
    column_index += 1

    sheet.write(0, column_index, "Num Times in Database", style)
    column_index += 1

    sheet.write(0, column_index, "Num Times in Run", style)
    column_index += 1

    sheet.write(0, column_index, "Median VAF in DB", style)
    column_index += 1

    sheet.write(0, column_index, "Median VAF in Run", style)
    column_index += 1

    sheet.write(0, column_index, "StdDev VAF", style)
    column_index += 1

    sheet.write(0, column_index, "VAF Percentile Rank", style)
    column_index += 1

    sheet.write(0, column_index, "Callers", style)
    column_index += 1

    sheet.write(0, column_index, "Filters", style)
    column_index += 1

    sheet.write(0, column_index, "Caller Counts", style)
    column_index += 1

    sheet.write(0, column_index, "COSMIC IDs", style)
    column_index += 1

    sheet.write(0, column_index, "Num COSMIC Samples", style)
    column_index += 1

    sheet.write(0, column_index, "COSMIC AA", style)
    column_index += 1

    sheet.write(0, column_index, "Clinvar Pathogenicity", style)
    column_index += 1

    sheet.write(0, column_index, "Clinvar HGVS", style)
    column_index += 1

    sheet.write(0, column_index, "Clinvar Disease", style)
    column_index += 1

    sheet.write(0, column_index, "Coverage", style)
    column_index += 1

    sheet.write(0, column_index, "Num Reads", style)
    column_index += 1

    sheet.write(0, column_index, "Impact", style)
    column_index += 1

    sheet.write(0, column_index, "Severity", style)
    column_index += 1

    sheet.write(0, column_index, "Maximum Population AF", style)
    column_index += 1

    sheet.write(0, column_index, "Min Caller Depth", style)
    column_index += 1

    sheet.write(0, column_index, "Max Caller Depth", style)
    column_index += 1

    sheet.write(0, column_index, "Chrom", style)
    column_index += 1

    sheet.write(0, column_index, "Start", style)
    column_index += 1

    sheet.write(0, column_index, "End", style)
    column_index += 1

    sheet.write(0, column_index, "rsIDs", style)
    column_index += 1

    sheet.write(0, column_index, "Matching Samples in Run", style)
    column_index += 1


    if 'mutect' in callers:
        sheet.write(0, column_index, "MuTect_AF", style)
        column_index += 1

        sheet.write(0, column_index, "MuTect_DP", style)
        column_index += 1

    if 'vardict' in callers:
        sheet.write(0, column_index, "VarDict_AF", style)
        column_index += 1

        sheet.write(0, column_index, "VarDict_DP", style)
        column_index += 1

    if 'freebayes' in callers:
        sheet.write(0, column_index, "FreeBayes_AF", style)
        column_index += 1

        sheet.write(0, column_index, "FreeBayes_DP", style)
        column_index += 1

    if 'scalpel' in callers:
        sheet.write(0, column_index, "Scalpel_AF", style)
        column_index += 1

        sheet.write(0, column_index, "Scalpel_DP", style)
        column_index += 1

    if 'platypus' in callers:
        sheet.write(0, column_index, "Platypus_AF", style)
        column_index += 1

        sheet.write(0, column_index, "Platypus_DP", style)
        column_index += 1

    if 'pindel' in callers:
        sheet.write(0, column_index, "Pindel_AF", style)
        column_index += 1

        sheet.write(0, column_index, "Pindel_DP", style)
        column_index += 1

def set_up_reporting_sheet(sheet, callers, styles):
    column_index = 0

    sheet.write(0, column_index, "IGV Review")
    column_index += 1
    sheet.write(0, column_index, "Review Status")
    column_index += 1

    set_up_sheet_header(column_index, sheet, callers, styles['header_style'])

    igv_list = ['Unreviewed', 'IGV/Manual', 'Prev Reviewed']
    status_list = ['Unreviewed', 'R - Pass', 'R - Suspicious', 'R - Artifact',
    'F - Pass', 'F - Suspicious', 'F -Artifact']

    sheet.write('A2', 'Unreviewed')
    sheet.data_validation('A2', {'validate': 'list', 'source': igv_list})

    sheet.write('B2', 'Unreviewed')
    sheet.data_validation('B2', {'validate': 'list', 'source':status_list})

    sheet.write(10, 0, "Suspicious Variants")
    sheet.write('A12', 'Unreviewed')
    sheet.data_validation('A12', {'validate': 'list', 'source':igv_list})

    sheet.write('B12', 'Unreviewed')
    sheet.data_validation('B12', {'validate': 'list', 'source':status_list})

    sheet.write(15, 0, "VUS")
    sheet.write('A17', 'Unreviewed')
    sheet.data_validation('A17', {'validate': 'list', 'source':igv_list})

    sheet.write('B17', 'Unreviewed')
    sheet.data_validation('B17', {'validate': 'list', 'source':status_list})

def parse_run_vafs(file):
    run_vafs = defaultdict(list)
    # Structure: ["Variant_ID", "Num Times in Run", "Median VAF", "Mean VAF", "StdDev", "VAFs"]
    with open(file, 'r') as csvfile:
        reader = csv.reader(csvfile)
        header = next(reader)
        for row in reader:
            run_vafs[row[0]] = [row[1], row[2], row[3], row[4], row[5]]

    return run_vafs

def get_coverage_data(sample, target_amplicons):
    sample_id = sample['sample_id']
    coverage_data = defaultdict(lambda: dict())
    coverage_file = f"../Coverage/{sample_id}.sambamba_regioncoverage.bed"

    with open(coverage_file, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t')
        header = next(reader)
        for row in reader:
            if row[3] in target_amplicons:
                coverage_data[row[3]]['num_reads'] = int(row[4])
                coverage_data[row[3]]['mean_coverage'] = float(row[5])
                coverage_data[row[3]]['perc_bp_500'] = float(row[6])
                coverage_data[row[3]]['perc_bp_1000'] = float(row[7])

    return coverage_data

def write_coveragesheet(coverage_sheet, coverage_data, sample, styles):
    sample_id = sample['sample_id']

    coverage_sheet.write(0, 0, "Sample")
    coverage_sheet.write(0, 1, f"{sample_id}")

    coverage_sheet.write(1, 0, "Libraries")
    coverage_sheet.write(1, 1, f"{sample_id}")

    coverage_sheet.write(2, 0, "Run ID")
    coverage_sheet.write(2, 1, f"")

    coverage_sheet.write(3, 0, "Reporting Templates")
    coverage_sheet.write(3, 1, f"{sample['report_panel']}")

    coverage_sheet.write(4, 0, "Minimum Somatic Allele Frequency")
    coverage_sheet.write(4, 1, f"")

    coverage_sheet.write(5, 0, "Minimum Amplicon Depth")
    coverage_sheet.write(5, 1, f"")

    coverage_sheet.write(6, 0, "Maximum Population Allele Frequency")
    coverage_sheet.write(6, 1, f"")

    coverage_sheet.write(7, 0, "Depth Fail Criteria: Max Caller DP")
    coverage_sheet.write(7, 1, f"")

    coverage_sheet.write(8, 0, "Bioinformatics Pipeline Version")
    coverage_sheet.write(8, 1, f"")

    coverage_sheet.write(9, 0, "COSMIC Version")
    coverage_sheet.write(9, 1, f"")

    coverage_sheet.write(10, 0, "ClinVar Version")
    coverage_sheet.write(10, 1, f"")

    coverage_sheet.write(12, 0, "Sample")
    coverage_sheet.write(12, 1, "Library")
    coverage_sheet.write(12, 2, "Amplicon")
    coverage_sheet.write(12, 3, "Num Reads")
    coverage_sheet.write(12, 4, "Coverage")
    coverage_sheet.write(12, 5, "Database Median")
    coverage_sheet.write(12, 6, "Database Std Dev")
    coverage_sheet.write(12, 7, "Percent bp > 500X")
    coverage_sheet.write(12, 8, "Percent bp > 1000X")

    coverage_sheet.write("F1", "1st Reviewer:")
    coverage_sheet.write("F2", "2nd Reviewer:")
    coverage_sheet.write("F3", "Final Review:")

    reviewers_list = ['DG', 'JW', 'RX', 'MC', 'SF', 'TG']

    coverage_sheet.write('G1', 'Reviewer')
    coverage_sheet.data_validation('G1', {'validate': 'list', 'source': reviewers_list})

    coverage_sheet.write('G2', 'Reviewer')
    coverage_sheet.data_validation('G2', {'validate': 'list', 'source': reviewers_list})

    coverage_sheet.write('G3', 'Reviewer')
    coverage_sheet.data_validation('G3', {'validate': 'list', 'source': reviewers_list})

    row_num = 13

    failed_coverage_amplicons = list()

    for amplicon in coverage_data.keys():
        if coverage_data[amplicon]['mean_coverage'] < 250:
            style = styles['error_style']
            failed_coverage_amplicons.append(amplicon)
        elif coverage_data[amplicon]['mean_coverage'] < 500:
            style = styles['warning_style']
        else:
            style = styles['pass_style']

        coverage_sheet.write(row_num, 0, f"{sample_id}", style)
        coverage_sheet.write(row_num, 1, f"{sample_id}", style)
        coverage_sheet.write(row_num, 2, f"{amplicon}", style)
        coverage_sheet.write(row_num, 3, f"{coverage_data[amplicon]['num_reads']}", style)
        coverage_sheet.write(row_num, 4, f"{round(coverage_data[amplicon]['mean_coverage'], 0)}", style)
        coverage_sheet.write(row_num, 5, f"", style)
        coverage_sheet.write(row_num, 6, f"", style)
        coverage_sheet.write(row_num, 7, f"{coverage_data[amplicon]['perc_bp_500']}", style)
        coverage_sheet.write(row_num, 8, f"{coverage_data[amplicon]['perc_bp_1000']}", style)

        row_num += 1

    coverage_sheet.write(10, 12, "Failed Amplicons (< 250X Median Coverage)")
    coverage_sheet.write(10, 13, "Coverage")
    row_num = 11
    for amplicon in failed_coverage_amplicons:
        coverage_sheet.write(row_num, 12, f"{amplicon}")
        coverage_sheet.write(row_num, 13, f"{round(coverage_data[amplicon]['mean_coverage'], 0)}")
        row_num += 1

def create_sample_report(data):
    sample = data[0]
    run_vafs = data[1]
    sys.stdout.write(f"Creating excel report for {sample['sample_id']}...\n")
    callers = "mutect, freebayes, vardict, scalpel, platypus, pindel"
    sample_id = sample['sample_id']
    report_name = f"{sample_id}.xlsx"
    sample_variants = f"{sample_id}.variants.csv"
    report_panel_path = f"/home/dgaston/ddb-configs/disease_panels/{sample['seq_panel']}/{sample['report_panel']}"
    target_amplicons = utils.get_target_amplicons(report_panel_path)
    amplicon_coverage_data = get_coverage_data(sample, target_amplicons)

    report_data = dict()
    filtered_variant_data = defaultdict(list)
    off_target_amplicon_counts = defaultdict(int)
    target_amplicon_coverage = dict()
    ordered_amplicon_coverage = list()
    reportable_amplicons = list()

    iterated = 0
    passing_variants = 0
    filtered_low_freq = 0
    filtered_low_depth = 0
    filtered_off_target = 0

    wb = xlsxwriter.Workbook(report_name, {'strings_to_numbers': True,
                                           'nan_inf_to_errors': True})

    # Set up Styles
    styles = dict()
    styles['error_style'] = wb.add_format({"bg_color": "#FFC7CE", "font_color": "#9C0006"})
    styles['pass_style'] = wb.add_format({"bg_color": "#C6EFCE", "font_color": "#006100"})
    styles['warning_style'] = wb.add_format({'bg_color': '#FF9900'})
    styles['interest_style'] = wb.add_format({'bg_color': '#808080'})
    styles['default_style'] = wb.add_format({'bg_color': '#FFFFFF'})
    styles['header_style'] = wb.add_format({'bold': 1})

    sheets = dict()
    # Set up Worksheets
    sheets['coverage_sheet'] = wb.add_worksheet("Coverage")
    sheets['report_sheet'] = wb.add_worksheet("Reporting Variants")
    sheets['cosvar_sheet'] = wb.add_worksheet("ClinVar COSMIC Pass")
    sheets['high_sheet'] = wb.add_worksheet("High Impact Pass")
    sheets['med_sheet'] = wb.add_worksheet("Med Impact Pass")
    sheets['low_sheet'] = wb.add_worksheet("Low Impact Pass")
    sheets['benign_sheet'] = wb.add_worksheet("Benign Pass")
    sheets['freepin_sheet'] = wb.add_worksheet("FreeBayes Pindel Only")
    sheets['cosvar_fail_sheet'] = wb.add_worksheet("ClinVar COSMIC Fail")
    sheets['high_fail_sheet'] = wb.add_worksheet("High Impact Fail")
    sheets['med_fail_sheet'] = wb.add_worksheet("Med Impact Fail")
    sheets['low_fail_sheet'] = wb.add_worksheet("Low Impact Fail")
    sheets['freepin_fail_sheet'] = wb.add_worksheet("FreeBayes Pindel Only Fail")
    sheets['benign_fail_sheet'] = wb.add_worksheet("Benign Fail")

    write_coveragesheet(sheets['coverage_sheet'], amplicon_coverage_data, sample, styles)
    set_up_reporting_sheet(sheets['report_sheet'], callers, styles)

    for sheet in sheets:
        column_index = 0
        row = 1
        if sheet != "coverage_sheet":
            set_up_sheet_header(column_index, sheets[sheet], callers, styles['header_style'])

    with open(sample_variants, 'r') as csvfile:
        reader = csv.DictReader(csvfile)
        row_counts = defaultdict(lambda: 1)
        for variant in reader:
            if float(variant['max_som_aaf']) >= 0.01:
                amplicons = variant['amplicons'].split(',')
                if any(i in target_amplicons for i in amplicons):
                    variant_id = f"{variant['Chr']}-{variant['Pos']}-{variant['Ref']}-{variant['Alt']}"
                    vaf_data = run_vafs[variant_id]
                    style = variant_style(variant, variant_id, styles, vaf_data)
                    sheet = classify_variant(variant, sheets)
                    write_variant_to_sheet(sample_id, variant, vaf_data, sheets[sheet], style, row_counts[sheet])
                    row_counts[sheet] += 1

    wb.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--samples_file', help="Input file for samples")
    parser.add_argument('-t', '--threads', help="Number of CPU Threads")
    parser.add_argument('-l', '--snps_list', help="Input SNP list")

    args = parser.parse_args()
    args.logLevel = "INFO"

    pool = Pool(args.threads)

    samples = samples_config.parse(args.samples_file)

    variant_frequencies = defaultdict(list)
    for sample in samples:
        variant_frequencies = convert_sample(sample, variant_frequencies)

    with open("run_var_frequencies_summary.csv", 'w') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(["Variant_ID", "Num Times in Run", "Median VAF", "Mean VAF", "StdDev", "VAFs"])
        for variant in variant_frequencies.keys():
            vafs = variant_frequencies[variant]
            vaf_string = "|".join([str(i) for i in vafs])
            writer.writerow([variant, len(vafs), np.median(vafs), np.mean(vafs), np.std(vafs), vaf_string])

    run_vafs = parse_run_vafs("run_var_frequencies_summary.csv")
    sample_data = list()

    for sample in samples:
        data = ((sample, run_vafs))
        sample_data.append(data)

    results = pool.map(create_sample_report, sample_data)

    sys.stdout.write("Creating SNP report\n")
    samples = samples_config.parse(args.samples_file)
    wb = xlsxwriter.Workbook(args.report)
    sheet = wb.add_worksheet("SNPs")
    all_callers = wb.add_worksheet("All Callers")

    snps = list()

    with open(args.snps_list, 'r') as snp_file:
        reader = csv.reader(snp_file, dialect='excel-tab')
        sheet.write(0, 0, "SNP")
        row_num = 1
        for row in reader:
            variant_id = f"{row[0]}-{row[1]}-{row[2]}-{row[3]}"
            snps.append(variant_id)
            sheet.write(row_num, 0, f"{variant_id}")
            row_num += 1

    col_num = 1
    for sample in samples:
        sheet.write(0, col_num, f"{sample}")
        variant_vafs = dict()
        with open(f"{sample}.variants.csv", 'r') as csvfile:
            reader = csv.DictReader(csvfile)
            for variant in reader:
                variant_id = f"{variant['Chr']}-{variant['Pos']}-{variant['Ref']}-{variant['Alt']}"
                if variant_id in snps:
                    variant_vafs[variant_id] = variant['freebayes_AF']
            row_num = 1
            for snp in snps:
                sheet.write(row_num, col_num, f"{variant_vafs[variant_id]}")
                row_num += 1
        col_num += 1

    wb.close()
