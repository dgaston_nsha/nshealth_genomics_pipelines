#!/usr/bin/env python

import sys
import csv
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--index_file', help="Lab list of illumina indices and sequences")
    parser.add_argument('-s', '--samples', help="samples csv file")
    parser.add_argument('-o', '--output', help="Output samples csv file")

    args = parser.parse_args()
    args.logLevel = "INFO"
    with open(args.index_file, 'r') as index:
        lookup = index.readlines()

    with open (args.output, 'w') as outfile:
        with open(args.samples, 'r') as infile:
            reader = csv.reader(infile, delimiter=',')
            writer = csv.writer(outfile, delimiter=',')

            for row in reader:
                for look_row in lookup:
                    look_row_elements = look_row.split(',')
                    if row[4] == look_row_elements[6] and row[5] == look_row_elements[8]:
                        row.append(look_row_elements[5])
                        row.append(look_row_elements[7])
                        row.append(look_row_elements[4])
                        writer.writerow(row)
