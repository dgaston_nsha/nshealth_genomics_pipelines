#!/usr/bin/env python

# Standard packages
import os
import sys
import csv
import argparse
import xlsxwriter

from modules import configuration

from collections import defaultdict

version = "1.1"

def get_imbalance_counts(file):
    read_counts = defaultdict(int)
    if os.path.exists(file):
        with open(file, 'r') as csvfile:
            reader = csv.reader(csvfile, delimiter='\t')
            for row in reader:
                read_counts[f"{row[0]}-{row[1]}-{row[2]}"] = int(row[3])

    return read_counts


def write_csv_data(sheet, filefh):
    with open(filefh, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t')
        row_count = 0
        for row in reader:
            column_count = 0
            for column in row:
                sheet.write(row_count, column_count, column)
                column_count += 1
            row_count += 1

def read_counts(filefh):
    read_counts = dict()
    with open(filefh, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t')
        for row in reader:
            summed_count = int(row[1]) + int(row[2]) + int(row[3])
            gene_id = row[0][0:15]
            read_counts[gene_id] = summed_count

    return read_counts

def write_qc_data(qc_sheet, samples, sample, fusioninspector_log_fh, star_log_fh, arriba_log_fh, run_qc_metrics):
    qc_sheet.write("A1", "Run ID:")
    qc_sheet.write("B1", f"{samples[sample]['run_id']}")

    reviewers_list = ['DG', 'JW', 'RX', 'MC', 'SF', 'TG']

    qc_sheet.write("D1", "1st Reviewer:")
    qc_sheet.write("D2", "2nd Reviewer:")
    qc_sheet.write("D3", "Final Review:")

    qc_sheet.write('E1', 'Reviewer')
    qc_sheet.data_validation('E1', {'validate': 'list', 'source': reviewers_list})

    qc_sheet.write('E2', 'Reviewer')
    qc_sheet.data_validation('E2', {'validate': 'list', 'source': reviewers_list})

    qc_sheet.write('E3', 'Reviewer')
    qc_sheet.data_validation('E3', {'validate': 'list', 'source': reviewers_list})

    qc_sheet.write("A3", "STAR-Fusion Log Data")
    if os.path.exists(star_log_fh):
        with open(star_log_fh, 'r') as file:
            log_file = file.readlines()
            line = log_file[5].split('\t')
            qc_sheet.write("A4", f"Number of Input Reads")
            qc_sheet.write("B4", int(line[1].strip()))
            run_qc_metrics[sample]['star_reads'] = line[1].strip()

            line = log_file[6].split('\t')
            qc_sheet.write("A5", f"Avg Input Read Length")
            qc_sheet.write("B5", float(line[1].strip()))
            run_qc_metrics[sample]['star_read_len'] = line[1].strip()

            line = log_file[8].split('\t')
            line2 = log_file[9].split('\t')
            qc_sheet.write("A6", f"Number Uniquely Mapped (%)")
            qc_sheet.write("B6", f"{int(line[1].strip())} ({line2[1].strip()})")
            run_qc_metrics[sample]['star_perc_mapped'] = line2[1].strip()

            line = log_file[10].split('\t')
            qc_sheet.write("A7", f"Average Mapped Length")
            qc_sheet.write("B7", float(line[1].strip()))
            run_qc_metrics[sample]['star_mapped_len'] = line[1].strip()

            line = log_file[30].split('\t')
            qc_sheet.write("A8", f"Number Unmapped (too short)")
            qc_sheet.write("B8", int(line[1].strip()))
            run_qc_metrics[sample]['star_perc_unmapped_short'] = line[1].strip()

            line = log_file[35].split('\t')
            qc_sheet.write("A9", f"Number Chimeric")
            qc_sheet.write("B9", int(line[1].strip()))
            run_qc_metrics[sample]['star_perc_chimeric'] = line[1].strip()
    else:
        qc_sheet.write(3, 0, f"WARNING: STAR-Fusion not run on sample")

    qc_sheet.write(9, 0, "FusionInspector Log Data")
    if os.path.exists(fusioninspector_log_fh):
        with open(fusioninspector_log_fh, 'r') as file:
            log_file = file.readlines()
            line = log_file[5].split('\t')
            qc_sheet.write(10, 0, f"Number of Input Reads")
            qc_sheet.write(10, 1, int(line[1].strip()))
            run_qc_metrics[sample]['insp_reads'] = line[1].strip()

            line = log_file[6].split('\t')
            qc_sheet.write(11, 0, f"Avg Input Read Length")
            qc_sheet.write(11, 1, float(line[1].strip()))
            run_qc_metrics[sample]['insp_read_len'] = line[1].strip()

            line = log_file[8].split('\t')
            line2 = log_file[9].split('\t')
            qc_sheet.write(12, 0, f"Number Uniquely Mapped")
            qc_sheet.write(12, 1, f"{int(line[1].strip())} ({line2[1].strip()})")
            run_qc_metrics[sample]['insp_perc_mapped'] = line2[1].strip()

            line = log_file[10].split('\t')
            qc_sheet.write(13, 0, f"Average Mapped Length")
            qc_sheet.write(13, 1, float(line[1].strip()))
            run_qc_metrics[sample]['insp_mapped_len'] = line[1].strip()

            line = log_file[30].split('\t')
            qc_sheet.write(14, 0, f"Number Unmapped (too short)")
            qc_sheet.write(14, 1, int(line[1].strip()))
            run_qc_metrics[sample]['insp_perc_unmapped_short'] = line[1].strip()

            line = log_file[35].split('\t')
            qc_sheet.write(15, 0, f"Number Chimeric")
            qc_sheet.write(15, 1, int(line[1].strip()))
            run_qc_metrics[sample]['insp_perc_chimeric'] = line[1].strip()
    else:
        qc_sheet.write(10, 0, f"WARNING: FusionInspector not run on sample")

    qc_sheet.write(17, 0, "Arriba Log Data")
    if os.path.exists(arriba_log_fh):
        with open(arriba_log_fh, 'r') as file:
            log_file = file.readlines()
            line = log_file[5].split('\t')
            qc_sheet.write(18, 0, f"Number of Input Reads")
            qc_sheet.write(18, 1, int(line[1].strip()))
            run_qc_metrics[sample]['arriba_reads'] = line[1].strip()

            line = log_file[6].split('\t')
            qc_sheet.write(19, 0, f"Avg Input Read Length")
            qc_sheet.write(19, 1, float(line[1].strip()))
            run_qc_metrics[sample]['arriba_read_len'] = line[1].strip()

            line = log_file[8].split('\t')
            line2 = log_file[9].split('\t')
            qc_sheet.write(20, 0, f"Number Uniquely Mapped")
            qc_sheet.write(20, 1, f"{int(line[1].strip())} ({line2[1].strip()})")
            run_qc_metrics[sample]['arriba_perc_mapped'] = line2[1].strip()

            line = log_file[10].split('\t')
            qc_sheet.write(21, 0, f"Average Mapped Length")
            qc_sheet.write(21, 1, float(line[1].strip()))
            run_qc_metrics[sample]['arriba_mapped_len'] = line[1].strip()

            line = log_file[30].split('\t')
            qc_sheet.write(22, 0, f"Number Unmapped (too short)")
            qc_sheet.write(22, 1, int(line[1].strip()))
            run_qc_metrics[sample]['arriba_perc_unmapped_short'] = line[1].strip()

            line = log_file[35].split('\t')
            qc_sheet.write(23, 0, f"Number Chimeric")
            qc_sheet.write(23, 1, int(line[1].strip()))
            run_qc_metrics[sample]['arriba_perc_chimeric'] = line[1].strip()
    else:
        qc_sheet.write(18, 0, f"WARNING: Arriba not run on sample")

    return run_qc_metrics

def write_imbalance_data(sheet, star_imbalance_counts, arriba_imbalance_counts, gene_read_counts):
    # Housekeeping Genes
    tbp = "ENSG00000112592"
    myc = "ENSG00000136997"
    hmbs = "ENSG00000256269"
    itgb7 = "ENSG00000139626"
    lrp1 = "ENSG00000123384"

    sum_counts = gene_read_counts[tbp] + gene_read_counts[myc] + gene_read_counts[hmbs] + gene_read_counts[itgb7] + gene_read_counts[lrp1]

    sheet.write("I1", "Sum Housekeeping (STAR)")
    sheet.write("J1", "Gene")
    sheet.write("K1", "STAR 5' Count")
    sheet.write("L1", "STAR 3' Count")
    sheet.write("M1", "STAR Score")
    sheet.write("N1", "Arriba 5' Count")
    sheet.write("O1", "Arriba 3' Count")
    sheet.write("P1", "Arriba Score")

    #NTRK1
    ntrk1_star_diff = star_imbalance_counts['chr1-156879999-156881850'] - star_imbalance_counts['chr1-156842081-156864799']
    ntrk1_arribba_diff = arriba_imbalance_counts['chr1-156879999-156881850'] - arriba_imbalance_counts['chr1-156842081-156864799']

    sheet.write("I2", sum_counts)
    sheet.write("J2", "NTRK1")
    sheet.write("K2", star_imbalance_counts['chr1-156842081-156864799'])
    sheet.write("L2", star_imbalance_counts['chr1-156879999-156881850'])
    sheet.write('M2', ntrk1_star_diff / sum_counts if sum_counts else 0)
    sheet.write("N2", arriba_imbalance_counts['chr1-156842081-156864799'])
    sheet.write("O2", arriba_imbalance_counts['chr1-156879999-156881850'])
    sheet.write('P2', ntrk1_arribba_diff / sum_counts if sum_counts else 0)

    #RET
    ret_star_diff = star_imbalance_counts['chr10-43124883-43127504'] - star_imbalance_counts['chr10-43109031-43111465']
    ret_arribba_diff = arriba_imbalance_counts['chr10-43124883-43127504'] - arriba_imbalance_counts['chr10-43109031-43111465']

    sheet.write("I3", sum_counts)
    sheet.write("J3", "RET")
    sheet.write("K3", star_imbalance_counts['chr10-43109031-43111465'])
    sheet.write("L3", star_imbalance_counts['chr10-43124883-43127504'])
    sheet.write('M3', ret_star_diff / sum_counts if sum_counts else 0)
    sheet.write("N3", arriba_imbalance_counts['chr10-43109031-43111465'])
    sheet.write("O3", arriba_imbalance_counts['chr10-43124883-43127504'])
    sheet.write('P3', ret_arribba_diff / sum_counts if sum_counts else 0)

    #ALK
    alk_star_diff = star_imbalance_counts['chr2-29207171-29209878'] - star_imbalance_counts['chr2-29328350-29383859']
    alk_arribba_diff = arriba_imbalance_counts['chr2-29207171-29209878'] - arriba_imbalance_counts['chr2-29328350-29383859']

    sheet.write("I4", sum_counts)
    sheet.write("J4", "ALK")
    sheet.write("K4", star_imbalance_counts['chr2-29328350-29383859'])
    sheet.write("L4", star_imbalance_counts['chr2-29207171-29209878'])
    sheet.write('M4', alk_star_diff / sum_counts if sum_counts else 0)
    sheet.write("N4", arriba_imbalance_counts['chr2-29328350-29383859'])
    sheet.write("O4", arriba_imbalance_counts['chr2-29207171-29209878'])
    sheet.write('P4', alk_arribba_diff / sum_counts if sum_counts else 0)

    #ROS1
    ros1_star_diff = star_imbalance_counts['chr6-117311020-117317272'] - star_imbalance_counts['chr6-117389350-117393321']
    ros1_arribba_diff = arriba_imbalance_counts['chr6-117311020-117317272'] - arriba_imbalance_counts['chr6-117389350-117393321']

    sheet.write("I5", sum_counts)
    sheet.write("J5", "ROS1")
    sheet.write("K5", star_imbalance_counts['chr6-117389350-117393321'])
    sheet.write("L5", star_imbalance_counts['chr6-117311020-117317272'])
    sheet.write('M5', ros1_star_diff / sum_counts if sum_counts else 0)
    sheet.write("N5", arriba_imbalance_counts['chr6-117389350-117393321'])
    sheet.write("O5", arriba_imbalance_counts['chr6-117311020-117317272'])
    sheet.write('P5', ros1_arribba_diff / sum_counts if sum_counts else 0)

def write_expression_data(sheet, gene_read_counts, sample, run_qc_metrics):
    # Housekeeping Genes
    tbp = "ENSG00000112592"
    myc = "ENSG00000136997"
    hmbs = "ENSG00000256269"
    itgb7 = "ENSG00000139626"
    lrp1 = "ENSG00000123384"

    # Sinai House Keeping Genes
    sec31a = "ENSG00000138674"
    cd74 = "ENSG00000019582"
    ctnnb1 = "ENSG00000168036"
    flna = "ENSG00000196924"
    prkar1a = "ENSG00000108946"
    septin2 = "ENSG00000168385"
    abi1 = "ENSG00000136754"
    hsp90aa1 = "ENSG00000080824"
    nono = "ENSG00000147140"
    znf207 = "ENSG00000010244"
    cnbp = "ENSG00000169714"
    tcf12 = "ENSG00000140262"
    gtf2i = "ENSG00000263001"
    # taf15 = ""
    stat6 = "ENSG00000166888"

    sum_counts = gene_read_counts[tbp] + gene_read_counts[myc] + gene_read_counts[hmbs] + gene_read_counts[itgb7] + gene_read_counts[lrp1]

    sheet.write("D5", "STAR Housekeeping Gene Expression")
    sheet.write("D6", "Gene")
    sheet.write("E6", "Ensemble ID")
    sheet.write("F6", "Total read Counts")

    sheet.write("D7", f"TBP")
    sheet.write("E7", f"{tbp}")
    sheet.write("F7", f"{gene_read_counts[tbp]}")
    run_qc_metrics[sample]['tbp'] = gene_read_counts[tbp]

    sheet.write("D8", f"MYC")
    sheet.write("E8", f"{myc}")
    sheet.write("F8", f"{gene_read_counts[myc]}")
    run_qc_metrics[sample]['myc'] = gene_read_counts[myc]

    sheet.write("D9", f"HMBS")
    sheet.write("E9", f"{hmbs}")
    sheet.write("F9", f"{gene_read_counts[hmbs]}")
    run_qc_metrics[sample]['hmbs'] = gene_read_counts[hmbs]

    sheet.write("D10", f"ITGB7")
    sheet.write("E10", f"{itgb7}")
    sheet.write("F10", f"{gene_read_counts[itgb7]}")
    run_qc_metrics[sample]['itgb7'] = gene_read_counts[itgb7]

    sheet.write("D11", f"LRP1")
    sheet.write("E11", f"{lrp1}")
    sheet.write("F11", f"{gene_read_counts[lrp1]}")
    run_qc_metrics[sample]['lrp1'] = gene_read_counts[lrp1]

    sheet.write("D12", f"Summed Count")
    sheet.write("E12", f"-")
    sheet.write("F12", sum_counts)
    run_qc_metrics[sample]['sum'] = sum_counts

    sheet.write("D14", "STAR Mt. Sinai Housekeeping Gene Expression")
    sheet.write("D14", "Gene")
    sheet.write("E14", "Ensemble ID")
    sheet.write("F14", "Total read Counts")

    sheet.write("D15", f"SEC31A")
    sheet.write("E15", f"{sec31a}")
    sheet.write("F15", f"{gene_read_counts[sec31a]}")

    sheet.write("D16", f"CD74")
    sheet.write("E16", f"{cd74}")
    sheet.write("F16", f"{gene_read_counts[cd74]}")

    sheet.write("D17", f"CTNNB1")
    sheet.write("E17", f"{ctnnb1}")
    sheet.write("F17", f"{gene_read_counts[ctnnb1]}")

    sheet.write("D18", f"FLNA")
    sheet.write("E18", f"{flna}")
    sheet.write("F18", f"{gene_read_counts[flna]}")

    prkar1a
    sheet.write("D19", f"PRKAR1A")
    sheet.write("E19", f"{prkar1a}")
    sheet.write("F19", f"{gene_read_counts[prkar1a]}")

    septin2
    sheet.write("D20", f"SEPTIN2")
    sheet.write("E20", f"{septin2}")
    sheet.write("F20", f"{gene_read_counts[septin2]}")

    abi1
    sheet.write("D21", f"ABI1")
    sheet.write("E21", f"{abi1}")
    sheet.write("F21", f"{gene_read_counts[abi1]}")

    sheet.write("D22", f"HSP90AA1")
    sheet.write("E22", f"{hsp90aa1}")
    sheet.write("F22", f"{gene_read_counts[hsp90aa1]}")

    nono
    sheet.write("D23", f"NONO")
    sheet.write("E23", f"{nono}")
    sheet.write("F23", f"{gene_read_counts[nono]}")

    sheet.write("D24", f"ZNF207")
    sheet.write("E24", f"{znf207}")
    sheet.write("F24", f"{gene_read_counts[znf207]}")

    sheet.write("D25", f"CNBP")
    sheet.write("E25", f"{cnbp}")
    sheet.write("F25", f"{gene_read_counts[cnbp]}")

    sheet.write("D26", f"TCF12")
    sheet.write("D26", f"{tcf12}")
    sheet.write("D26", f"{gene_read_counts[tcf12]}")

    sheet.write("D27", f"GTF2I")
    sheet.write("E27", f"{gtf2i}")
    sheet.write("F27", f"{gene_read_counts[gtf2i]}")

    sheet.write("D28", f"TAF15")
    # sheet.write("E28", f"{taf15}")
    # sheet.write("F28", f"{gene_read_counts[taf15]}")

    sheet.write("D29", f"STAT6")
    sheet.write("E29", f"{stat6}")
    sheet.write("F29", f"{gene_read_counts[stat6]}")


    return run_qc_metrics


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--samples_file', help="Input file for samples")
    parser.add_argument('-c', '--configuration', help="Configuration file for various settings")

    args = parser.parse_args()
    args.logLevel = "INFO"

    sys.stdout.write("Parsing configuration data\n")
    config = configuration.configure_runtime(args.configuration)

    sys.stdout.write("Parsing sample data\n")
    samples = configuration.configure_samples(args.samples_file, config)

    pwd = os.getcwd()
    resource_dir = os.path.abspath("/clin/resources")

    expressionDB = os.path.join(resource_dir, "STAR_rna_expression_GRCh38.db")

    run_qc_metrics = defaultdict(dict)

    sys.stdout.write("Parsing sample output data...\n")
    sample_names = []
    for sample in samples:
        sample_names.append(sample)
        sys.stdout.write(f"Parsing RNA Fusion Calling Data for sample {sample}\n")
        report_name = f"{samples[sample]['sample_name']}_RNA.xlsx"

        star_outdir_name = f"{samples[sample]['sample_name']}_STAR-Fusion_output/"
        fusioninspector_outdir_name = f"{samples[sample]['sample_name']}_STAR-Fusion_output/FusionInspector-validate/"
        arriba_outdir_name = f"{samples[sample]['sample_name']}_Arriba_output/"

        fusioninspector_dir = os.path.join(pwd, fusioninspector_outdir_name)
        fusioninspector_log_dir = os.path.join(fusioninspector_dir, "fi_workdir")
        aribba_dir = os.path.join(pwd, arriba_outdir_name)
        star_dir = os.path.join(pwd, star_outdir_name)

        fusioninspector_results_fh = os.path.join(fusioninspector_dir, "finspector.FusionInspector.fusions.abridged.tsv")
        fusioninspector_log_fh = os.path.join(fusioninspector_log_dir, "Log.final.out")
        star_log_fh = os.path.join(star_dir, "Log.final.out")
        star_fusions_fh = os.path.join(star_dir, "star-fusion.fusion_predictions.abridged.coding_effect.tsv")
        arriba_fusions_fh = os.path.join(aribba_dir, "fusions.tsv")
        arriba_log_fh = os.path.join(aribba_dir, "Log.final.out")
        ctat_output_fh = os.path.join(star_dir, f"{samples[sample]['sample_name']}_splicing.cancer.introns")
        gene_reads_fh = os.path.join(star_dir, "ReadsPerGene.out.tab")
        arriba_ctat_output_fh = os.path.join(aribba_dir, f"{samples[sample]['sample_name']}_splicing.cancer.introns")
        star_imbalance_fh = os.path.join(star_dir, "imbalance_counts.bed")
        arriba_imbalance_fh = os.path.join(aribba_dir, "imbalance_counts.bed")

        wb = xlsxwriter.Workbook(report_name)

        error_style = wb.add_format({'bg_color': '#FF0000'})
        warning_style = wb.add_format({'bg_color': '#FF9900'})
        pass_style = wb.add_format({'bg_color': '#00FF00'})
        interest_style = wb.add_format({'bg_color': '#808080'})
        default_style = wb.add_format({'bg_color': '#FFFFFF'})

        qc_sheet = wb.add_worksheet("Expression and Imbalance")
        # imbalance_sheet = wb.add_worksheet("5'-3' Imbalance")
        inspector_sheet = wb.add_worksheet("Fusion Inspector")
        star_sheet = wb.add_worksheet("STAR-Fusion")
        arriba_sheet = wb.add_worksheet("Arriba Fusion")
        ctat_sheet = wb.add_worksheet("CTAT-Splicing - STAR")
        arriba_ctat_sheet = wb.add_worksheet("CTAT-Splicing - Arriba")

        gene_read_counts = read_counts(gene_reads_fh)

        run_qc_metrics = write_qc_data(qc_sheet, samples, sample, fusioninspector_log_fh, star_log_fh, arriba_log_fh, run_qc_metrics)
        run_qc_metrics = write_expression_data(qc_sheet, gene_read_counts, sample, run_qc_metrics)

        if os.path.exists(ctat_output_fh):
            write_csv_data(ctat_sheet, ctat_output_fh)
        else:
            sys.stdout.write("No CTAT (STAR-Fusion) data...\n")

        if os.path.exists(arriba_ctat_output_fh):
            write_csv_data(arriba_ctat_sheet, arriba_ctat_output_fh)
        else:
            sys.stdout.write("No CTAT (Arriba) data...\n")

        if os.path.exists(fusioninspector_results_fh):
            write_csv_data(inspector_sheet, fusioninspector_results_fh)
        else:
            sys.stdout.write("No Fusion-Inspector data...\n")
            run_qc_metrics[sample]['insp_reads'] = "N/A"
            run_qc_metrics[sample]['insp_read_len'] = "N/A"
            run_qc_metrics[sample]['insp_perc_mapped'] = "N/A"
            run_qc_metrics[sample]['insp_mapped_len'] = "N/A"
            run_qc_metrics[sample]['insp_perc_unmapped_short'] = "N/A"
            run_qc_metrics[sample]['insp_perc_chimeric'] = "N/A"

        if os.path.exists(star_fusions_fh):
            write_csv_data(star_sheet, star_fusions_fh)
        else:
            sys.stdout.write("No STAR-Fusion data...\n")

        if os.path.exists(arriba_fusions_fh):
            write_csv_data(arriba_sheet, arriba_fusions_fh)
        else:
            sys.stdout.write("No Arriba data...\n")

        star_imbalance_counts = get_imbalance_counts(star_imbalance_fh)
        arriba_imbalance_counts = get_imbalance_counts(arriba_imbalance_fh)
        write_imbalance_data(qc_sheet, star_imbalance_counts, arriba_imbalance_counts, gene_read_counts)

        wb.close()

    sys.stdout.write("Writing all sample QC metrics...\n")
    keys = list(run_qc_metrics[sample_names[0]].keys())
    fieldnames = ['Sample']
    fieldnames.extend(keys)
    with open("run_qc_summary.txt", 'w') as csvfile:
        writer = csv.DictWriter(csvfile, delimiter='\t', fieldnames=fieldnames)
        writer.writeheader()
        for sample in samples:
            dict = run_qc_metrics[sample]
            dict['Sample'] = sample
            writer.writerow(dict)
