#!/usr/bin/env python

# Standard packages
import sys
import csv
import argparse
import subprocess

from multiprocessing import Pool
from collections import defaultdict

# Subroutine for executing command lines outside of the Pool based multiprocessing
def execute_cmd(cmd_string):
    sys.stdout.write(
        f"Running: {cmd_string}\n"
    )
    subprocess.run(
        f"{cmd_string}", stderr=subprocess.PIPE, shell=True
    )

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', help="Input file for samples")
    parser.add_argument('-n', '--num_cpu', help="Number of CPU cores to use for multiprocessing")
    args = parser.parse_args()
    args.logLevel = "INFO"

    version = "1.0"

    # Resources and command lines
    ref = f"/mnt/shared-data/Resources/ReferenceData/GRCh38/Homo_sapiens_assembly38.fasta"
    dbsnp_sites = f"/mnt/shared-data/Resources/ReferenceData/GRCh38/dbsnp_146.hg38.vcf.gz"
    gnomad_sites = f"/mnt/shared-data/Resources/ReferenceData/GRCh38/af-only-gnomad.hg38.vcf.gz"

    num_cpu = int(args.num_cpu)

    pool = Pool(num_cpu)
    small_pool = Pool(int(num_cpu / 2))
    tiny_pool = Pool(int(num_cpu / 8))

    print(f"Setting full pool size to {num_cpu}\n")
    print(f"Setting small pool size to {num_cpu / 2} for memory intensive programs\n")
    print(f"Setting tiny pool size to {num_cpu / 8} for other threaded programs\n")

    bwa_commands = []
    add_commands = []
    mark_commands = []
    tag_commands = []
    recal_commands = []
    bqsr_commands = []
    mutect_commands = []
    freebayes_commands = []
    vardict_commands = []
    varscan_commands = []
    pisces_commands = []
    bcftools_commands = []

    # Parse inputs and construct all commands needed to be executed
    with open(args.input, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t')
        header = next(reader)
        for row in reader:
            library = row[0]
            fastq1 = row[1]
            fastq2 = row[2]

            tmp = f"{library}.tmp"
            aligned_bam = f"{library}.bwa_aligned.bam"
            rg_bam = f"{library}.readgroups.bam"
            marked = f"{library}.marked.bam"
            fixed = f"{library}.fixed.bam"
            recal_table = f"{library}.recal.table"
            recalibrated = f"{library}.recalibrated.bam"
            mutect_vcf = f"{library}.mutect2.vcf.gz"
            f1r2 = f"{library}.f1r2.tar.gz"

            if args.normal:
                normal_bam = f""
                normal_name = f""

            add_commands.append(f"gatk AddOrReplaceReadGroups -I {aligned_bam} -O {rg_bam} --RGID {library} --RGLB {library} --RGSM {library} --RGPL ILLUMINA --RGPU Illumina")
            tag_commands.append(f"gatk SetNmMdAndUqTags -I {rg_bam} -O {fixed} -R {ref}")
            recal_commands.append(f"gatk BaseRecalibrator -I {fixed} -R {ref} -O {recal_table} --known-sites {dbsnp_sites} --known-sites {gnomad_sites}")
            bqsr_commands.append(f"gatk ApplyBQSR -R {ref} -I {fixed} -O {recalibrated} --bqsr-recal-file {recal_table}")

            if args.normal:
                mutect_commands.append(gatk Mutect2 -R {ref} -I {recalibrated} -I {normal_bam} -normal {normal_name} -O {mutect_vcf} --germline-resource {gnomad_sites} --native-pair-hmm-threads 8 --f1r2-tar-gz {f1r2} -G StandardMutectAnnotation)
            else:
                mutect_commands.append(f"gatk Mutect2 -R {ref} -I {recalibrated} -O {mutect_vcf} --germline-resource {gnomad_sites} --native-pair-hmm-threads 8 --f1r2-tar-gz {f1r2} -G StandardMutectAnnotation")


    # Execute BWA-MEM with its own multiprocessing
    for command in bwa_commands:
        execute_cmd(command)

    # GATK Commands are executed in single threaded modes using Python multiprocessing
    # across samples
    add_results = pool.map(execute_cmd, add_commands)
    tag_results = pool.map(execute_cmd, tag_commands)
    recal_results = pool.map(execute_cmd, recal_commands)
    bqsr_results = pool.map(execute_cmd, bqsr_commands)
    mutect_results = tiny_pool.map(execute_cmd, mutect_commands)
