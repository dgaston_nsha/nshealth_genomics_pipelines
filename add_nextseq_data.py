#!/usr/bin/env python

import os
import sys
import csv
import getpass
import argparse

from cassandra import query
from cassandra.cluster import Cluster
from cassandra.cqlengine import connection
from cassandra.auth import PlainTextAuthProvider

from multiprocessing import Pool
from modules import configuration
from database import utils_updated as utils

from database.datastore import Target
from database.datastore import TargetCovData
from database.datastore import Variant
from database.datastore import VariantCallerVAFs
from database.datastore import VariantStats

from collections import defaultdict

def add_cov_data(data):
    sequencer = data[0]
    samples = data[1]
    sample = data[2]
    library = data[3]
    auth = data[4]
    address = data[5]

    library_id = samples[sample][library]['library_name']
    sample_id = samples[sample][library]['sample_name']

    connection.setup([address], "datastore", auth_provider=auth)

    cov_file = f"{library_id}.sambamba_regioncoverage.bed"
    sys.stdout.write(f"Adding data from {cov_file}\n")

    with open(cov_file, 'r') as coverage:
        reader = csv.reader(coverage, delimiter='\t')
        header = next(reader)

        for row in reader:
            data = TargetCovData.create(target_name = row[3],
                                        panel_name = f"{samples[sample][library]['panel']}",
                                        sequencer = sequencer,
                                        sample = f"{sample_id}",
                                        library_name = f"{library_id}",
                                        num_reads = row[4],
                                        mean_coverage = row[5],
                                        percent_bp_100 = row[6],
                                        percent_bp_200 = row[7],
                                        percent_bp_500 = row[8],
                                        percent_bp_1000 = row[9],
                                        percent_bp_1500 = row[10],
                                        percent_bp_2000 = row[11])

    return f"Processed file {cov_file}"

def add_var_data(data):
    sequencer = data[0]
    samples = data[1]
    sample = data[2]
    library = data[3]
    auth = data[4]
    address = data[5]

    library_id = samples[sample][library]['library_name']
    sample_id = samples[sample][library]['sample_name']

    connection.setup([address], "datastore", auth_provider=auth)

    sample_variants = f"{library_id}.variants.csv"
    sys.stdout.write(f"Adding data from {sample_variants}\n")
    with open(sample_variants, 'r') as csvfile:
        reader = csv.DictReader(csvfile)
        row_counts = defaultdict(lambda: 1)
        for variant in reader:
            variant_id = f"{variant['Chr']}-{variant['Pos']}-{variant['Ref']}-{variant['Alt']}"
            data = VariantCallerVAFs.create(variant_id = variant_id,
                                            panel_name = f"{samples[sample][library]['panel']}",
                                            sequencer = sequencer,
                                            sample = f"{sample_id}",
                                            library_name = f"{library_id}",
                                            max_vaf = variant['max_som_aaf'],
                                            freebayes_vaf = variant['freebayes_AF'] or None,
                                            mutect_vaf = variant['mutect_AF'] or None,
                                            scalpel_vaf = variant['scalpel_AF'] or None,
                                            pindel_vaf = variant['pindel_AF'] or None,
                                            platypus_vaf = variant['platypus_AF'] or None,
                                            vardict_vaf = variant['vardict_AF'] or None)
    return f"Processed file {sample_variants}"


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--samples_file', help="Input file for samples")
    parser.add_argument('-c', '--configuration', help="Configuration file for various settings")
    parser.add_argument('-t', '--threads', help="Number of CPU Threads", default=12)
    parser.add_argument('-m', '--sequencer', help="Sequencer model used (MiSeq/NextSeq)")
    parser.add_argument('-u', '--username', help="Cassandra DB username")
    parser.add_argument('-a', '--address', help="IP Address for Cassandra connection", default='127.0.0.1')

    args = parser.parse_args()
    args.logLevel = "INFO"

    auth_provider = None
    if args.username:
        password = getpass.getpass()
        auth_provider = PlainTextAuthProvider(username=args.username, password=password)
    else:
        sys.stderr("Failed to provide Cassandra DB username")
        exit()

    pool = Pool(int(args.threads))

    sys.stdout.write("Parsing configuration data\n")
    config = configuration.configure_runtime(args.configuration)

    sys.stdout.write("Parsing sample data\n")
    libraries = configuration.configure_samples(args.samples_file, config)
    samples = configuration.merge_library_configs_samples(libraries)

    sample_data = list()

    for sample in samples:
        for library in samples[sample]:
            data = ((args.sequencer, samples, sample, library, auth_provider, args.address))
            sample_data.append(data)

    sys.stdout.write("Adding coverage data for samples\n")
    results = pool.map(add_cov_data, sample_data)

    sys.stdout.write("Adding variant data for samples\n")
    results = pool.map(add_var_data, sample_data)
