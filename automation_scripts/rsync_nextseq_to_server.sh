#!/bin/sh

run_dir=""
fastq_dir="${run_dir}/Analysis/1/Data/fastq"
aggregate_qc_plots_dir="${run_dir}/Analysis/1/Data/AggregateFastQCPlots"
report_files_dir="${run_dir}/Analysis/1/Data/report_files"
reports_dir="${run_dir}/Analysis/1/Data/Reports"
report="${run_dir}/Analysis/1/Data/report.html"
run_analytics_dir="${run_dir}/Analysis/1/Data/RunInstrumentAnalyticsMetrics"
interop_dir="${run_dir}/InterOp"
primary_analysis_metrics_dir="${run_dir}/PrimaryAnalysisMetrics"

server_ip="142.239.155.181"
server_dir="/mnt/shared-data/clin/moldiagostics_analyses/${run_dir}/"
username="dgaston"
server_location="${username}@${server_ip}:${server_dir}"

rsync -avzh $fastq_dir $server_location
rsync -avzh $aggregate_qc_plots_dir $server_location
rsync -avzh $report_files_dir $server_location
rsync -avzh $reports_dir $server_location
rsync -avzh $report $server_location
rsync -avzh $run_analytics_dir $server_location
rsync -avzh $interop_dir $server_location
rsync -avzh $primary_analysis_metrics_dir $server_location

touch transfered.out
rsync -avzh transfered.out $server_location
