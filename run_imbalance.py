#!/usr/bin/env python

# Standard packages
import os
import sys
import argparse
import subprocess

from modules import configuration
from collections import defaultdict
from multiprocessing import Pool

VERSION = "1.1"

# Subroutine for executing command lines outside of the Pool based multiprocessing
def execute_cmd(cmd_string):
    sys.stdout.write(
        f"Running: {cmd_string}\n"
    )
    subprocess.run(
        f"{cmd_string}", stderr=subprocess.PIPE, shell=True
    )

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--samples_file', help="Input file for samples")
    parser.add_argument('-c', '--configuration', help="Configuration file for various settings")
    parser.add_argument('-b', '--bed', help="Full path to BED file to use for imbalance targets",
    default="/mnt/shared-data/ddb-configs/rna/ampliseq_focus_imbalance_rna_GRCh38.bed")
    parser.add_argument('-t', '--threads', help="Number of Threads", default=12)

    args = parser.parse_args()
    args.logLevel = "INFO"

    sys.stdout.write("Parsing configuration data\n")
    config = configuration.configure_runtime(args.configuration)

    sys.stdout.write("Parsing sample data\n")
    samples = configuration.configure_samples(args.samples_file, config)

    pool = Pool(int(args.threads))
    commands = list()

    pwd = os.getcwd()

    if not os.path.exists(args.bed):
        sys.stdout.write(f"File {args.bed} does not exist. Exiting...\n")
        sys.exit()

    sys.stdout.write("Running Analyses...\n")
    for sample in samples:
        fq1 = samples[sample]['fastq1']
        fq2 = samples[sample]['fastq2']

        star_outdir_name = f"{samples[sample]['sample_name']}_STAR-Fusion_output/"
        arriba_outdir_name = f"{samples[sample]['sample_name']}_Arriba_output/"

        star_dir = os.path.join(pwd, star_outdir_name)
        aribba_dir = os.path.join(pwd, arriba_outdir_name)

        star_bam_fh = os.path.join(star_dir, "Aligned.out.bam")
        arriba_bam_fh = os.path.join(aribba_dir, "Aligned.sortedByCoord.out.bam")

        star_imbalance_fh = os.path.join(star_dir, "imbalance_counts.bed")
        arriba_imbalance_fh = os.path.join(aribba_dir, "imbalance_counts.bed")

        commands.append(f"bedtools coverage -a {args.bed} -b {star_bam_fh} > {star_imbalance_fh}")
        commands.append(f"bedtools coverage -a {args.bed} -b {arriba_bam_fh} > {arriba_imbalance_fh}")

    sys.stdout.write(f"Beginning execution of {len(commands)} with {args.threads} threads...\n")
    results = pool.map(execute_cmd, commands)
