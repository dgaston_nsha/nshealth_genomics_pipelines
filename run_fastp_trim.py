#!/usr/bin/env python

# Standard packages
import sys
import argparse
import subprocess

from modules import configuration
from collections import defaultdict


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--samples_file', help="Input file for samples")
    parser.add_argument('-c', '--configuration', help="Configuration file for various settings")
    parser.add_argument('-t', '--threads', help="Number of Threads", default=24)

    args = parser.parse_args()
    args.logLevel = "INFO"

    fastp_bin = f"fastp"

    sys.stdout.write("Parsing configuration data\n")
    config = configuration.configure_runtime(args.configuration)

    sys.stdout.write("Parsing sample data\n")
    samples = configuration.configure_samples(args.samples_file, config)

    sys.stdout.write("Running Analyses...\n")
    for sample in samples:

        fq1 = f"{samples[sample]['sample_name']}.trimmed.R1.fastq.gz"
        fq2 = f"{samples[sample]['sample_name']}.trimmed.R2.fastq.gz"

        unpaired1 = f"{samples[sample]['sample_name']}.fastp_unpaired.R1.fastq.gz"
        unpaired2 = f"{samples[sample]['sample_name']}.fastp_unpaired.R2.fastq.gz"
        html_report = f"{samples[sample]['sample_name']}.fastp_report.html"
        json_report = f"{samples[sample]['sample_name']}.fastp_report.json"

        fastp_cmd = f"{fastp_bin} -i {samples[sample]['fastq1']} -I {samples[sample]['fastq2']} -o {fq1} -O {fq2} --unpaired1 {unpaired1} --unpaired2 {unpaired2} -h {html_report} -j {json_report} --dont_overwrite"

        sys.stdout.write(f"#################################################\n")
        sys.stdout.write(f"Running Fastp\n")
        sys.stdout.write(f"#################################################\n")
        sys.stdout.write(f"Running: {fastp_cmd}\n")
        subprocess.run(f"{fastp_cmd}", stderr=subprocess.PIPE, shell=True)
