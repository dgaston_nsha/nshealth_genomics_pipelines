#!/usr/bin/env python

# Standard packages
import os
import sys
import argparse
import subprocess

from modules import configuration
from collections import defaultdict


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--samples_file', help="Input file for samples")
    parser.add_argument('-c', '--configuration', help="Configuration file for various settings")
    parser.add_argument('-t', '--threads', help="Number of Threads", default=24)

    args = parser.parse_args()
    args.logLevel = "INFO"

    analysis_root_dir = os.getcwd()
    ctat_genome = f"/mnt/shared-data/Resources/RNA/BroadCTAT/GRCh38_gencode_v37_CTAT_lib_Mar012021.plug-n-play/ctat_genome_lib_build_dir"

    arriba_ref = f"/mnt/shared-data/Resources/RNA/Arriba/GRCh38Viral/STAR_index_GRCh38viral_ENSEMBL93/"
    arriba_gtf = f"/mnt/shared-data/Resources/RNA/Arriba/GRCh38Viral/ENSEMBL93.gtf"
    arriba_fasta = f"/mnt/shared-data/Resources/RNA/Arriba/GRCh38Viral/GRCh38viral.fa"

    arriba_root = f"/home/dgaston/miniconda3/envs/nshealth_genomics/var/lib/arriba"
    arriba_blacklist = f"{arriba_root}/blacklist_hg38_GRCh38_v2.3.0.tsv.gz"
    arriba_known = f"{arriba_root}/known_fusions_hg38_GRCh38_v2.3.0.tsv.gz"
    arriba_domains = f"{arriba_root}/protein_domains_hg38_GRCh38_v2.3.0.gff3"

    user_id = os.getuid()

    # Docker should run with --user={user_id}:{user_id} so it runs as the user
    # but this creates issues of not being able to read the FastQs so need to
    # test out the proper commands

    arriba_bin = f"/home/dgaston/miniconda3/envs/nshealth_genomics/bin/run_arriba.sh"
    sf_bin = f"docker run -v `pwd`:/data -v /mnt/shared-data:/mnt/shared-data --rm trinityctat/starfusion:1.10.1 STAR-Fusion"
    ctat_splicing = f"docker run -v `pwd`:/data -v /mnt/shared-data:/mnt/shared-data --rm trinityctat/ctat_splicing:0.0.1 CTAT-SPLICING/STAR_to_cancer_introns.py"

    sys.stdout.write("Parsing configuration data\n")
    config = configuration.configure_runtime(args.configuration)

    sys.stdout.write("Parsing sample data\n")
    samples = configuration.configure_samples(args.samples_file, config)

    sys.stdout.write("Running Analyses...\n")
    for sample in samples:
        fq1 = samples[sample]['fastq1']
        fq2 = samples[sample]['fastq2']
        output_dir = f"{samples[sample]['sample_name']}_STAR-Fusion_output"
        arriba_output = f"{samples[sample]['sample_name']}_Arriba_output/"
        arriba_dir = os.path.join(analysis_root_dir, arriba_output)

        sf_cmd = f"{sf_bin} --left_fq {fq1} --right_fq {fq2} --genome_lib_dir {ctat_genome} --examine_coding_effect --FusionInspector validate --denovo_reconstruct --CPU {args.threads} -O /data/{output_dir}"
        splicing_cmd = f"{ctat_splicing} --vis --ctat_genome_lib {ctat_genome} --SJ_tab_file /data/{output_dir}/SJ.out.tab --chimJ_file /data/{output_dir}/Chimeric.out.junction --bam_file /data/{output_dir}/Aligned.out.bam --output_prefix /data/{output_dir}/{samples[sample]['sample_name']}_splicing --sample_name {samples[sample]['sample_name']}"
        arriba_cmd = f"{arriba_bin} {arriba_ref} {arriba_gtf} {arriba_fasta} {arriba_blacklist} {arriba_known} {arriba_domains} {args.threads} {fq1} {fq2}"
        arriba_splicing_cmd = f"{ctat_splicing} --vis --ctat_genome_lib {ctat_genome} --SJ_tab_file /data/{arriba_output}/SJ.out.tab --bam_file /data/{arriba_output}/Aligned.sortedByCoord.out.bam --output_prefix /data/{arriba_output}/{samples[sample]['sample_name']}_splicing --sample_name {samples[sample]['sample_name']}"

        sys.stdout.write(f"#################################################\n")
        sys.stdout.write(f"Running STAR-Fusion\n")
        sys.stdout.write(f"#################################################\n")
        sys.stdout.write(f"Running: {sf_cmd}\n")
        subprocess.run(f"{sf_cmd}", stderr=subprocess.PIPE, shell=True)

        sys.stdout.write(f"#################################################\n")
        sys.stdout.write(f"Running CTAT-Splicing on STAR-Fusion Alignment\n")
        sys.stdout.write(f"#################################################\n")
        sys.stdout.write(f"Running: {splicing_cmd}\n")
        subprocess.run(f"{splicing_cmd}", stderr=subprocess.PIPE, shell=True)

        sys.stdout.write(f"#################################################\n")
        sys.stdout.write(f"Running Arriba\n")
        sys.stdout.write(f"#################################################\n")
        sys.stdout.write(f"Changing to directory: {arriba_dir}\n")
        os.mkdir(arriba_dir)
        os.chdir(arriba_dir)
        sys.stdout.write(f"Running: {arriba_cmd}\n")
        subprocess.run(f"{arriba_cmd}", stderr=subprocess.PIPE, shell=True)
        os.chdir(analysis_root_dir)

        sys.stdout.write(f"#################################################\n")
        sys.stdout.write(f"Running CTAT-Splicing on Arriba Alignment\n")
        sys.stdout.write(f"#################################################\n")
        sys.stdout.write(f"Running: {splicing_cmd}\n")
        subprocess.run(f"{arriba_splicing_cmd}", stderr=subprocess.PIPE, shell=True)
